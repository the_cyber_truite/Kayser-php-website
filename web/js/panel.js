var locs = [];
locs["paris"] = new google.maps.LatLng(48.9566140, 2.1522219);
locs["avignon"] = new google.maps.LatLng(43.9493170, 4.8055280);
locs["beausoleil"] = new google.maps.LatLng(43.7417670, 7.4230190);
locs["lyon"] = new google.maps.LatLng(45.7640430, 4.8356590);
locs["menton"] = new google.maps.LatLng(43.7744810, 7.4975400);
locs["lille"] = new google.maps.LatLng(50.633333, 3.066667);

// locs["unitedstates"] = new google.maps.LatLng(40.4998689, -96.5806928);
locs["chile"] = new google.maps.LatLng(-33.4691199, -70.6419970);
locs["cambodia"] = new google.maps.LatLng(11.9879665, 104.9806145);
locs["colombia"] = new google.maps.LatLng(4.5980560, -74.0758330);
locs["emirates"] = new google.maps.LatLng(24.8217803, 54.8823378);
locs["hongkong"] = new google.maps.LatLng(22.2973559, 114.1809081);
locs["indonesia"] = new google.maps.LatLng(-6.2087634, 106.8455990);
locs["japan"] = new google.maps.LatLng(36.2048240, 138.2529240);
locs["korea"] = new google.maps.LatLng(37.5665350, 126.9779692);
locs["lebanon"] = new google.maps.LatLng(33.8886289, 35.4954794);
locs["morocco"] = new google.maps.LatLng(35.7594651, -5.8339543);
locs["mexico"] = new google.maps.LatLng(19.4326077, -99.1332080);
locs["newyork"] = new google.maps.LatLng(40.7033127, -73.979681);
locs["philippines"] = new google.maps.LatLng(14.5995124, 120.9842195);
locs["portugal"] = new google.maps.LatLng(38.7222524, -9.1393366);
locs["rdc"] = new google.maps.LatLng(-4.3316670, 15.3138890);
locs["saudiarabia"] = new google.maps.LatLng(24.266906, 45.1078489);
locs["senegal"] = new google.maps.LatLng(14.7645042, -17.3660286);
locs["southcorea"] = new google.maps.LatLng(37.5651,126.98955);
locs["singapore"] = new google.maps.LatLng(1.3520830, 103.8198360);
locs["taiwan"] = new google.maps.LatLng(25.0483346,121.5820524);
locs["thailand"] = new google.maps.LatLng(13.03887,101.490104);
locs["tunisia"] = new google.maps.LatLng(36.8064948, 10.1815316);
locs["unitedstates"] = new google.maps.LatLng(40.7585043, -73.9782180);


var zoom = [];
zoom["paris"] = 12;
zoom["avignon"] = 13;
zoom["beausoleil"] = 14;
zoom["lyon"] = 13;
zoom["menton"] = 13;
zoom["lille"] = 13;

zoom["cambodia"] = 9;
zoom["chile"] =   10;
zoom["colombia"] = 11; 
zoom["emirates"] = 9;
zoom["hongkong"] = 12;
zoom["indonesia"] = 10; 
zoom["japan"] = 6;
zoom["korea"] = 12;
zoom["lebanon"] = 12;
zoom["morocco"] = 12; 
zoom["mexico"] = 12; 
zoom["newyork"] = 11 ; 
zoom["philippines"] = 12;
zoom["portugal"] = 10; 
zoom["rdc"] = 12; 
zoom["saudiarabia"] = 7; 
zoom["senegal"] = 10; 
zoom["southcorea"] = 10; 
zoom["singapore"] = 12;
zoom["taiwan"] = 11;
zoom["thailand"] = 7;
zoom["tunisia"] = 11;
zoom["unitedstates"] = 12; 

var searchLocation = null;

function getLocation(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    var loc = '';
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            loc = String(sParameterName[1]);
        }
    }
    if(loc == ''){
      return null;
    }else{
      searchLocation = loc;
      return loc;
    }

}

function showLocation(){
  var loc = getLocation('loc');

  var geolocation = false;
  if(loc == '' || locs[loc] == "undefined" || loc == null){
    loc = 'paris';
    geolocation = true;
  }
  var LatLng = locs[loc];
  
  var map = new google.maps.Map(document.getElementById('map-canvas'), {center: LatLng, zoom: zoom[loc], mapTypeId: google.maps.MapTypeId.ROADMAP});
  var panelDiv = document.getElementById('panel');
  var data = new MedicareDataSource;
  var view = new storeLocator.View(map, data, {geolocation: geolocation, features: data.getFeatures()});
  return new storeLocator.Panel(panelDiv, {view: view});
}

jQuery(function($) {
  google.maps.event.addDomListener(window, 'load', showLocation());
});

