/**
 * @implements storeLocator.DataFeed
 * @constructor
 */
function MedicareDataSource() {
}

MedicareDataSource.prototype.getStores = function(bounds, features, callback) {
  var that = this;
  var center = bounds.getCenter();
  var glutenFeature = this.FEATURES_.getById('Gluten-YES');
  var restaurantFeature = this.FEATURES_.getById('Restaurant-YES');
  var cafeFeature = this.FEATURES_.getById('Cafe-YES');

  var where = '(ST_INTERSECTS(geometry, ' + this.boundsToWkt_(bounds) + ')' +
      ' OR ST_DISTANCE(geometry, ' + this.latLngToWkt_(center) + ') < 20000)';

  ftData = [];
  if (features.contains(glutenFeature)) {
    ftData.push('gluten');
  }
  if (features.contains(restaurantFeature)) {
    ftData.push('restaurant');
  }
  if (features.contains(cafeFeature)) {
    ftData.push('cafe');
  }

  var url = '/fr/stores';
  data = {};
  data['features'] = this.join_(ftData, ',');
  if(searchLocation != null && searchLocation != '' ) data['location'] = searchLocation;
      // console.log(data);
  $.getJSON(url, data, function(resp) {
    var stores = that.parse_(resp);
    that.sortByDistance_(center, stores);
    callback(stores);
  });
};

MedicareDataSource.prototype.latLngToWkt_ = function(point) {
  return 'ST_POINT(' + point.lng() + ', ' + point.lat() + ')';
};

MedicareDataSource.prototype.boundsToWkt_ = function(bounds) {
  var ne = bounds.getNorthEast();
  var sw = bounds.getSouthWest();
  return [
    "ST_GEOMFROMTEXT('POLYGON ((",
    sw.lng(), ' ', sw.lat(), ', ',
    ne.lng(), ' ', sw.lat(), ', ',
    ne.lng(), ' ', ne.lat(), ', ',
    sw.lng(), ' ', ne.lat(), ', ',
    sw.lng(), ' ', sw.lat(),
    "))')"
  ].join('');
};

MedicareDataSource.prototype.parse_ = function(data) {
  var stores = [];
  for (var i = 0, row; row = data.features[i]; i++) {
    var props = row.properties;
    var features = new storeLocator.FeatureSet;
    features.add(this.FEATURES_.getById('Gluten-' + props.sans_gluten));
    features.add(this.FEATURES_.getById('Restaurant-' + props.restaurant));
    features.add(this.FEATURES_.getById('Cafe-' + props.cafe));

    var position = new google.maps.LatLng(row.geometry.coordinates[1],
                                          row.geometry.coordinates[0]);

    var shop = this.join_([props.Shp_num_an, props.Shp_centre], ', ');
    var locality = this.join_([props.Locality, props.Postcode], ', ');

    var name = (props.Country == 'France' && props.Fcilty_nam != 'Beaugrenelle' && props.Fcilty_nam != 'Orly Aéroport' && props.Fcilty_nam != 'Levallois Trebois')
      ? 'Boulangerie '+ props.Fcilty_nam
      : props.Fcilty_nam;
    var params =  {
      title: name,
      address: this.join_([props.Street_add, locality], '<br>'),
      phone: 'Tél '+props.Fcilty_ctc,
      misc: props.Hrs_of_bus,
    }
    if(typeof props.website != 'undefined' && props.website != '') props["web"] = '<a href="'+props.website+'">'+props.website+'</a>';

    var store = new storeLocator.Store(props.uuid, position, features, params);
    stores.push(store);
  }
  return stores;
};

/**
 * @const
 * @type {!storeLocator.FeatureSet}
 * @private
 */
MedicareDataSource.prototype.FEATURES_ = new storeLocator.FeatureSet(
  new storeLocator.Feature('Gluten-YES', 'Corner Sans Gluten'),
  new storeLocator.Feature('Restaurant-YES', 'Restaurant Du Boulanger'),
  new storeLocator.Feature('Cafe-YES', 'Boulangerie Café')
);

/**
 * @return {!storeLocator.FeatureSet}
 */
MedicareDataSource.prototype.getFeatures = function() {
  return this.FEATURES_;
};


/**
 * Joins elements of an array that are non-empty and non-null.
 * @private
 * @param {!Array} arr array of elements to join.
 * @param {string} sep the separator.
 * @return {string}
 */
MedicareDataSource.prototype.join_ = function(arr, sep) {
  var parts = [];
  for (var i = 0, ii = arr.length; i < ii; i++) {
    arr[i] && parts.push(arr[i]);
  }
  return parts.join(sep);
};

/**
 * Sorts a list of given stores by distance from a point in ascending order.
 * Directly manipulates the given array (has side effects).
 * @private
 * @param {google.maps.LatLng} latLng the point to sort from.
 * @param {!Array.<!storeLocator.Store>} stores  the stores to sort.
 */
MedicareDataSource.prototype.sortByDistance_ = function(latLng, stores) {
  stores.sort(function(a, b) {
    return a.distanceTo(latLng) - b.distanceTo(latLng);
  });
};