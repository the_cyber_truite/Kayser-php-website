/**
 * @extends storeLocator.StaticDataFeed
 * @constructor
 */
function MedicareDataSource() {
  $.extend(this, new storeLocator.StaticDataFeed);

  var that = this;
  // $.get('http://localhost:8888/kayser/web/js/liste_kayser_excel.csv', function(data) {
  $.get('http://maison-kayser.nemus.io/web/js/liste_kayser_excel.csv', function(data) {
    that.setStores(that.parse_(data));

    // console.log(data);
  });
}

/**
 * @const
 * @type {!storeLocator.FeatureSet}
 * @private
 */
MedicareDataSource.prototype.FEATURES_ = new storeLocator.FeatureSet(
  new storeLocator.Feature('Gluten-YES', 'Corner Sans Gluten'),
  new storeLocator.Feature('Restaurant-YES', 'Restaurant Du Boulanger'),
  new storeLocator.Feature('Rapide-YES', 'Boulangerie Café')
);

/**
 * @return {!storeLocator.FeatureSet}
 */
MedicareDataSource.prototype.getFeatures = function() {
  return this.FEATURES_;
};

/**
 * @private
 * @param {string} csv
 * @return {!Array.<!storeLocator.Store>}
 */
MedicareDataSource.prototype.parse_ = function(csv) {
  var stores = [];
  var rows = csv.split('\n');
  var headings = this.parseRow_(rows[0]);

  for (var i = 1, row; row = rows[i]; i++) {
    row = this.toObject_(headings, this.parseRow_(row));
    var features = new storeLocator.FeatureSet;
    features.add(this.FEATURES_.getById('Gluten-' + row.Gluten));
    features.add(this.FEATURES_.getById('Restaurant-' + row.Restaurant));
    features.add(this.FEATURES_.getById('Rapide-' + row.Rapide));

    var position = new google.maps.LatLng(row.Ycoord, row.Xcoord);

    var shop = this.join_([row.Shp_num_an, row.Shp_centre], ', ');
    var locality = this.join_([row.Postcode, row.City], ' ');

    var name = (row.Country == 'France' && row.Fcilty_nam != 'Beaugrenelle' && row.Fcilty_nam != 'Orly Aéroport' && row.Fcilty_nam != 'Levallois Trebois')
      ? 'Boulangerie '+ row.Fcilty_nam
      : row.Fcilty_nam;
    var props =  {
      title: name,
      address: this.join_([row.Street_add, locality]),
      phone: '<u>Téléphone</u>: '+row.Fcilty_ctc,
      misc: ('Ouverture :' + row.Hrs),
    }
    if(typeof row.website != 'undefined' && row.website != '') props["web"] = '<a href="'+row.website+'">'+row.website+'</a>';
    var store = new storeLocator.Store(row.uuid, position, features, props);
    stores.push(store);
  }
  return stores;
};

/**
 * Joins elements of an array that are non-empty and non-null.
 * @private
 * @param {!Array} arr array of elements to join.
 * @param {string} sep the separator.
 * @return {string}
 */
MedicareDataSource.prototype.join_ = function(arr, sep) {
  var parts = [];
  for (var i = 0, ii = arr.length; i < ii; i++) {
    arr[i] && parts.push(arr[i]);
  }
  return parts.join(sep);
};

/**
 * Very rudimentary CSV parsing - we know how this particular CSV is formatted.
 * IMPORTANT: Don't use this for general CSV parsing!
 * @private
 * @param {string} row
 * @return {Array.<string>}
 */
MedicareDataSource.prototype.parseRow_ = function(row) {
  // Strip leading quote.
  // if (row.charAt(0) == '"') {
  //   row = row.substring(1);
  // }
  // Strip trailing quote. There seems to be a character between the last quote
  // and the line ending, hence 2 instead of 1.
  // if (row.charAt(row.length - 1) == '"') {
  //   row = row.substring(0, row.length - 1);
  // }

  row = row.split(';');

  return row;
};

/**
 * Creates an object mapping headings to row elements.
 * @private
 * @param {Array.<string>} headings
 * @param {Array.<string>} row
 * @return {Object}
 */
MedicareDataSource.prototype.toObject_ = function(headings, row) {
  var result = {};
  for (var i = 0, ii = row.length; i < ii; i++) {
    result[headings[i]] = row[i];
  }
  return result;
};
