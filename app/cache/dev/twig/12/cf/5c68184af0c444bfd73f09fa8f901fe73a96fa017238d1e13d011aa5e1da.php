<?php

/* KayserPlatformBundle:Header:subnav-products.html.twig */
class __TwigTemplate_12cf5c68184af0c444bfd73f09fa8f901fe73a96fa017238d1e13d011aa5e1da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"subnav hidden-sm hidden-xs\">
    <li><a href=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("kay_produits_list", array("category" => "pains", "subcategory" => "all")), "html", null, true);
        echo "\" id=\"subnav-pains\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PAIN", array(), "Menu"), "html", null, true);
        echo "</a></li>
    <ul>
\t    <li><a href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("kay_produits_list", array("category" => "pains", "subcategory" => "levain")), "html", null, true);
        echo "\" id=\"subnav-pains-levain\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("LEVAIN", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("kay_produits_list", array("category" => "pains", "subcategory" => "gluten")), "html", null, true);
        echo "\" id=\"subnav-pains-gluten\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("GLUTEN", array(), "Menu"), "html", null, true);
        echo "</a></li>
    </ul>
    <li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("kay_produits_list", array("category" => "viennoiseries", "subcategory" => "all")), "html", null, true);
        echo "\" id=\"subnav-viennoiseries\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("BRIOCHE", array(), "Menu"), "html", null, true);
        echo "</a></li>
    <br/>
    <li><a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("kay_produits_carte", array("slug" => "patisseries"));
        echo "\" id=\"subnav-patisseries\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PATISSERIE", array(), "Menu"), "html", null, true);
        echo "</a></li>
    <ul>
    \t<li><a href=\"";
        // line 11
        echo $this->env->getExtension('routing')->getPath("kay_produits_carte", array("slug" => "patisseries"));
        echo "\" id=\"subnav-patisseries-classiques\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONSULTER", array(), "Menu"), "html", null, true);
        echo "</a></li>
    </ul>
    <li><a href=\"";
        // line 13
        echo $this->env->getExtension('routing')->getPath("kay_produits_carte", array("slug" => "sales"));
        echo "\" id=\"subnav-sales\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("SALES", array(), "Menu"), "html", null, true);
        echo "</a></li>
    <ul>
    \t<li><a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("kay_produits_carte", array("slug" => "sales"));
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONSULTER", array(), "Menu"), "html", null, true);
        echo "</a></li>
    </ul>
";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Header:subnav-products.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 15,  63 => 13,  56 => 11,  49 => 9,  42 => 7,  35 => 5,  29 => 4,  22 => 2,  19 => 1,);
    }
}
