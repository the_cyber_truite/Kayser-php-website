<?php

/* KayserPlatformBundle:Default:faq.html.twig */
class __TwigTemplate_2f4d88eed5c8029d6e430ef53f74536f11e98710854a3c2c3d3c70e31c574cb2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("KayserPlatformBundle::layout.html.twig", "KayserPlatformBundle:Default:faq.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'platform_body' => array($this, 'block_platform_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "KayserPlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        // line 5
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Information légales
";
    }

    // line 9
    public function block_platform_body($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"container main-container\">
        <div class=\"row\">
            <div class=\"col-md-3\">
                <div data-spy=\"affix\">
                <ol class=\"breadcrumb\">
                  <li><a href=\"#\">";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("HOME", array(), "Menu"), "html", null, true);
        echo "</a></li>
                  <li><a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("kay_faq");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONTACT", array(), "Menu"), "html", null, true);
        echo "</a></li>
                </ol>
                ";
        // line 18
        $this->loadTemplate("KayserPlatformBundle:Header:subnav-contact.html.twig", "KayserPlatformBundle:Default:faq.html.twig", 18)->display($context);
        // line 19
        echo "                </div>
            </div>
            <div class=\"col-md-9\">
                <div class=\"row full tile\" style=\"margin-bottom:74px;\">
                    <div class=\"row col-history-row\">
                        <h3>";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tableFaq"]) ? $context["tableFaq"] : $this->getContext($context, "tableFaq")), 1, array(), "array"), "title", array()), "html", null, true);
        echo "</h3>
                            <div class=\"col-md-10 col-md-offset-2\">
                                <div class=\"row\" style=\"margin:0;text-align:justify\">                      
                                    ";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(2, 11));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 28
            echo "                                        <h4> ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tableFaq"]) ? $context["tableFaq"] : $this->getContext($context, "tableFaq")), $context["i"], array(), "array"), "title", array()), "html", null, true);
            echo " </h4>
                                        <p> ";
            // line 29
            echo $this->getAttribute($this->getAttribute((isset($context["tableFaq"]) ? $context["tableFaq"] : $this->getContext($context, "tableFaq")), $context["i"], array(), "array"), "body", array());
            echo " </p>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 40
    public function block_javascripts($context, array $blocks = array())
    {
        // line 41
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
    jQuery(function(\$) {
        // Affix
        \$('#myAffix').affix({ offset: { top: -100, } });
        // Active
    })
    </script>
";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Default:faq.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 41,  103 => 40,  92 => 31,  84 => 29,  79 => 28,  75 => 27,  69 => 24,  62 => 19,  60 => 18,  53 => 16,  49 => 15,  42 => 10,  39 => 9,  33 => 5,  30 => 4,  11 => 2,);
    }
}
