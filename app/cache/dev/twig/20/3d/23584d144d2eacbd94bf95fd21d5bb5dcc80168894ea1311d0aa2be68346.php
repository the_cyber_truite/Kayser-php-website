<?php

/* KayserPlatformBundle:Default:equipes.html.twig */
class __TwigTemplate_203d23584d144d2eacbd94bf95fd21d5bb5dcc80168894ea1311d0aa2be68346 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 4
        $this->parent = $this->loadTemplate("KayserPlatformBundle::layout.html.twig", "KayserPlatformBundle:Default:equipes.html.twig", 4);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'platform_body' => array($this, 'block_platform_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "KayserPlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        // line 7
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Notre Histoire
";
    }

    // line 11
    public function block_platform_body($context, array $blocks = array())
    {
        // line 12
        echo "    <div class=\"container main-container\">
        <div class=\"row\">
            <div class=\"col-md-3\">
                <div data-spy=\"affix\">
                <ol class=\"breadcrumb\">
                  <li><a href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("kay_homepage");
        echo "\">HOME</a></li>
                  <li><a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("kay_savoirfaire");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("SAVOIR", array(), "Menu"), "html", null, true);
        echo "</a></li>
                  <li><a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("kay_equipes");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("METHODES", array(), "Menu"), "html", null, true);
        echo "</a></li>
                </ol>
                ";
        // line 21
        $this->loadTemplate("KayserPlatformBundle:Header:submenu.html.twig", "KayserPlatformBundle:Default:equipes.html.twig", 21)->display($context);
        // line 22
        echo "                </div>
            </div>
            <div class=\"col-md-9\">
                <div class=\"row full tile\" style=\"margin-bottom:74px;\">
                    <div class=\"col-sm-12 history-col\">
                        <img src=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/kayser-equipe1.png"), "html", null, true);
        echo "\" alt=\"Des membres de l'équipe Kayser\">
                    </div>

                    <div class=\"row col-history-row\">
                        <div class=\"col-sm-12 col-history-content\">
                            <h3> ";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tableEquipes"]) ? $context["tableEquipes"] : $this->getContext($context, "tableEquipes")), 1, array(), "array"), "title", array()), "html", null, true);
        echo " </h3>
                            <p class=\"intro\"></p>
                                <p> ";
        // line 34
        echo $this->getAttribute($this->getAttribute((isset($context["tableEquipes"]) ? $context["tableEquipes"] : $this->getContext($context, "tableEquipes")), 1, array(), "array"), "body", array());
        echo " </p>
                                <h4> ";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tableEquipes"]) ? $context["tableEquipes"] : $this->getContext($context, "tableEquipes")), 2, array(), "array"), "title", array()), "html", null, true);
        echo " </h4>
                                <p> ";
        // line 36
        echo $this->getAttribute($this->getAttribute((isset($context["tableEquipes"]) ? $context["tableEquipes"] : $this->getContext($context, "tableEquipes")), 2, array(), "array"), "body", array());
        echo " </p>
                                <p> ";
        // line 37
        echo $this->getAttribute($this->getAttribute((isset($context["tableEquipes"]) ? $context["tableEquipes"] : $this->getContext($context, "tableEquipes")), 3, array(), "array"), "body", array());
        echo " </p>
                                <p> ";
        // line 38
        echo $this->getAttribute($this->getAttribute((isset($context["tableEquipes"]) ? $context["tableEquipes"] : $this->getContext($context, "tableEquipes")), 4, array(), "array"), "body", array());
        echo " </p>
                                <h4> ";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tableEquipes"]) ? $context["tableEquipes"] : $this->getContext($context, "tableEquipes")), 5, array(), "array"), "title", array()), "html", null, true);
        echo " </h4>
                                <p> ";
        // line 40
        echo $this->getAttribute($this->getAttribute((isset($context["tableEquipes"]) ? $context["tableEquipes"] : $this->getContext($context, "tableEquipes")), 5, array(), "array"), "body", array());
        echo " </p>
                            <p class=\"border\"></p>
                                ";
        // line 42
        echo $this->getAttribute($this->getAttribute((isset($context["tableEquipes"]) ? $context["tableEquipes"] : $this->getContext($context, "tableEquipes")), 6, array(), "array"), "body", array());
        echo "
                        </div>
                    </div>

                </div>

                <div class=\"row\">
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 50
        echo $this->env->getExtension('routing')->getPath("kay_histoire");
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfFeature"]) ? $context["sfFeature"] : $this->getContext($context, "sfFeature")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"Monsieur Eric Kayser\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 53
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfFeature"]) ? $context["sfFeature"] : $this->getContext($context, "sfFeature")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 54
        echo $this->getAttribute((isset($context["sfFeature"]) ? $context["sfFeature"] : $this->getContext($context, "sfFeature")), "body", array());
        echo "</p>
                                ";
        // line 56
        echo "                            </div>
                        </div></a>
                    </div>
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 60
        echo $this->env->getExtension('routing')->getPath("kay_philosophie");
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfUn"]) ? $context["sfUn"] : $this->getContext($context, "sfUn")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"Du bon pain de qualité !\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 63
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfUn"]) ? $context["sfUn"] : $this->getContext($context, "sfUn")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 64
        echo $this->getAttribute((isset($context["sfUn"]) ? $context["sfUn"] : $this->getContext($context, "sfUn")), "body", array());
        echo "</p>
                                ";
        // line 66
        echo "                            </div>
                        </div></a>
                    </div>
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 70
        echo $this->env->getExtension('routing')->getPath("kay_methodes");
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfDeux"]) ? $context["sfDeux"] : $this->getContext($context, "sfDeux")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"Kayser, nos méthodes\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 73
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfDeux"]) ? $context["sfDeux"] : $this->getContext($context, "sfDeux")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 74
        echo $this->getAttribute((isset($context["sfDeux"]) ? $context["sfDeux"] : $this->getContext($context, "sfDeux")), "body", array());
        echo "</p>
                                ";
        // line 76
        echo "                            </div>
                        </div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 85
    public function block_javascripts($context, array $blocks = array())
    {
        // line 86
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
    jQuery(function(\$) {
        // Affix
        \$('#myAffix').affix({ offset: { top: -100, } });
        // Active
        \$('#gnav-savoirfaire, #dropnav-equipes, #subnav-equipes').addClass('active');
    })
    </script>
";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Default:equipes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  205 => 86,  202 => 85,  191 => 76,  187 => 74,  183 => 73,  178 => 71,  174 => 70,  168 => 66,  164 => 64,  160 => 63,  155 => 61,  151 => 60,  145 => 56,  141 => 54,  137 => 53,  132 => 51,  128 => 50,  117 => 42,  112 => 40,  108 => 39,  104 => 38,  100 => 37,  96 => 36,  92 => 35,  88 => 34,  83 => 32,  75 => 27,  68 => 22,  66 => 21,  59 => 19,  53 => 18,  49 => 17,  42 => 12,  39 => 11,  33 => 7,  30 => 6,  11 => 4,);
    }
}
