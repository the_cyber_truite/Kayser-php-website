<?php

/* KayserPlatformBundle:Default:produits.html.twig */
class __TwigTemplate_f84c78e116eb282968d81c05868ee6812420e966851d598660736bb2c8481ec6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("KayserPlatformBundle::layout.html.twig", "KayserPlatformBundle:Default:produits.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'platform_body' => array($this, 'block_platform_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "KayserPlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        // line 5
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Notre Savoir-Faire
";
    }

    // line 8
    public function block_platform_body($context, array $blocks = array())
    {
        // line 9
        echo "    <div class=\"container main-container\">
        <div class=\"row\">
            <div class=\"col-md-3\">
                <div data-spy=\"affix\">
                <ol class=\"breadcrumb\">
                  <li><a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("kay_homepage");
        echo "\">HOME</a></li>
                  <li><a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("kay_produits");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PRODUITS", array(), "Menu"), "html", null, true);
        echo "</a></li>
                </ol>
                ";
        // line 17
        $this->loadTemplate("KayserPlatformBundle:Header:subnav-products.html.twig", "KayserPlatformBundle:Default:produits.html.twig", 17)->display($context);
        // line 18
        echo "                </div>
            </div>
            <div class=\"col-md-9\">
                <a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("kay_produits_list", array("category" => "pains"));
        echo "\" class=\"tile-link\"><div class=\"row feature tile over\" style=\"margin-bottom:14px;\">
                    <div class=\"col-sm-4\">
                        <h3>";
        // line 23
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfFeature"]) ? $context["sfFeature"] : $this->getContext($context, "sfFeature")), "title", array())), "html", null, true);
        echo "</h3>
                        <p>";
        // line 24
        echo $this->getAttribute((isset($context["sfFeature"]) ? $context["sfFeature"] : $this->getContext($context, "sfFeature")), "body", array());
        echo "</p>
                        ";
        // line 26
        echo "                    </div>
                    <div class=\"col-sm-8 feature hero\">
                        <img src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfFeature"]) ? $context["sfFeature"] : $this->getContext($context, "sfFeature")), "image", array()), "webPath", array()), "kay_featured_bloc"), "html", null, true);
        echo "\" alt=\"Nos bon pain\">
                    </div>
                </div></a>
                <div class=\"row\">
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("kay_produits_list", array("category" => "viennoiseries"));
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfUn"]) ? $context["sfUn"] : $this->getContext($context, "sfUn")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"Nos bonnes chouquettes - Viennoiseries et brioches\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 36
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfUn"]) ? $context["sfUn"] : $this->getContext($context, "sfUn")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 37
        echo $this->getAttribute((isset($context["sfUn"]) ? $context["sfUn"] : $this->getContext($context, "sfUn")), "body", array());
        echo "</p>
                                ";
        // line 39
        echo "                            </div>
                        </div></a>
                    </div>
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 43
        echo $this->env->getExtension('routing')->getPath("kay_produits_carte", array("slug" => "patisseries"));
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfDeux"]) ? $context["sfDeux"] : $this->getContext($context, "sfDeux")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"Nos pâtisseries\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 46
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfDeux"]) ? $context["sfDeux"] : $this->getContext($context, "sfDeux")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 47
        echo $this->getAttribute((isset($context["sfDeux"]) ? $context["sfDeux"] : $this->getContext($context, "sfDeux")), "body", array());
        echo "</p>
                                ";
        // line 49
        echo "                            </div>
                        </div></a>
                    </div>
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 53
        echo $this->env->getExtension('routing')->getPath("kay_produits_carte", array("slug" => "sales"));
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfTrois"]) ? $context["sfTrois"] : $this->getContext($context, "sfTrois")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"Les salés - Sandwich et autres\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 56
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfTrois"]) ? $context["sfTrois"] : $this->getContext($context, "sfTrois")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 57
        echo $this->getAttribute((isset($context["sfTrois"]) ? $context["sfTrois"] : $this->getContext($context, "sfTrois")), "body", array());
        echo "</p>
                                ";
        // line 59
        echo "                            </div>
                        </div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 68
    public function block_javascripts($context, array $blocks = array())
    {
        // line 69
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
    jQuery(function(\$) {
        // Affix
        \$('#myAffix').affix({ offset: { top: -100, } });
        // Active
        \$('#gnav-produits').addClass('active');
    })
    </script>
";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Default:produits.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 69,  166 => 68,  155 => 59,  151 => 57,  147 => 56,  142 => 54,  138 => 53,  132 => 49,  128 => 47,  124 => 46,  119 => 44,  115 => 43,  109 => 39,  105 => 37,  101 => 36,  96 => 34,  92 => 33,  84 => 28,  80 => 26,  76 => 24,  72 => 23,  67 => 21,  62 => 18,  60 => 17,  53 => 15,  49 => 14,  42 => 9,  39 => 8,  33 => 5,  30 => 4,  11 => 2,);
    }
}
