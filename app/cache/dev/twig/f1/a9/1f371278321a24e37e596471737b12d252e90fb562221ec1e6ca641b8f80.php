<?php

/* KayserPlatformBundle:Default:philosophie.html.twig */
class __TwigTemplate_f1a91f371278321a24e37e596471737b12d252e90fb562221ec1e6ca641b8f80 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("KayserPlatformBundle::layout.html.twig", "KayserPlatformBundle:Default:philosophie.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'platform_body' => array($this, 'block_platform_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "KayserPlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        // line 5
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Notre Histoire
";
    }

    // line 9
    public function block_platform_body($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"container main-container\">
        <div class=\"row\">
            <div class=\"col-md-3\">
                <div data-spy=\"affix\">
                <ol class=\"breadcrumb\">
                  <li><a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("kay_homepage");
        echo "\">HOME</a></li>
                  <li><a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("kay_savoirfaire");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("SAVOIR", array(), "Menu"), "html", null, true);
        echo "</a></li>
                  <li><a href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("kay_philosophie");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PHILOSOPHIES", array(), "Menu"), "html", null, true);
        echo "</a></li>
                </ol>
                ";
        // line 19
        $this->loadTemplate("KayserPlatformBundle:Header:submenu.html.twig", "KayserPlatformBundle:Default:philosophie.html.twig", 19)->display($context);
        // line 20
        echo "                </div>
            </div>
            <div class=\"col-md-9\">
                <div class=\"row full tile\" style=\"margin-bottom:74px;\">
                    ";
        // line 27
        echo "                    <div class=\"col-sm-12 history-col\">
                        <img src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/philo-feature.png"), "html", null, true);
        echo "\" alt=\"Fabrication de pain\">
                    </div>
                    <div class=\"row col-history-row\">
                        <div class=\"col-sm-12 col-history-content\">
                            <h3> ";
        // line 32
        echo $this->getAttribute($this->getAttribute((isset($context["tablePhilo"]) ? $context["tablePhilo"] : $this->getContext($context, "tablePhilo")), 1, array(), "array"), "title", array());
        echo " </h3>
                            <p class=\"intro\"> ";
        // line 33
        echo $this->getAttribute($this->getAttribute((isset($context["tablePhilo"]) ? $context["tablePhilo"] : $this->getContext($context, "tablePhilo")), 1, array(), "array"), "body", array());
        echo " </p>
                            <h4>  ";
        // line 34
        echo $this->getAttribute($this->getAttribute((isset($context["tablePhilo"]) ? $context["tablePhilo"] : $this->getContext($context, "tablePhilo")), 2, array(), "array"), "title", array());
        echo " </h4>
                            <p> ";
        // line 35
        echo $this->getAttribute($this->getAttribute((isset($context["tablePhilo"]) ? $context["tablePhilo"] : $this->getContext($context, "tablePhilo")), 2, array(), "array"), "body", array());
        echo " </p>
                            <p class=\"cite\"> ";
        // line 36
        echo $this->getAttribute($this->getAttribute((isset($context["tablePhilo"]) ? $context["tablePhilo"] : $this->getContext($context, "tablePhilo")), 3, array(), "array"), "body", array());
        echo " </p>
                            <p> ";
        // line 37
        echo $this->getAttribute($this->getAttribute((isset($context["tablePhilo"]) ? $context["tablePhilo"] : $this->getContext($context, "tablePhilo")), 4, array(), "array"), "body", array());
        echo " </p>
                            <h4>  ";
        // line 38
        echo $this->getAttribute($this->getAttribute((isset($context["tablePhilo"]) ? $context["tablePhilo"] : $this->getContext($context, "tablePhilo")), 5, array(), "array"), "title", array());
        echo " </h4>
                            <p> ";
        // line 39
        echo $this->getAttribute($this->getAttribute((isset($context["tablePhilo"]) ? $context["tablePhilo"] : $this->getContext($context, "tablePhilo")), 5, array(), "array"), "body", array());
        echo " </p>
                            <p> ";
        // line 40
        echo $this->getAttribute($this->getAttribute((isset($context["tablePhilo"]) ? $context["tablePhilo"] : $this->getContext($context, "tablePhilo")), 6, array(), "array"), "body", array());
        echo " </p>
                            <br />
                            <div class=\"row\">
                                <div class=\"col-sm-3 history-caption\">
                                    <p> ";
        // line 44
        echo $this->getAttribute($this->getAttribute((isset($context["tablePhilo"]) ? $context["tablePhilo"] : $this->getContext($context, "tablePhilo")), 7, array(), "array"), "body", array());
        echo " </p>
                                </div>
                                <div class=\"col-sm-9 history-pictures-one\">
                                    <img src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/kayser-philo4.png"), "html", null, true);
        echo "\" alt=\"Fabrication de pain\">
                                </div>
                            </div>
                            <p> ";
        // line 50
        echo $this->getAttribute($this->getAttribute((isset($context["tablePhilo"]) ? $context["tablePhilo"] : $this->getContext($context, "tablePhilo")), 8, array(), "array"), "body", array());
        echo " </p>
                            <p> ";
        // line 51
        echo $this->getAttribute($this->getAttribute((isset($context["tablePhilo"]) ? $context["tablePhilo"] : $this->getContext($context, "tablePhilo")), 9, array(), "array"), "body", array());
        echo " </p>
                            <p class=\"border\">&nbsp;</p>
                            ";
        // line 53
        echo $this->getAttribute($this->getAttribute((isset($context["tablePhilo"]) ? $context["tablePhilo"] : $this->getContext($context, "tablePhilo")), 10, array(), "array"), "body", array());
        echo "
                        </div>
                    </div>

                </div>

                <div class=\"row\">
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 61
        echo $this->env->getExtension('routing')->getPath("kay_histoire");
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfFeature"]) ? $context["sfFeature"] : $this->getContext($context, "sfFeature")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"Monsieur Kayser - Notre histoire\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 64
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfFeature"]) ? $context["sfFeature"] : $this->getContext($context, "sfFeature")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 65
        echo $this->getAttribute((isset($context["sfFeature"]) ? $context["sfFeature"] : $this->getContext($context, "sfFeature")), "body", array());
        echo "</p>
                                ";
        // line 67
        echo "                            </div>
                        </div></a>
                    </div>
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 71
        echo $this->env->getExtension('routing')->getPath("kay_methodes");
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfDeux"]) ? $context["sfDeux"] : $this->getContext($context, "sfDeux")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"Etagère - Nos méthodes\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 74
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfDeux"]) ? $context["sfDeux"] : $this->getContext($context, "sfDeux")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 75
        echo $this->getAttribute((isset($context["sfDeux"]) ? $context["sfDeux"] : $this->getContext($context, "sfDeux")), "body", array());
        echo "</p>
                                ";
        // line 77
        echo "                            </div>
                        </div></a>
                    </div>
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 81
        echo $this->env->getExtension('routing')->getPath("kay_equipes");
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfTrois"]) ? $context["sfTrois"] : $this->getContext($context, "sfTrois")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"Nos compagnons - Notre équipe\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 84
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfTrois"]) ? $context["sfTrois"] : $this->getContext($context, "sfTrois")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 85
        echo $this->getAttribute((isset($context["sfTrois"]) ? $context["sfTrois"] : $this->getContext($context, "sfTrois")), "body", array());
        echo "</p>
                                ";
        // line 87
        echo "                            </div>
                        </div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 96
    public function block_javascripts($context, array $blocks = array())
    {
        // line 97
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
    jQuery(function(\$) {
        // Affix
        \$('#myAffix').affix({ offset: { top: -100, } });
        // Active
        \$('#gnav-savoirfaire, #dropnav-philosophie, #subnav-philosophie').addClass('active');
    })
    </script>
";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Default:philosophie.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  232 => 97,  229 => 96,  218 => 87,  214 => 85,  210 => 84,  205 => 82,  201 => 81,  195 => 77,  191 => 75,  187 => 74,  182 => 72,  178 => 71,  172 => 67,  168 => 65,  164 => 64,  159 => 62,  155 => 61,  144 => 53,  139 => 51,  135 => 50,  129 => 47,  123 => 44,  116 => 40,  112 => 39,  108 => 38,  104 => 37,  100 => 36,  96 => 35,  92 => 34,  88 => 33,  84 => 32,  77 => 28,  74 => 27,  68 => 20,  66 => 19,  59 => 17,  53 => 16,  49 => 15,  42 => 10,  39 => 9,  33 => 5,  30 => 4,  11 => 2,);
    }
}
