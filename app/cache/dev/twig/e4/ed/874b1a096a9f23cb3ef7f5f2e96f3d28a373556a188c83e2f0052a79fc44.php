<?php

/* KayserPlatformBundle:Footer:footer.html.twig */
class __TwigTemplate_e4ed874b1a096a9f23cb3ef7f5f2e96f3d28a373556a188c83e2f0052a79fc44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer>
  <div class=\"footer show\">
    <div class=\"container\">
      <ul class=\"footer-nav hidden-sm hidden-xs\">
        <li><a href=\"#\">";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("rejoindre", array(), "PiedDePage"), "html", null, true);
        echo "</a></li>
        <li><a href=\"#\">";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("contacter", array(), "PiedDePage"), "html", null, true);
        echo "</a></li>
        <li><a href=\"#\">";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("presse", array(), "PiedDePage"), "html", null, true);
        echo "</a></li>
        <li><a href=\"#\">";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("client", array(), "PiedDePage"), "html", null, true);
        echo "</a></li>
        <li><a href=\"#\">";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("information", array(), "PiedDePage"), "html", null, true);
        echo "</a></li>
      </ul>

      <div class=\"small-link\">
        <ul class=\"footer-sm-nav visible-sm visible-xs\">
          <li><a style=\"cursor:pointer\">Liens rapides</a></li>
          <br/>
        </ul>
      </div>

      <div class=\"visible-sm visible-xs\">
          <ul class=\"footer-small\">
            <li><a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("kay_candidature");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("candidature", array(), "PiedDePage"), "html", null, true);
        echo "</a></li>
            <li><a href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("kay_annonces");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("annonce", array(), "PiedDePage"), "html", null, true);
        echo "</a></li>
            <li><a href=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("kay_contact");
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("information", array(), "PiedDePage"), "html", null, true);
        echo " </a></li>
            <li><a href=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("kay_faq");
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("faq", array(), "PiedDePage"), "html", null, true);
        echo " </a></li>
            <li><a href=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath("kay_clients");
        echo "\" id=\"subnav-pains\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("hotel", array(), "PiedDePage"), "html", null, true);
        echo " </a></li>
            <li><a href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("kay_clients", array("slug" => "entreprises"));
        echo " \" id=\"subnav-pains\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("entreprise", array(), "PiedDePage"), "html", null, true);
        echo " </a></li>
          </ul>
      </div>

      <div class=\"row footer-links\">
        <div class=\"col-20p\">
          <ul class=\"footer-links hidden-sm hidden-xs\">
            <li><a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("kay_candidature");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("candidature", array(), "PiedDePage"), "html", null, true);
        echo "</a></li>
            <li><a href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("kay_annonces");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("annonce", array(), "PiedDePage"), "html", null, true);
        echo "</a></li>
          </ul>
        </div>
        <div class=\"col-20p\">
          <ul class=\"footer-links hidden-sm hidden-xs\">
            <li><a href=\"";
        // line 39
        echo $this->env->getExtension('routing')->getPath("kay_contact");
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("information", array(), "PiedDePage"), "html", null, true);
        echo " </a></li>
            <li><a href=\"";
        // line 40
        echo $this->env->getExtension('routing')->getPath("kay_faq");
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("faq", array(), "PiedDePage"), "html", null, true);
        echo " </a></li>
          </ul>
        </div>
        <div class=\"col-20p hidden-sm hidden-xs\">
          <div class=\"col-md-10\" style=\"padding-left:0;\"> 
              ";
        // line 45
        echo $this->env->getExtension('translator')->trans("texte_presse", array(), "PiedDePage");
        echo "
          </div>
        </div>
        <div class=\"col-20p\">
          <ul class=\"footer-links hidden-sm hidden-xs\">
              <li><a href=\"";
        // line 50
        echo $this->env->getExtension('routing')->getPath("kay_clients");
        echo "\" id=\"subnav-pains\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("hotel", array(), "PiedDePage"), "html", null, true);
        echo " </a></li>
              <li><a href=\"";
        // line 51
        echo $this->env->getExtension('routing')->getPath("kay_clients", array("slug" => "entreprises"));
        echo " \" id=\"subnav-pains\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("entreprise", array(), "PiedDePage"), "html", null, true);
        echo " </a></li>
          </ul>
        </div>
        <div class=\"col-20p hidden-sm hidden-xs\">
          <div class=\"col-md-10\" style=\"padding-left:0;\">";
        // line 55
        echo $this->env->getExtension('translator')->trans("adresse", array(), "PiedDePage");
        echo "
          <a href=\"";
        // line 56
        echo $this->env->getExtension('routing')->getPath("kay_credits");
        echo "\" > ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("adresse_suivante", array(), "PiedDePage"), "html", null, true);
        echo " </a>
          </div>
        </div>
      </div>
      <div class=\"footer-sm-social visible-sm visible-xs\">
        <ul>
          <li><a href=\"https://www.facebook.com/MaisonKayser\"><span class=\"nav-social nav-social-facebook\"></span></a></li>
          <li><a href=\"https://twitter.com/maison_ek\"><span class=\"nav-social nav-social-twitter\"></span></a></li>
          <li><a href=\"https://plus.google.com/102660033434943800028/about\"><span class=\"nav-social nav-social-google\"></span></a></li>
          <li><a href=\"https://www.pinterest.com/maisonkayser/\"><span class=\"nav-social nav-social-pinterest\"></span></a></li>
          <li><a href=\"http://instagram.com/maisonkayser\"><span class=\"nav-social nav-social-instagram\"></span></a></li>
        </ul>
      </div>
    </div>
    <div id=\"close-footer-block\">
      <a id=\"close-footer\" href=\"#\" class=\"black\"><span>&times;</span></a>
    </div>
  </div>
</footer>";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Footer:footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  157 => 56,  153 => 55,  144 => 51,  138 => 50,  130 => 45,  120 => 40,  114 => 39,  104 => 34,  98 => 33,  86 => 26,  80 => 25,  74 => 24,  68 => 23,  62 => 22,  56 => 21,  41 => 9,  37 => 8,  33 => 7,  29 => 6,  25 => 5,  19 => 1,);
    }
}
