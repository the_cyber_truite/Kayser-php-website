<?php

/* KayserPlatformBundle:Header:subnav-contact.html.twig */
class __TwigTemplate_2efcda6f590c9376629875ef0976bed512e5fbebe08c7e40f128985838b1bcff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "\t<ul class=\"subnav hidden-sm hidden-xs\">
\t    <li><a href=\"";
        // line 2
        echo $this->env->getExtension('routing')->getPath("kay_contact");
        echo " \" id=\"subnav-pains\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONTACT", array(), "Menu"), "html", null, true);
        echo "</a></li>
\t    <li><a href=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("kay_faq");
        echo " \" id=\"subnav-pains\">FAQ</a></li>
\t</ul>
";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Header:subnav-contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 2,  19 => 1,);
    }
}
