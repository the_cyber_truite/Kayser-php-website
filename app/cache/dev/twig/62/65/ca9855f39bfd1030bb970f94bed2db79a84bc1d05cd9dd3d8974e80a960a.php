<?php

/* ::base.html.twig */
class __TwigTemplate_6265ca9855f39bfd1030bb970f94bed2db79a84bc1d05cd9dd3d8974e80a960a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'topmenu' => array($this, 'block_topmenu'),
            'headerhero' => array($this, 'block_headerhero'),
            'menu' => array($this, 'block_menu'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"fr\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
    <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 11
        echo "    ";
        // line 12
        echo "    ";
        // line 13
        echo "    <link rel=\"shortcut icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "?v=2\" />
    <link href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/icheck/skins/flat/red.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js\"></script>

</head>
<body>
  ";
        // line 19
        $this->displayBlock('topmenu', $context, $blocks);
        // line 24
        echo "
  ";
        // line 25
        $this->displayBlock('headerhero', $context, $blocks);
        // line 28
        echo "
  ";
        // line 29
        $this->displayBlock('menu', $context, $blocks);
        // line 32
        echo "
    
  ";
        // line 34
        $this->displayBlock('body', $context, $blocks);
        // line 36
        echo "
  ";
        // line 37
        $this->displayBlock('footer', $context, $blocks);
        // line 40
        echo "
    

    ";
        // line 43
        $this->displayBlock('javascripts', $context, $blocks);
        // line 161
        echo "
</body>

</html>";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "Maison Kayser";
    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 8
        echo "        <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("dist/css/bootstrap.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/kayser.css"), "html", null, true);
        echo "\">
    ";
    }

    // line 19
    public function block_topmenu($context, array $blocks = array())
    {
        // line 20
        echo "    ";
        $this->loadTemplate("KayserPlatformBundle:Header:topmenu.html.twig", "::base.html.twig", 20)->display($context);
        // line 21
        echo "    ";
        echo $this->env->getExtension('sonata_seo')->getTitle();
        echo "
    ";
        // line 22
        echo $this->env->getExtension('sonata_seo')->getMetadatas();
        echo "
  ";
    }

    // line 25
    public function block_headerhero($context, array $blocks = array())
    {
        // line 26
        echo "    ";
        $this->loadTemplate("KayserPlatformBundle:Header:headerhero.html.twig", "::base.html.twig", 26)->display($context);
        // line 27
        echo "  ";
    }

    // line 29
    public function block_menu($context, array $blocks = array())
    {
        // line 30
        echo "    ";
        $this->loadTemplate("KayserPlatformBundle:Header:menu.html.twig", "::base.html.twig", 30)->display($context);
        // line 31
        echo "  ";
    }

    // line 34
    public function block_body($context, array $blocks = array())
    {
        // line 35
        echo "  ";
    }

    // line 37
    public function block_footer($context, array $blocks = array())
    {
        // line 38
        echo "    ";
        $this->loadTemplate("KayserPlatformBundle:Footer:footer.html.twig", "::base.html.twig", 38)->display($context);
        // line 39
        echo "  ";
    }

    // line 43
    public function block_javascripts($context, array $blocks = array())
    {
        // line 44
        echo "        <script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js\"></script>
        <script src=\"";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("dist/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.scrollTo.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("ckeditor/ckeditor.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("ckeditor/config.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("ckeditor/styles.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.parallax-1.1.3.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/icheck/icheck.js"), "html", null, true);
        echo "\"></script>

        <!--<script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-XXXXXXX-1', 'auto');
          ga('send', 'pageview');
        </script>-->
        <script>
          jQuery(function(\$){
            if(\$(document).width() < 991){
              var footerHeight = 50;
            }else{
              var footerHeight = 30;
            }

            // Footer
            \$('.footer-sm-nav > li, .footer-nav > li > a').on('click', function(e){
              e.preventDefault();
              if(\$(document).width() < 991){
                var hauteur = \$(document).height() + 179 + 100;
              }else{
                var hauteur = \$(document).height() + 179 + 160;
              }
              \$(window).delay(100).scrollTo( { top:hauteur, left:0}, 800 );
              \$('div.footer-links').css({'height': \"auto\"});
              var newHeight = \$('.footer').find('div.container').height();
              \$('.footer').css({'height': newHeight + 20});
              \$('#close-footer').css('margin-top', '-'+(newHeight)+'px');
              \$('#close-footer').show();
            });
            \$('#close-footer').on('click', function(e) {
              e.preventDefault();
              \$(this).hide();
              \$('div.footer-links').css({'height': 0});
              \$('.footer').css({'height': footerHeight+'px'});
            });

            // Affix
            // \$('#myAffix').affix({
            //   offset: {
            //     top: 100,
            //   }
            // });
            \$(\"#myAffix\").on('affixed.bs.affix', function(){
              \$('.main-container').addClass('boom');
            });
            \$(\"#myAffix\").on('affixed-top.bs.affix', function(){
              \$('.main-container').removeClass('boom');
            });

            // Footer FIX
            var hauteurmainct = \$('.main-container').height();
            var hauteurfenetre = \$(window).height();
            // var boom = hauteurfenetre - (hauteurmainct + 201) + 60;
            var boom = hauteurfenetre - (hauteurmainct + 137 + 60) + 30;
            // console.log(hauteurmainct +' - '+ hauteurfenetre + 'boom : ' + boom);
            if (boom > 60) {
              \$('.main-container').css(\"margin-bottom\", boom);
            }
            \$(window).resize(function() {
              var hauteurmainct = \$('.main-container').height();
              var hauteurfenetre = \$(window).height();
              var boom = hauteurfenetre - (hauteurmainct + 137 + 60) + 30;
              if (boom > 60) {
                \$('.main-container').css(\"margin-bottom\", boom);
              }
            });

            // Menu mobile
            \$('#link-for-sm-nav').on('click', function(e) {
              e.preventDefault();
              \$('#nav-for-small').toggle();
            });

          //   \$(window).scroll(function(){  
          //       if(\$(document).scrollTop() > 100)
          //       {    
          //           if(!\$('.footer').hasClass(\"show\")){
          //           \$('.footer').css({'height': footerHeight+'px'});
          //             \$('.footer').addClass(\"show\");
          //           }
          //       }
          //       else
          //       {
          //           if(\$('.footer').hasClass(\"show\"))  \$('.footer').removeClass(\"show\");
          //           \$('.footer').css({'height': 0});
          //       }
          //   });
          // });

            \$(window).scroll(function(){  
                if(\$(document).scrollTop() > 0)
                {    
                    if(!\$('.footer').hasClass(\"show\")){
                    \$('.footer').css({'height': footerHeight+'px'});
                      \$('.footer').addClass(\"show\");
                    }
                }
            });
          });
          
          \$('body').on('touchstart.dropdown', '.dropdown-menu', function (e) { 
              e.stopPropagation(); 
          });
        </script>
    ";
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 51,  194 => 50,  190 => 49,  186 => 48,  182 => 47,  178 => 46,  174 => 45,  171 => 44,  168 => 43,  164 => 39,  161 => 38,  158 => 37,  154 => 35,  151 => 34,  147 => 31,  144 => 30,  141 => 29,  137 => 27,  134 => 26,  131 => 25,  125 => 22,  120 => 21,  117 => 20,  114 => 19,  108 => 9,  103 => 8,  100 => 7,  94 => 6,  87 => 161,  85 => 43,  80 => 40,  78 => 37,  75 => 36,  73 => 34,  69 => 32,  67 => 29,  64 => 28,  62 => 25,  59 => 24,  57 => 19,  49 => 14,  44 => 13,  42 => 12,  40 => 11,  38 => 7,  34 => 6,  27 => 1,);
    }
}
