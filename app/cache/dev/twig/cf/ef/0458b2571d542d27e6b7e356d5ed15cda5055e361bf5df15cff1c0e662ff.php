<?php

/* KayserPlatformBundle:Default:histoire.html.twig */
class __TwigTemplate_cfef0458b2571d542d27e6b7e356d5ed15cda5055e361bf5df15cff1c0e662ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("KayserPlatformBundle::layout.html.twig", "KayserPlatformBundle:Default:histoire.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'platform_body' => array($this, 'block_platform_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "KayserPlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        // line 5
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Notre Histoire
";
    }

    // line 9
    public function block_platform_body($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"container main-container\">
        <div class=\"row\">
            <div class=\"col-md-3\">
                <div data-spy=\"affix\">
                <ol class=\"breadcrumb\">
                  <li><a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("kay_homepage");
        echo "\">HOME</a></li>
                  <li><a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("kay_savoirfaire");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("SAVOIR", array(), "Menu"), "html", null, true);
        echo "</a></li>
                  <li><a href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("kay_histoire");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("HISTOIRES", array(), "Menu"), "html", null, true);
        echo "</a></li>
                </ol>
                ";
        // line 19
        $this->loadTemplate("KayserPlatformBundle:Header:submenu.html.twig", "KayserPlatformBundle:Default:histoire.html.twig", 19)->display($context);
        // line 20
        echo "                </div>
            </div>
            <div class=\"col-md-9\">
                <div class=\"row full tile\" style=\"margin-bottom:74px;\">
                    <div class=\"col-sm-9 history-col\">
                        <img src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/histoire1-kayser.png"), "html", null, true);
        echo "\" alt=\"Monsieur Eric Kayser\">
                    </div>
                    <div class=\"col-sm-3 history-col\">
                        <img src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/histoire2-kayser.jpg"), "html", null, true);
        echo "\" alt=\"Du bon pain !\">
                        <img src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/histoire3-kayser.jpg"), "html", null, true);
        echo "\" alt=\"Les chouquettes de Kayser, c'est chouette !\">
                    </div>

                    <div class=\"row col-history-row\">
                        <div class=\"col-sm-12 col-history-content\">
                            <h3> ";
        // line 34
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 1, array(), "array"), "title", array())), "html", null, true);
        echo " </h3>
                            <p class=\"intro\"> ";
        // line 35
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 1, array(), "array"), "body", array());
        echo " </p>
                            <h4> ";
        // line 36
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 2, array(), "array"), "title", array());
        echo " </h4>
                            <p> ";
        // line 37
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 2, array(), "array"), "body", array());
        echo " </p>
                            <p> ";
        // line 38
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 3, array(), "array"), "body", array());
        echo " </p>
                            <p class=\"cite\"> ";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 4, array(), "array"), "body", array()), "html", null, true);
        echo " </p>
                            <p> ";
        // line 40
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 5, array(), "array"), "body", array());
        echo " </p>
                            <h4> ";
        // line 41
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 6, array(), "array"), "title", array());
        echo " </h4>
                            <p> ";
        // line 42
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 6, array(), "array"), "body", array());
        echo " </p>
                            <p> ";
        // line 43
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 7, array(), "array"), "body", array());
        echo " </p>
                            <div class=\"row\">
                                <div class=\"col-sm-3 history-caption\" id=\"fermentolevain\">
                                    <p> ";
        // line 46
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 8, array(), "array"), "body", array());
        echo " </p>                       
                                </div>
                                <div class=\"col-sm-1 history-caption\">
                                </div>
                                <div class=\"col-sm-21 history-pictures\">
                                    <img src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/kayser-histoire3.png"), "html", null, true);
        echo "\" alt=\"Machine fermentolevain\">
                                </div>
                            </div>
                                    <p> ";
        // line 54
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 9, array(), "array"), "body", array());
        echo " </p>
                                    <p> ";
        // line 55
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 10, array(), "array"), "body", array());
        echo " </p>
                                    <h4> ";
        // line 56
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 11, array(), "array"), "title", array());
        echo " </h4>
                                    <p> ";
        // line 57
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 11, array(), "array"), "body", array());
        echo " </p>
                                    <p> ";
        // line 58
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 12, array(), "array"), "body", array());
        echo " </p>
                                    <p> ";
        // line 59
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 13, array(), "array"), "body", array());
        echo " </p>
                                    <p> ";
        // line 60
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 14, array(), "array"), "body", array());
        echo " </p> 
                            <p class=\"border\">&nbsp;</p>
                         ";
        // line 62
        echo $this->getAttribute($this->getAttribute((isset($context["tableHistoire"]) ? $context["tableHistoire"] : $this->getContext($context, "tableHistoire")), 15, array(), "array"), "body", array());
        echo "
                        </div>
                    </div>
                </div>
                <div class=\"row\">
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 68
        echo $this->env->getExtension('routing')->getPath("kay_philosophie");
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfUn"]) ? $context["sfUn"] : $this->getContext($context, "sfUn")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sfUn"]) ? $context["sfUn"] : $this->getContext($context, "sfUn")), "title", array()), "html", null, true);
        echo "\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 71
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfUn"]) ? $context["sfUn"] : $this->getContext($context, "sfUn")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 72
        echo $this->getAttribute((isset($context["sfUn"]) ? $context["sfUn"] : $this->getContext($context, "sfUn")), "body", array());
        echo "</p>
                                ";
        // line 74
        echo "                            </div>
                        </div></a>
                    </div>
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 78
        echo $this->env->getExtension('routing')->getPath("kay_methodes");
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfDeux"]) ? $context["sfDeux"] : $this->getContext($context, "sfDeux")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sfDeux"]) ? $context["sfDeux"] : $this->getContext($context, "sfDeux")), "title", array()), "html", null, true);
        echo "\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 81
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfDeux"]) ? $context["sfDeux"] : $this->getContext($context, "sfDeux")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 82
        echo $this->getAttribute((isset($context["sfDeux"]) ? $context["sfDeux"] : $this->getContext($context, "sfDeux")), "body", array());
        echo "</p>
                                ";
        // line 84
        echo "                            </div>
                        </div></a>
                    </div>
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 88
        echo $this->env->getExtension('routing')->getPath("kay_equipes");
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfTrois"]) ? $context["sfTrois"] : $this->getContext($context, "sfTrois")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["sfTrois"]) ? $context["sfTrois"] : $this->getContext($context, "sfTrois")), "title", array()), "html", null, true);
        echo "\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 91
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfTrois"]) ? $context["sfTrois"] : $this->getContext($context, "sfTrois")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 92
        echo $this->getAttribute((isset($context["sfTrois"]) ? $context["sfTrois"] : $this->getContext($context, "sfTrois")), "body", array());
        echo "</p>
                                ";
        // line 94
        echo "                            </div>
                        </div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style type=\"text/css\">
    .tile h3, .tile h4, .tile p{
        padding-right: initial;
    }
    </style>    

";
    }

    // line 109
    public function block_javascripts($context, array $blocks = array())
    {
        // line 110
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
    jQuery(function(\$) {
        // Affix
        \$('#myAffix').affix({ offset: { top: -100, } });
        // Active
        \$('#gnav-savoirfaire, #dropnav-histoire, #subnav-histoire').addClass('active');
    })
    </script>
";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Default:histoire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  276 => 110,  273 => 109,  256 => 94,  252 => 92,  248 => 91,  241 => 89,  237 => 88,  231 => 84,  227 => 82,  223 => 81,  216 => 79,  212 => 78,  206 => 74,  202 => 72,  198 => 71,  191 => 69,  187 => 68,  178 => 62,  173 => 60,  169 => 59,  165 => 58,  161 => 57,  157 => 56,  153 => 55,  149 => 54,  143 => 51,  135 => 46,  129 => 43,  125 => 42,  121 => 41,  117 => 40,  113 => 39,  109 => 38,  105 => 37,  101 => 36,  97 => 35,  93 => 34,  85 => 29,  81 => 28,  75 => 25,  68 => 20,  66 => 19,  59 => 17,  53 => 16,  49 => 15,  42 => 10,  39 => 9,  33 => 5,  30 => 4,  11 => 2,);
    }
}
