<?php

/* KayserPlatformBundle:Header:topmenu.html.twig */
class __TwigTemplate_44d657eb60dd2130f530f3524f8e545414bed11e0de8849749adf51622a2d870 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"top-nav\" style=\"z-index:9999;\">
  <div class=\"container\">
    <ul>
      <div id=\"choisir_langue\">
      \t<li><a href=\"http://localhost/kayser/web/app_dev.php/fr/\">";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("FR", array(), "Menu"), "html", null, true);
        echo "</a> |
      \t<a href=\"http://localhost/kayser/web/app_dev.php/en/\">";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("EN", array(), "Menu"), "html", null, true);
        echo "</a> |
      \t<a href=\"http://localhost/kayser/web/app_dev.php/es/\">";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ES", array(), "Menu"), "html", null, true);
        echo "</a> |
        <a href=\"http://localhost/kayser/web/app_dev.php/jp/\">";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("JP", array(), "Menu"), "html", null, true);
        echo "</a></li>
      </div>
      <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("kay_contact");
        echo "\"><span class=\"icon-contact\" aria-hidden=\"true\"></span>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONTACTER", array(), "Menu"), "html", null, true);
        echo "</a></li>
    </ul>
  </div>
    ";
        // line 13
        echo $this->env->getExtension('sonata_seo')->getTitle();
        echo "
    ";
        // line 14
        echo $this->env->getExtension('sonata_seo')->getMetadatas();
        echo "
</header>";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Header:topmenu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 14,  50 => 13,  42 => 10,  37 => 8,  33 => 7,  29 => 6,  25 => 5,  19 => 1,);
    }
}
