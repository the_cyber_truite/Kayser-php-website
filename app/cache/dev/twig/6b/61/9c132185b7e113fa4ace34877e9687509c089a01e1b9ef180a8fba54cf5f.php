<?php

/* KayserPlatformBundle:Header:submenu.html.twig */
class __TwigTemplate_6b619c132185b7e113fa4ace34877e9687509c089a01e1b9ef180a8fba54cf5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"subnav hidden-sm hidden-xs\">
    <li><a href=\"";
        // line 2
        echo $this->env->getExtension('routing')->getPath("kay_histoire");
        echo "\" id=\"subnav-histoire\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("HISTOIRES", array(), "Menu"), "html", null, true);
        echo "</a></li>
    <li><a href=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("kay_philosophie");
        echo "\" id=\"subnav-philosophie\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PHILOSOPHIES", array(), "Menu"), "html", null, true);
        echo "</a></li>
    <li><a href=\"";
        // line 4
        echo $this->env->getExtension('routing')->getPath("kay_methodes");
        echo "\" id=\"subnav-methodes\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("METHODES", array(), "Menu"), "html", null, true);
        echo "</a></li>
    <li><a href=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("kay_equipes");
        echo "\" id=\"subnav-equipes\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("EQUIPES", array(), "Menu"), "html", null, true);
        echo "</a></li>
</ul>";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Header:submenu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 5,  34 => 4,  28 => 3,  22 => 2,  19 => 1,);
    }
}
