<?php

/* KayserPlatformBundle:Default:index.html.twig */
class __TwigTemplate_5df58a7359d82bebd551d67bc711189d8f5bfa99b018ddf0d6c2b8d057758543 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("KayserPlatformBundle::layout.html.twig", "KayserPlatformBundle:Default:index.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'platform_body' => array($this, 'block_platform_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "KayserPlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        // line 5
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Accueil
";
    }

    // line 8
    public function block_platform_body($context, array $blocks = array())
    {
        // line 9
        echo "    <div id=\"home\"></div>
    <div class=\"container main-container\" { style=\"padding-top:30px;\" }>
        <div class=\"row\">
            <h3 class=\"left-col-social\">";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("actualite", array(), "Menu"), "html", null, true);
        echo "</h3>
                <div class=\"col-md-3 hidden-sm hidden-xs\">
                    <a class=\"twitter-timeline\" href=\"https://twitter.com/Maison_EK\" data-widget-id=\"607890028161867776\">Tweets de @Maison_EK</a>
                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\"://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");
                    </script>
                </div>
            
             <div class=\"col-md-9 first-column-sm\"> 
                <a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "\" class=\"tile-link\"><div class=\"row feature tile over\" style=\"margin-bottom:14px;\">
                    <div class=\"col-sm-4\">
                        <h3>";
        // line 22
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["blocFeatureHome"]) ? $context["blocFeatureHome"] : $this->getContext($context, "blocFeatureHome")), "title", array())), "html", null, true);
        echo "</h3>
                        ";
        // line 23
        echo $this->getAttribute((isset($context["blocFeatureHome"]) ? $context["blocFeatureHome"] : $this->getContext($context, "blocFeatureHome")), "body", array());
        echo "
                    </div>
                    <div class=\"col-sm-8 feature hero\">
                        <img src=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["blocFeatureHome"]) ? $context["blocFeatureHome"] : $this->getContext($context, "blocFeatureHome")), "image", array()), "webPath", array()), "kay_featured_bloc"), "html", null, true);
        echo "\" alt=\"Vitrine de présentation Kayser\">
                    </div>
                </div></a>
                <div class=\"row\">
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 31
        echo $this->env->getExtension('routing')->getPath("kay_produits");
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["blocHomeOne"]) ? $context["blocHomeOne"] : $this->getContext($context, "blocHomeOne")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"Patisserie Kayser - Nos patisseries\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 34
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["blocHomeOne"]) ? $context["blocHomeOne"] : $this->getContext($context, "blocHomeOne")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 35
        echo $this->getAttribute((isset($context["blocHomeOne"]) ? $context["blocHomeOne"] : $this->getContext($context, "blocHomeOne")), "body", array());
        echo "</p>
                                ";
        // line 37
        echo "                            </div>
                        </div></a>
                    </div>
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 41
        echo $this->env->getExtension('routing')->getPath("kay_savoirfaire");
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["blocHomeTwo"]) ? $context["blocHomeTwo"] : $this->getContext($context, "blocHomeTwo")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"Pain Kayser - Notre savoir-faire\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 44
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["blocHomeTwo"]) ? $context["blocHomeTwo"] : $this->getContext($context, "blocHomeTwo")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 45
        echo $this->getAttribute((isset($context["blocHomeTwo"]) ? $context["blocHomeTwo"] : $this->getContext($context, "blocHomeTwo")), "body", array());
        echo "</p>
                                ";
        // line 47
        echo "                            </div>
                        </div></a>
                    </div>
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 51
        echo $this->env->getExtension('routing')->getPath("kay_homepage");
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["blocHomeThree"]) ? $context["blocHomeThree"] : $this->getContext($context, "blocHomeThree")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"Monsieur Kayser - Boutique en Ligne\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 54
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["blocHomeThree"]) ? $context["blocHomeThree"] : $this->getContext($context, "blocHomeThree")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 55
        echo $this->getAttribute((isset($context["blocHomeThree"]) ? $context["blocHomeThree"] : $this->getContext($context, "blocHomeThree")), "body", array());
        echo "</p>
                                ";
        // line 57
        echo "                            </div>
                        </div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 66
    public function block_javascripts($context, array $blocks = array())
    {
        // line 67
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
    jQuery(function(\$) {
            //Feature Graphics height
            var ftheight = \$(window).height() - 77;
            // var ftheight = \$(window).height() - 227;
            \$('.feature-graphics').height(ftheight);

        // Header
        \$('.header-hero, .feature-graphics').show();
        // Affix
        var affixvar = \$(window).height() - 74;
        \$('#myAffix').affix({
          offset: {
            top: affixvar,
          }
        })

        // Carousel
        \$('.carousel').carousel({
          interval: 1500
        })

        // Scroll down
        var fenetre = \$(window).height() - 74;
        \$('#hp-scrolldown').on('click', function() {
            \$(window).scrollTo( { top:fenetre, left:0}, 800 );
        });

    })
    \$(document).ready(function(){
        // Parallax
        var pos = 0.3; // Pour le site en live les url doivent être : /img/md_hp_slideshow_3.jpg sous ce format donc => enelver /kayser/web/
        if (\$(window).width() < 768 ) {
            var items = [\"/kayser/web/img/xs_hp_slideshow_2.jpg\",\"/kayser/web/img/xs_hp_slideshow_3.jpg\"];
        }else if (\$(window).width() < 992 ) {
            var items = [\"/kayser/web/img/sm_hp_slideshow_1.jpg\",\"/kayser/web/img/sm_hp_slideshow_2.jpg\",\"/kayser/web/img/sm_hp_slideshow_3.jpg\"];
        }else if (\$(window).width() < 1199 ) {
            var items = [\"/kayser/web/img/md_hp_slideshow_1.jpg\",\"/kayser/web/img/md_hp_slideshow_2.jpg\",\"/kayser/web/img/md_hp_slideshow_3.jpg\"];
            var pos = 0.7;
        }else {
            var items = [\"/kayser/web/img/hp_slideshow_1.jpg\",\"/kayser/web/img/hp_slideshow_2.jpg\",\"/kayser/web/img/hp_slideshow_3.jpg\"];
        }
        var item = items[Math.floor(Math.random()*items.length)];
        \$('#parallax-slide').css(\"backgroundImage\", \"url(\"+item+\")\");

        if (\$(window).width() > 768 )
        {
            \$('#parallax-slide').css(\"background-size\",\"cover\");
        }
        else
        {
            \$('#parallax-slide').css(\"background-size\",\"450px 552px\");

            if (item == \"/kayser/web/img/xs_hp_slideshow_1.jpg\")
            {
                \$('#parallax-slide').css(\"background-size\",\"655px 490px\");
            }
        }
        \$('#parallax-slide').parallax(\"50%\", pos);
    })
    </script>

";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 67,  155 => 66,  144 => 57,  140 => 55,  136 => 54,  131 => 52,  127 => 51,  121 => 47,  117 => 45,  113 => 44,  108 => 42,  104 => 41,  98 => 37,  94 => 35,  90 => 34,  85 => 32,  81 => 31,  73 => 26,  67 => 23,  63 => 22,  58 => 20,  47 => 12,  42 => 9,  39 => 8,  33 => 5,  30 => 4,  11 => 2,);
    }
}
