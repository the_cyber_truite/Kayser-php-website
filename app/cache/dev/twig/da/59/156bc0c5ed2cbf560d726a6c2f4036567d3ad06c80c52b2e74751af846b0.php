<?php

/* KayserPlatformBundle:Default:login.html.twig */
class __TwigTemplate_da59156bc0c5ed2cbf560d726a6c2f4036567d3ad06c80c52b2e74751af846b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("KayserPlatformBundle::layout.html.twig", "KayserPlatformBundle:Default:login.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'platform_body' => array($this, 'block_platform_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "KayserPlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        // line 5
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Notre Savoir-Faire
";
    }

    // line 8
    public function block_platform_body($context, array $blocks = array())
    {
        // line 9
        echo "    <div class=\"container main-container\">
        <div class=\"row\">  
        ";
        // line 11
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 12
            echo "            <div>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "message", array()), "html", null, true);
            echo "</div>
        ";
        }
        // line 14
        echo " 
        <form action=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("login_check");
        echo "\" method=\"post\">
            <label for=\"username\">Username:</label>
            <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" />
            <label for=\"password\">Password:</label>
            <input type=\"password\" id=\"password\" name=\"_password\" />
            <button type=\"submit\">login</button>
        </form>
        </div>
    </div>
";
    }

    // line 26
    public function block_javascripts($context, array $blocks = array())
    {
        // line 27
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
    jQuery(function(\$) {
        // Affix
        \$('#myAffix').affix({ offset: { top: -100, } });
        // Active
        \$('#gnav-savoirfaire').addClass('active');
    })
    </script>
";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Default:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 27,  74 => 26,  62 => 17,  57 => 15,  54 => 14,  48 => 12,  46 => 11,  42 => 9,  39 => 8,  33 => 5,  30 => 4,  11 => 2,);
    }
}
