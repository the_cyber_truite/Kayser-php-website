<?php

/* KayserPlatformBundle:Default:sales.html.twig */
class __TwigTemplate_ae5425b3bcd289ba841028f3807f3fc5d2c40ea872ad304d6948de178cffe33c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("KayserPlatformBundle::layout.html.twig", "KayserPlatformBundle:Default:sales.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'platform_body' => array($this, 'block_platform_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "KayserPlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        // line 5
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Notre Histoire
";
    }

    // line 9
    public function block_platform_body($context, array $blocks = array())
    {
        // line 10
        echo "<div class=\"container main-container\">
    <div class=\"row\">
        <div class=\"col-md-3\">
            <div data-spy=\"affix\">
                <ol class=\"breadcrumb\">
                  <li><a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("kay_homepage");
        echo "\">HOME</a></li>
                  <li><a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("kay_produits");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PRODUITS", array(), "Menu"), "html", null, true);
        echo "</a></li>
                  <li><a href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("kay_produits_carte", array("slug" => "sales"));
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("SALES", array(), "Menu"), "html", null, true);
        echo "</a></li>
              </ol>
              ";
        // line 19
        $this->loadTemplate("KayserPlatformBundle:Header:subnav-products.html.twig", "KayserPlatformBundle:Default:sales.html.twig", 19)->display($context);
        // line 20
        echo "          </div>
      </div>
      <div class=\"col-md-9\">
        <div class=\"row full tile\" style=\"margin-bottom:74px;\">
            <div class=\"row col-history-row\">
                <div class=\"col-sm-12 col-history-content\">
                    <h3> ";
        // line 26
        echo $this->getAttribute($this->getAttribute((isset($context["productSale"]) ? $context["productSale"] : $this->getContext($context, "productSale")), 0, array(), "array"), "title", array());
        echo " </h3>
                    <p class=\"intro\"> ";
        // line 27
        echo $this->getAttribute($this->getAttribute((isset($context["productSale"]) ? $context["productSale"] : $this->getContext($context, "productSale")), 0, array(), "array"), "body", array());
        echo " </p>
                  ";
        // line 28
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, (isset($context["catSales"]) ? $context["catSales"] : $this->getContext($context, "catSales"))));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 29
            echo "                    <div class=\"menu-section\"><p style=\"max-width: 230px\"> ";
            echo $this->getAttribute($this->getAttribute((isset($context["productSale"]) ? $context["productSale"] : $this->getContext($context, "productSale")), $context["i"], array(), "array"), "title", array());
            echo " </p></div>
                    <section> 
                    ";
            // line 31
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["productSale"]) ? $context["productSale"] : $this->getContext($context, "productSale")));
            foreach ($context['_seq'] as $context["keys"] => $context["product"]) {
                // line 32
                echo "                      ";
                if (($this->getAttribute($context["product"], "category", array()) == $context["i"])) {
                    // line 33
                    echo "                        ";
                    if (($this->getAttribute($context["product"], "position", array()) != 0)) {
                        // line 34
                        echo "                          ";
                        if ( !(($this->getAttribute($context["product"], "position", array()) - 1) % 3)) {
                            // line 35
                            echo "                        <div class=\"row menu-tab\"> 
                              <div class=\"product-list\">
                                <h4 class=\"menu-title\">";
                            // line 37
                            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "title", array()), "html", null, true);
                            echo "</h4>
                                <p class=\"menu-detail\">";
                            // line 38
                            echo $this->getAttribute($context["product"], "body", array());
                            echo "</p>
                              </div>      
                        </div>
                          ";
                        }
                        // line 42
                        echo "                          ";
                        if ( !(($this->getAttribute($context["product"], "position", array()) - 2) % 3)) {
                            echo " 
                        <div class=\"row menu-tab\"> 
                              <div class=\"product-list\">
                                <h4 class=\"menu-title\">";
                            // line 45
                            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "title", array()), "html", null, true);
                            echo "</h4>
                                <p class=\"menu-detail\">";
                            // line 46
                            echo $this->getAttribute($context["product"], "body", array());
                            echo "</p>
                              </div>
                        </div> 
                          ";
                        }
                        // line 50
                        echo "                          ";
                        if ( !(($this->getAttribute($context["product"], "position", array()) - 3) % 3)) {
                            echo " 
                        <div class=\"row menu-tab\"> 
                              <div class=\"product-list\">
                                <h4 class=\"menu-title\">";
                            // line 53
                            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "title", array()), "html", null, true);
                            echo "</h4>
                                <p class=\"menu-detail\">";
                            // line 54
                            echo $this->getAttribute($context["product"], "body", array());
                            echo "</p>
                              </div>
                        </div>
                          ";
                        }
                        // line 58
                        echo "                        ";
                    }
                    // line 59
                    echo "                      ";
                }
                // line 60
                echo "                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['keys'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 61
            echo "                    </section>
                  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "         
          <br style=\"clear:both;\"/>
</div>
</div>
</div> ";
        // line 67
        echo "</div>
</div>
</div>
<style type=\"text/css\">
.label-gluten{
    font-size: 10px;
    text-transform: uppercase;
    padding-bottom: 2px;
}
</style>    
";
    }

    // line 79
    public function block_javascripts($context, array $blocks = array())
    {
        // line 80
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
<script>
jQuery(function(\$) {
        // Affix
        \$('#myAffix').affix({ offset: { top: -100, } });
        // Active
        \$('#subnav-sales').addClass('active');
    })
</script>
";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Default:sales.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 80,  197 => 79,  183 => 67,  177 => 62,  170 => 61,  164 => 60,  161 => 59,  158 => 58,  151 => 54,  147 => 53,  140 => 50,  133 => 46,  129 => 45,  122 => 42,  115 => 38,  111 => 37,  107 => 35,  104 => 34,  101 => 33,  98 => 32,  94 => 31,  88 => 29,  84 => 28,  80 => 27,  76 => 26,  68 => 20,  66 => 19,  59 => 17,  53 => 16,  49 => 15,  42 => 10,  39 => 9,  33 => 5,  30 => 4,  11 => 2,);
    }
}
