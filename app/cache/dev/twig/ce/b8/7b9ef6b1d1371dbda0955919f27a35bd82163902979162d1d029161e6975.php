<?php

/* KayserPlatformBundle:Header:menu.html.twig */
class __TwigTemplate_ceb87b9ef6b1d1371dbda0955919f27a35bd82163902979162d1d029161e6975 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<nav class=\"navbar navbar-default\" id=\"myAffix\">
  <div class=\"container\">
    <div class=\"navbar-header\">
      <button type=\"button\" class=\"navbar-toggle collapsed visible-xs\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
        <span class=\"sr-only\">Toggle navigation</span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
      </button>
      <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("kay_homepage");
        echo "#home\" class=\"navbar-brand\">
        <img src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/logo-icon.png"), "html", null, true);
        echo "\" alt=\"Maison Kayser\">
      </a>
    </div>
    <div id=\"navbar\" class=\"navbar-collapse collapse\" aria-expanded=\"false\">
      <ul class=\"nav navbar-nav hidden-xs hidden-sm\">
        <li class=\"dropdown\">
          <a id=\"gnav-savoirfaire\" href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("kay_savoirfaire");
        echo "\" data-target=\"";
        echo $this->env->getExtension('routing')->getPath("kay_savoirfaire");
        echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("savoir", array(), "Menu"), "html", null, true);
        echo "</a>
          <ul class=\"dropdown-menu\" role=\"menu\">
            <li><a id=\"dropnav-histoire\" href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("kay_histoire");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("histoire", array(), "Menu"), "html", null, true);
        echo "</a></li>
            <li><a id=\"dropnav-philosophie\" href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("kay_philosophie");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("philosophie", array(), "Menu"), "html", null, true);
        echo "</a></li>
            <li><a id=\"dropnav-methodes\" href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("kay_methodes");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("methodes", array(), "Menu"), "html", null, true);
        echo "</a></li>
            <li><a id=\"dropnav-equipes\" href=\"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("kay_equipes");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("equipes", array(), "Menu"), "html", null, true);
        echo "</a></li>
          </ul>
        </li>
        <li class=\"dropdown\">
          <a id=\"gnav-produits\" href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("kay_produits");
        echo "\" data-target=\"";
        echo $this->env->getExtension('routing')->getPath("kay_produits");
        echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("produits", array(), "Menu"), "html", null, true);
        echo "</a>
          <ul class=\"dropdown-menu\" role=\"menu\">
            <li><a id=\"dropnav-pains\" href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("kay_produits_list", array("category" => "pains"));
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("pain", array(), "Menu"), "html", null, true);
        echo "</a></li>
            <li><a id=\"dropnav-patisseries\" href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("kay_produits_carte", array("slug" => "patisseries"));
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("patisserie", array(), "Menu"), "html", null, true);
        // line 30
        echo "</a></li>
            <li><a id=\"dropnav-sales\" href=\"";
        // line 31
        echo $this->env->getExtension('routing')->getPath("kay_produits_carte", array("slug" => "sales"));
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("offres", array(), "Menu"), "html", null, true);
        echo "</a></li>
          </ul>
        </li>
        <li><a id=\"gnav-trouver\" href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("trouver", array(), "Menu"), "html", null, true);
        echo "</a></li>
        ";
        // line 36
        echo "        <li >
          <a id=\"gnav-enligne\" href=\"#\" class=\"visible-lg\" data-toggle=\"dropdown\" style=\"color:#CECECE\">";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ligne", array(), "Menu"), "html", null, true);
        echo "</a>
          <ul class=\"dropdown-menu\" role=\"menu\">
            <li><a href=\"#\">Retrait en magasin</a></li>
            <li><a href=\"#\">Livraison</a></li>
          </ul>
        </li>
      </ul>
      <ul class=\"nav navbar-nav visible-xs visible-sm\">
        <li class=\"dropdown\">
          <a id=\"gnav-savoirfaire\" ";
        // line 46
        echo " data-target=\"#menu-savoirfaire\"  href=\"#menu-savoirfaire\"  class=\"dropdown-toggle\" data-toggle=\"collapse\" role=\"button\" aria-expanded=\"false\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("savoir", array(), "Menu"), "html", null, true);
        echo " <span class=\"caret\"></span></a>
          <ul class=\"dropdown-menu collapse\" role=\"menu\" id=\"menu-savoirfaire\">
            <li><a id=\"dropnav-histoire\" href=\"";
        // line 48
        echo $this->env->getExtension('routing')->getPath("kay_histoire");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("histoire", array(), "Menu"), "html", null, true);
        echo "</a></li>
            <li><a id=\"dropnav-philosophie\" href=\"";
        // line 49
        echo $this->env->getExtension('routing')->getPath("kay_philosophie");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("philosophie", array(), "Menu"), "html", null, true);
        echo "</a></li>
            <li><a id=\"dropnav-methodes\" href=\"";
        // line 50
        echo $this->env->getExtension('routing')->getPath("kay_methodes");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("methodes", array(), "Menu"), "html", null, true);
        echo "</a></li>
            <li><a id=\"dropnav-equipes\" href=\"";
        // line 51
        echo $this->env->getExtension('routing')->getPath("kay_equipes");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("equipes", array(), "Menu"), "html", null, true);
        echo "</a></li>
          </ul>
        </li>
        <li class=\"dropdown\">
          <a id=\"gnav-produits\" ";
        // line 55
        echo " data-target=\"#menu-produits\" href=\"#menu-produits\" class=\"dropdown-toggle\" data-toggle=\"collapse\" role=\"button\" aria-expanded=\"false\">Nos Produits <span class=\"caret\"></span></a>
          <ul class=\"dropdown-menu collapse\" role=\"menu\" id=\"menu-produits\">
            <li><a id=\"dropnav-pains\" href=\"";
        // line 57
        echo $this->env->getExtension('routing')->getPath("kay_produits_list", array("category" => "pains"));
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("pain", array(), "Menu"), "html", null, true);
        echo "</a></li>
            <li><a id=\"dropnav-patisseries\" href=\"";
        // line 58
        echo $this->env->getExtension('routing')->getPath("kay_produits_carte", array("slug" => "patisseries"));
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("patisserie", array(), "Menu"), "html", null, true);
        echo "</a></li>
            <li><a id=\"dropnav-sales\" href=\"";
        // line 59
        echo $this->env->getExtension('routing')->getPath("kay_produits_carte", array("slug" => "sales"));
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("offres", array(), "Menu"), "html", null, true);
        echo "</a></li>
          </ul>
        </li>
        <li>
          <a id=\"gnav-trouver\" href=\"";
        // line 63
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "\" data-target=\"";
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("trouver", array(), "Menu"), "html", null, true);
        echo "</a>
        </li>
        <li>
          <a id=\"gnav-enligne\" class=\"visible-lg\" href=\"#\" style=\"color:#CECECE\">";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ligne", array(), "Menu"), "html", null, true);
        echo "</span></a>
       </li>
      </ul>
    </div>

    
    <div class=\"navbar-right hidden-xs hidden-sm\">
      <ul class=\"nav navbar-nav navbar-social\">
        <li><a href=\"https://twitter.com/maison_ek\" target=\"_blank\"><span class=\"nav-social nav-social-twitter\"></span></a></li>
        <li><a href=\"https://www.facebook.com/MaisonKayser\" target=\"_blank\"><span class=\"nav-social nav-social-facebook\"></span></a></li>
        <li><a href=\"https://plus.google.com/102660033434943800028/about\" target=\"_blank\"><span class=\"nav-social nav-social-google\"></span></a></li>
        <li><a href=\"https://www.youtube.com/user/Maisonek\" target=\"_blank\"><span class=\"nav-social nav-social-youtube\"></span></a></li>
        <li><a href=\"https://www.pinterest.com/maisonkayser/\" target=\"_blank\"><span class=\"nav-social nav-social-pinterest\"></span></a></li>
        <li><a href=\"http://instagram.com/maisonkayser\" target=\"_blank\"><span class=\"nav-social nav-social-instagram\"></span></a></li>
        ";
        // line 81
        echo "      </ul>
    </div>
    ";
        // line 91
        echo "  </div>
</nav>";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Header:menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 91,  215 => 81,  198 => 66,  188 => 63,  179 => 59,  173 => 58,  167 => 57,  163 => 55,  154 => 51,  148 => 50,  142 => 49,  136 => 48,  130 => 46,  118 => 37,  115 => 36,  109 => 34,  101 => 31,  98 => 30,  94 => 29,  88 => 28,  79 => 26,  70 => 22,  64 => 21,  58 => 20,  52 => 19,  43 => 17,  34 => 11,  30 => 10,  19 => 1,);
    }
}
