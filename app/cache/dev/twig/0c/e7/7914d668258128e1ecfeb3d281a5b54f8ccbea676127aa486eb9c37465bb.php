<?php

/* KayserPlatformBundle:Default:pains.html.twig */
class __TwigTemplate_0ce77914d668258128e1ecfeb3d281a5b54f8ccbea676127aa486eb9c37465bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("KayserPlatformBundle::layout.html.twig", "KayserPlatformBundle:Default:pains.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'platform_body' => array($this, 'block_platform_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "KayserPlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        // line 5
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Notre Histoire
";
    }

    // line 8
    public function block_platform_body($context, array $blocks = array())
    {
        // line 9
        echo "    <div class=\"container main-container\">
        <div class=\"row\">
            ";
        // line 12
        echo "            ";
        // line 13
        echo "            <div class=\"col-md-3\">
                <div data-spy=\"affix\">
                <ol class=\"breadcrumb\">
                  <li><a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("kay_homepage");
        echo "\">HOME</a></li>
                  <li><a href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("kay_produits");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PRODUITS", array(), "Menu"), "html", null, true);
        echo "</a></li>
                </ol>
                ";
        // line 19
        $this->loadTemplate("KayserPlatformBundle:Header:subnav-products.html.twig", "KayserPlatformBundle:Default:pains.html.twig", 19)->display($context);
        // line 20
        echo "                </div>
            </div>
            <div class=\"col-md-9\">
                <div class=\"row\">
                            <div class=\"col-sm-4\">
                    ";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : $this->getContext($context, "products")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 26
            echo "                        ";
            if ( !(($this->getAttribute($context["loop"], "index", array()) - 1) % 3)) {
                // line 27
                echo "                                <div class=\"row tile photo over product\">
                                    <img src=\"";
                // line 28
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute($this->getAttribute($context["product"], "image", array()), "webPath", array())), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
                echo "\">
                                    <div class=\"col-sm-12\">
                                        <h4 class=\"photo-title ";
                // line 30
                if ((twig_length_filter($this->env, $this->getAttribute($context["product"], "name", array())) > 20)) {
                    echo "two-lines";
                }
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
                if (($this->getAttribute($this->getAttribute($context["product"], "subcategory", array()), "name", array()) == "gluten")) {
                    echo " <span class=\"label label-gluten\">SANS GLUTEN</span>";
                }
                echo " </h4>
                                        ";
                // line 32
                echo "                                        <div class=\"photo-more-content\">
                                            <p>";
                // line 33
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "description", array()), "html", null, true);
                echo "</p>
                                        </div>
                                    </div>
                                </div>
                        ";
            }
            // line 38
            echo "                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "                            </div>
                    <div class=\"col-sm-4\">
                        ";
        // line 41
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : $this->getContext($context, "products")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 42
            echo "                            ";
            if ( !(($this->getAttribute($context["loop"], "index", array()) - 2) % 3)) {
                // line 43
                echo "                                <div class=\"row tile photo over product\">
                                    <img src=\"";
                // line 44
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute($this->getAttribute($context["product"], "image", array()), "webPath", array())), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
                echo "\">
                                    <div class=\"col-sm-12\">
                                        <h4 class=\"photo-title ";
                // line 46
                if ((twig_length_filter($this->env, $this->getAttribute($context["product"], "name", array())) > 20)) {
                    echo "two-lines";
                }
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
                if (($this->getAttribute($this->getAttribute($context["product"], "subcategory", array()), "name", array()) == "gluten")) {
                    echo " <span class=\"label label-gluten\">SANS GLUTEN</span>";
                }
                echo " </h4>
                                        ";
                // line 48
                echo "                                        <div class=\"photo-more-content\">
                                            <p>";
                // line 49
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "description", array()), "html", null, true);
                echo "</p>
                                        </div>
                                    </div>
                                </div>
                            ";
            }
            // line 54
            echo "                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                    </div>
                            <div class=\"col-sm-4\">
                    ";
        // line 57
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : $this->getContext($context, "products")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 58
            echo "                        ";
            if ( !($this->getAttribute($context["loop"], "index", array()) % 3)) {
                // line 59
                echo "                                <div class=\"row tile photo over product\">
                                    <img src=\"";
                // line 60
                echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl($this->getAttribute($this->getAttribute($context["product"], "image", array()), "webPath", array())), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
                echo "\">
                                    <div class=\"col-sm-12\">
                                        <h4 class=\"photo-title ";
                // line 62
                if ((twig_length_filter($this->env, $this->getAttribute($context["product"], "name", array())) > 20)) {
                    echo "two-lines";
                }
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
                if (($this->getAttribute($this->getAttribute($context["product"], "subcategory", array()), "name", array()) == "gluten")) {
                    echo " <span class=\"label label-gluten\">SANS GLUTEN</span>";
                }
                echo " </h4>
                                        ";
                // line 64
                echo "                                        <div class=\"photo-more-content\">
                                            <p>";
                // line 65
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "description", array()), "html", null, true);
                echo "</p>
                                        </div>
                                    </div>
                                </div>
                        ";
            }
            // line 70
            echo "                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        echo "                     </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 78
    public function block_javascripts($context, array $blocks = array())
    {
        // line 79
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
    jQuery(function(\$) {
        // Affix
        \$('#myAffix').affix({ offset: { top: -100 } });
        // Active
        \$('#gnav-produits, #subnav-";
        // line 85
        echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "html", null, true);
        echo ", #subnav-";
        echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, (isset($context["subcategory"]) ? $context["subcategory"] : $this->getContext($context, "subcategory")), "html", null, true);
        echo ", #dropnav-";
        echo twig_escape_filter($this->env, (isset($context["category"]) ? $context["category"] : $this->getContext($context, "category")), "html", null, true);
        echo "').addClass('active');
        // Picture tiles
        \$('.tile.product').on('click', function(e){
            e.preventDefault();
            \$(this).find('.photo-more-content').slideToggle(400);
            // \$(this).parent().next().slideToggle(400);
            // \$(this).find('.plus').toggle();
            // \$(this).find('.moins').toggle();
        });
    })
    </script>
";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Default:pains.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  301 => 85,  291 => 79,  288 => 78,  279 => 71,  265 => 70,  257 => 65,  254 => 64,  243 => 62,  236 => 60,  233 => 59,  230 => 58,  213 => 57,  209 => 55,  195 => 54,  187 => 49,  184 => 48,  173 => 46,  166 => 44,  163 => 43,  160 => 42,  143 => 41,  139 => 39,  125 => 38,  117 => 33,  114 => 32,  103 => 30,  96 => 28,  93 => 27,  90 => 26,  73 => 25,  66 => 20,  64 => 19,  57 => 17,  53 => 16,  48 => 13,  46 => 12,  42 => 9,  39 => 8,  33 => 5,  30 => 4,  11 => 2,);
    }
}
