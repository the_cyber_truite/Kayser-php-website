<?php

/* KayserPlatformBundle:Default:trouver.html.twig */
class __TwigTemplate_aa0867e6d878ba09613a96bfcda9a52b7a10373f3163b69de1e4f8ab6a2478f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("KayserPlatformBundle::layout.html.twig", "KayserPlatformBundle:Default:trouver.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'platform_body' => array($this, 'block_platform_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "KayserPlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        // line 5
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Où nous trouver
";
    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 9
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

<script src=\"https://maps.googleapis.com/maps/api/js?libraries=places\"></script>
<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.6/jquery.min.js\"></script>
<script src=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/store-locator.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/medicare-dynamic-ds.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/panel.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
<link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/storelocator.css"), "html", null, true);
        echo "\">

<style>
  #map-canvas { height: 500px; }
  #panel { width: 100%; }
  #panel .feature-filter label { width: 230px; margin: 0; text-align: center; font-weight: normal; font-size: 14px; }
  #panel .feature-filter input { margin: 4px 4px 0; }
  p.attribution, p.attribution a { color: #666; }
</style>
";
    }

    // line 27
    public function block_platform_body($context, array $blocks = array())
    {
        // line 28
        echo "    <div class=\"container main-container\">
        <div class=\"row\">
            <div class=\"col-md-3\">
                <div data-spy=\"affix\">
                    <ol class=\"breadcrumb\">
                      <li><a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("kay_homepage");
        echo "\">HOME</a></li>
                      <li><a href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("TROUVER", array(), "Menu"), "html", null, true);
        echo "</a></li>
                    </ol>
                    ";
        // line 36
        $this->loadTemplate("KayserPlatformBundle:Header:subnav-locator.html.twig", "KayserPlatformBundle:Default:trouver.html.twig", 36)->display($context);
        // line 37
        echo "                </div>             
            </div>
            <div class=\"col-md-9\">
                <div class=\"row full tile\" style=\"margin-bottom:14px;\">
                    <div id=\"map-canvas\"></div>
                    <div id=\"panel\"></div>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 49
    public function block_javascripts($context, array $blocks = array())
    {
        // line 50
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    
    <script>
    jQuery(function(\$) {
        // Affix
        \$('#myAffix').affix({ offset: { top: -100, } });

        // Active
        \$('#gnav-trouver').addClass('active');

        // Par pays
        // \$('.locator-link').on('click', function(e) {
        //     location.reload();
        // });


        // List fix
        \$(window).load(function() {
            \$('li.store').each(function () {
                var div = \$(this).find('.phone');
                val = div.val();
                div.val('<u>Télephone</u>'+val);
            });
        });


    })
    </script>
";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Default:trouver.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 50,  113 => 49,  99 => 37,  97 => 36,  90 => 34,  86 => 33,  79 => 28,  76 => 27,  62 => 16,  58 => 15,  54 => 14,  50 => 13,  43 => 9,  40 => 8,  34 => 5,  31 => 4,  11 => 2,);
    }
}
