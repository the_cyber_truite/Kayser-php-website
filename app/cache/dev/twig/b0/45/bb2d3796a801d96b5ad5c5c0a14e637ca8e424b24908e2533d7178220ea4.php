<?php

/* KayserPlatformBundle:Default:candidature.html.twig */
class __TwigTemplate_b045bb2d3796a801d96b5ad5c5c0a14e637ca8e424b24908e2533d7178220ea4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("KayserPlatformBundle::layout.html.twig", "KayserPlatformBundle:Default:candidature.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'platform_body' => array($this, 'block_platform_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "KayserPlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        // line 5
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Notre Histoire
";
    }

    // line 9
    public function block_platform_body($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"container main-container\">
        <div class=\"row\">
            <div class=\"col-md-3\">
                <div data-spy=\"affix\">
                    <ol class=\"breadcrumb\">
                      <li><a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("kay_homepage");
        echo "\">HOME</a></li>
                      <li><a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("kay_annonces");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("REJOINDRE", array(), "Menu"), "html", null, true);
        echo "</a></li>
                      <li><a href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("kay_candidature");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CANDIDATURE", array(), "Menu"), "html", null, true);
        echo "</a></li>
                    </ol>
                    ";
        // line 19
        $this->loadTemplate("KayserPlatformBundle:Header:subnav-annonces.html.twig", "KayserPlatformBundle:Default:candidature.html.twig", 19)->display($context);
        // line 20
        echo "                </div>
            </div>
            <div class=\"col-md-9\">
                <div class=\"row tile full\" style=\"margin-bottom:14px;\" id=\"row_full_title\">
                    <div class=\"col-sm-12\">
                        ";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "info"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 26
            echo "                            <div class=\"alert alert-success\" style=\"margin-top:10px;\">
                                <p>";
            // line 27
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "</p>
                            </div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "                    </div>
                    <h3>";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tableCandidature"]) ? $context["tableCandidature"] : $this->getContext($context, "tableCandidature")), 1, array(), "array"), "body", array()), "html", null, true);
        echo "</h3>
                    <p class=\"contact-us-p\">";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tableCandidature"]) ? $context["tableCandidature"] : $this->getContext($context, "tableCandidature")), 2, array(), "array"), "body", array()), "html", null, true);
        echo "</p>
                    <div class=\"col-history-row\">
                        <div class=\"col-md-7 col-md-offset-2 row col-history-content\">
                            <div class=\"row\" style=\"margin:0;\">
                                ";
        // line 36
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                                ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "
                                    ";
        // line 38
        if (((isset($context["language"]) ? $context["language"] : $this->getContext($context, "language")) == "fr")) {
            // line 39
            echo "                                        <div class=\"form-group\">
                                            ";
            // line 40
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gender", array()), 'widget', array("attr" => array("class" => "form-radio")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 43
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Nom")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 46
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Prénom")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 49
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "telephone", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Télephone")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 52
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Email")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 55
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Adresse")));
            echo "
                                        </div>
                                        <div class=\"row\">
                                            <div class=\"form-group col-md-5\">
                                                ";
            // line 59
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cp", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Code postal")));
            echo "
                                            </div>
                                            <div class=\"form-group col-md-7\">
                                                ";
            // line 62
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Ville")));
            echo "
                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            Votre C.V :
                                            ";
            // line 67
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cv", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
                                            <div style=\"color:red\">Format accepté (.pdf .docx) avec comme titre : Nom prenom cv</div>
                                        </div>
                                            Votre lettre de motivation :
                                        <div class=\"form-group\">
                                            ";
            // line 72
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "lettre", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
                                            <div style=\"color:red\">Format accepté (.pdf .docx) avec comme titre : Nom prenom lettre</div>
                                        <br />
                                            Poste recherché :
                                        <div class=\"form-group\">
                                            ";
            // line 77
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "objet", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Titre du poste")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 80
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "message", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => 6, "placeholder" => "Veuillez taper un message...")));
            echo "
                                        </div>
        <!-- PARTIE EN ANGLAIS --> ";
        } elseif ((        // line 82
(isset($context["language"]) ? $context["language"] : $this->getContext($context, "language")) == "en")) {
            echo " <!-- PARTIE EN ANGLAIS -->
                                        <div class=\"form-group\">
                                            ";
            // line 84
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gender", array()), 'widget', array("attr" => array("class" => "form-radio")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 87
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Name")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 90
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "First Name")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 93
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "telephone", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Tel")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 96
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Email")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 99
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Address")));
            echo "
                                        </div>
                                        <div class=\"row\">
                                            <div class=\"form-group col-md-5\">
                                                ";
            // line 103
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cp", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Zip Code")));
            echo "
                                            </div>
                                            <div class=\"form-group col-md-7\">
                                                ";
            // line 106
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "City")));
            echo "
                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            Your C.V:
                                            ";
            // line 111
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cv", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
                                            <div style = \"color: red\"> Accepted format (.docx .pdf) with the title : Name firstName cv </div>
                                        </div>
                                            Your cover letter:
                                        <div class=\"form-group\">
                                            ";
            // line 116
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "lettre", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
                                            <div style = \"color: red\"> Accepted format (.docx .pdf) with the title : Name firstName lettrer </div>
                                        </div>
                                            Job wanted:
                                        <div class=\"form-group\">
                                            ";
            // line 121
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "objet", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Title of job")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 124
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "message", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => 6, "placeholder" => "Type your message...")));
            echo "
                                        </div>
        <!-- PARTIE EN ESPAGNOL --> ";
        } elseif ((        // line 126
(isset($context["language"]) ? $context["language"] : $this->getContext($context, "language")) == "es")) {
            echo " <!-- PARTIE EN ESPAGNOL -->
                                        <div class=\"form-group\">
                                            ";
            // line 128
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gender", array()), 'widget', array("attr" => array("class" => "form-radio")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 131
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Apellido")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 134
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Nombre")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 137
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "telephone", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Teléfono")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 140
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Email")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 143
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Dirección")));
            echo "
                                        </div>
                                        <div class=\"row\">
                                            <div class=\"form-group col-md-5\">
                                                ";
            // line 147
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cp", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Código Postal")));
            echo "
                                            </div>
                                            <div class=\"form-group col-md-7\">
                                                ";
            // line 150
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "City")));
            echo "
                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            Tu C.V:
                                            ";
            // line 155
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cv", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
                                            <div style = \"color: red\"> Format aceptado (.docx .pdf) con el tìtulo : Nombre apellido cv </div>
                                        </div>
                                            Su carta de presentación:
                                        <div class=\"form-group\">
                                            ";
            // line 160
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "lettre", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
                                            <div style = \"color: red\"> Format aceptado (.docx .pdf) con el tìtulo : Nombre apellido carta </div>
                                        </div>
                                            Trabajo buscó:
                                        <div class=\"form-group\">
                                            ";
            // line 165
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "objet", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "Título del puesto")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 168
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "message", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => 6, "placeholder" => "Escribe tu mensaje aquí...")));
            echo "
                                        </div>
        <!-- PARTIE EN JAPONAIS --> ";
        } elseif ((        // line 170
(isset($context["language"]) ? $context["language"] : $this->getContext($context, "language")) == "jp")) {
            echo " <!-- PARTIE EN JAPONAIS -->
                                        <div class=\"form-group\">
                                            ";
            // line 172
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "gender", array()), 'widget', array("attr" => array("class" => "form-radio")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 175
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "nom", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "苗字")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 178
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "prenom", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "ファーストネーム")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 181
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "telephone", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "電話")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 184
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "電子メール")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 187
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "adresse", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "アドレス")));
            echo "
                                        </div>
                                        <div class=\"row\">
                                            <div class=\"form-group col-md-5\">
                                                ";
            // line 191
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cp", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "郵便番号")));
            echo "
                                            </div>
                                            <div class=\"form-group col-md-7\">
                                                ";
            // line 194
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "ville", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "街")));
            echo "
                                            </div>
                                        </div>
                                        <div class=\"form-group\">
                                            あなたの履歴書:
                                            ";
            // line 199
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "cv", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
                                            <div style = \"color: red\"> タイトル (.docx .pdf) タイトル : 名_ファーストネーム_あなたの履歴書 </div>
                                        </div>
                                            あなたのカバーレター:
                                        <div class=\"form-group\">
                                            ";
            // line 204
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "lettre", array()), 'widget', array("attr" => array("class" => "form-control")));
            echo "
                                            <div style = \"color: red\"> タイトル (.docx .pdf) タイトル : 名_ファーストネーム_あなたのカバーレター </div>
                                        <br />
                                            位置が求められる:
                                        <div class=\"form-group\">
                                            ";
            // line 209
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "objet", array()), 'widget', array("attr" => array("class" => "form-control", "placeholder" => "ポジションタイトル")));
            echo "
                                        </div>
                                        <div class=\"form-group\">
                                            ";
            // line 212
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "message", array()), 'widget', array("attr" => array("class" => "form-control", "rows" => 6, "placeholder" => "メッセージを入力してください。...")));
            echo "
                                        </div>
                                    ";
        }
        // line 215
        echo "                                </div>
                                <div class=\"col-sm-12\" style=\"padding-left:14px;padding-right:14px;margin-bottom:14px;\">
                                    <p><small><em>";
        // line 217
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tableCandidature"]) ? $context["tableCandidature"] : $this->getContext($context, "tableCandidature")), 3, array(), "array"), "body", array()), "html", null, true);
        echo "</em></small></p>
                                    <input type=\"submit\" value=";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ENVOYER", array(), "Menu"), "html", null, true);
        echo " class=\"btn btn-primary\">
                                    ";
        // line 219
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
                                </div>
                                <div class=\"col-sm-12\" style=\"padding-left:14px;padding-right:14px;margin-bottom:14px;\">
                                    <p><small>";
        // line 222
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tableCandidature"]) ? $context["tableCandidature"] : $this->getContext($context, "tableCandidature")), 4, array(), "array"), "body", array()), "html", null, true);
        echo "</small>
                                        <br/>
                                        <small><a href=\"mailto:recrutement@maison-kayser.com\">";
        // line 224
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tableCandidature"]) ? $context["tableCandidature"] : $this->getContext($context, "tableCandidature")), 5, array(), "array"), "body", array()), "html", null, true);
        echo "</a></small>
                                    </p>
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
                </div>

            </div>
        </div>
    </div>
";
    }

    // line 239
    public function block_javascripts($context, array $blocks = array())
    {
        // line 240
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
    jQuery(function(\$) {
        // Affix
        \$('#myAffix').affix({ offset: { top: -100, } });
        // Active
        \$('#subnav-candidature').addClass('active');
        if (\$(window).height() >= 900 ){
            \$('#row_full_title').height(\$('#row_full_title').height() + 243);
        }
    })

    </script>

";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Default:candidature.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  490 => 240,  487 => 239,  469 => 224,  464 => 222,  458 => 219,  454 => 218,  450 => 217,  446 => 215,  440 => 212,  434 => 209,  426 => 204,  418 => 199,  410 => 194,  404 => 191,  397 => 187,  391 => 184,  385 => 181,  379 => 178,  373 => 175,  367 => 172,  362 => 170,  357 => 168,  351 => 165,  343 => 160,  335 => 155,  327 => 150,  321 => 147,  314 => 143,  308 => 140,  302 => 137,  296 => 134,  290 => 131,  284 => 128,  279 => 126,  274 => 124,  268 => 121,  260 => 116,  252 => 111,  244 => 106,  238 => 103,  231 => 99,  225 => 96,  219 => 93,  213 => 90,  207 => 87,  201 => 84,  196 => 82,  191 => 80,  185 => 77,  177 => 72,  169 => 67,  161 => 62,  155 => 59,  148 => 55,  142 => 52,  136 => 49,  130 => 46,  124 => 43,  118 => 40,  115 => 39,  113 => 38,  109 => 37,  105 => 36,  98 => 32,  94 => 31,  91 => 30,  82 => 27,  79 => 26,  75 => 25,  68 => 20,  66 => 19,  59 => 17,  53 => 16,  49 => 15,  42 => 10,  39 => 9,  33 => 5,  30 => 4,  11 => 2,);
    }
}
