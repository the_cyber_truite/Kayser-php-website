<?php

/* KayserPlatformBundle:Header:headerhero.html.twig */
class __TwigTemplate_072cd553f0ad2a77a5c71c3fc8cef54b849fc316f9363e95c7465ed09af42947 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"feature-graphics\" id=\"parallax-slide\">

\t";
        // line 4
        echo "
\t<div id=\"hp-logo-feature\" class=\"img-responsive\"></div>
\t<div id=\"hp-scrolldown\"></div>
</div>";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Header:headerhero.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 4,  19 => 1,);
    }
}
