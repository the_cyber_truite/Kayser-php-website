<?php

/* KayserPlatformBundle:Header:subnav-annonces.html.twig */
class __TwigTemplate_13743ec8df2fc36d508952892c52b44b8b063c901377404b39ecb71625acc9ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"subnav hidden-sm hidden-xs col-md-1\">
    <li><a href=\"";
        // line 2
        echo $this->env->getExtension('routing')->getPath("kay_annonces");
        echo "\" id=\"subnav-annonces\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ANNONCE", array(), "Menu"), "html", null, true);
        echo "</a></li>
    <li><a href=\"";
        // line 3
        echo $this->env->getExtension('routing')->getPath("kay_candidature");
        echo "\" id=\"subnav-candidature\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CANDIDATURE", array(), "Menu"), "html", null, true);
        echo "</a></li>
</ul>";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Header:subnav-annonces.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 3,  22 => 2,  19 => 1,);
    }
}
