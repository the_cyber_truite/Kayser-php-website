<?php

/* KayserPlatformBundle:Default:methodes.html.twig */
class __TwigTemplate_13d81a33079d6443b16fd4e04152d1467b99b6af77ee744f298870be16508223 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 2
        $this->parent = $this->loadTemplate("KayserPlatformBundle::layout.html.twig", "KayserPlatformBundle:Default:methodes.html.twig", 2);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'platform_body' => array($this, 'block_platform_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "KayserPlatformBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        // line 5
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Notre Histoire
";
    }

    // line 9
    public function block_platform_body($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"container main-container\">
        <div class=\"row\">
            <div class=\"col-md-3\">
                <div data-spy=\"affix\">
                <ol class=\"breadcrumb\">
                  <li><a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("kay_homepage");
        echo "\">HOME</a></li>
                  <li><a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("kay_savoirfaire");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("SAVOIR", array(), "Menu"), "html", null, true);
        echo "</a></li>
                  <li><a href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("kay_methodes");
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("METHODES", array(), "Menu"), "html", null, true);
        echo "</a></li>
                </ol>
                ";
        // line 19
        $this->loadTemplate("KayserPlatformBundle:Header:submenu.html.twig", "KayserPlatformBundle:Default:methodes.html.twig", 19)->display($context);
        // line 20
        echo "                </div>
            </div>
            <div class=\"col-md-9\">
                <div class=\"row full tile\" style=\"margin-bottom:74px;\">
                    <div class=\"col-sm-12 history-col\">
                        <img src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("img/kayser-methode-feature.png"), "html", null, true);
        echo "\" alt=\"Fabrication du pain - Nos méthodes\">
                    </div>

                    <div class=\"row col-history-row\">
                        <div class=\"col-sm-12 col-history-content\">
                            <h3> ";
        // line 30
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 1, array(), "array"), "title", array())), "html", null, true);
        echo " </h3>
                            <p class=\"intro\"> ";
        // line 31
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 1, array(), "array"), "body", array());
        echo " </p>
                            <h4> ";
        // line 32
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 2, array(), "array"), "title", array());
        echo " </h4>
                            <p> ";
        // line 33
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 2, array(), "array"), "body", array());
        echo " </p>
                            <p> ";
        // line 34
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 3, array(), "array"), "body", array());
        echo " </p>
                            <p> ";
        // line 35
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 4, array(), "array"), "body", array());
        echo " </p>
                            <p> ";
        // line 36
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 5, array(), "array"), "body", array());
        echo " </p>
                            <h4> ";
        // line 37
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 6, array(), "array"), "title", array());
        echo " </h4>
                            <p> ";
        // line 38
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 6, array(), "array"), "body", array());
        echo " </p>
                            <h4> ";
        // line 39
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 7, array(), "array"), "title", array());
        echo " </h4>
                            <p> ";
        // line 40
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 7, array(), "array"), "body", array());
        echo " </p>
                            <h4> ";
        // line 41
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 8, array(), "array"), "title", array());
        echo " </h4>
                            <div class=\"row product-video\">
                                <div class=\"col-sm-7\">
                                        <p class=\"video-text\"> ";
        // line 44
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 8, array(), "array"), "body", array());
        echo " </p>
                                    </div>
                                <div class=\"col-sm-5\">
                                    <iframe width=\"200\" height=\"112\" src=\"";
        // line 47
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 8, array(), "array"), "url", array());
        echo "\" frameborder=\"0\" allowfullscreen></iframe>
                                </div>
                            </div>
                            <p class=\"border-video\">&nbsp;</p>
                            <div class=\"row product-video\">
                                <div class=\"col-sm-7\">
                                    <p class=\"video-text\"> ";
        // line 53
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 9, array(), "array"), "body", array());
        echo " </p>
                                </div>
                                <div class=\"col-sm-5\">
                                    <iframe width=\"200\" height=\"112\" src=\"";
        // line 56
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 9, array(), "array"), "url", array());
        echo "\" frameborder=\"0\" allowfullscreen></iframe>
                                </div>
                            </div>
                            <p class=\"border-video\">&nbsp;</p>
                            <div class=\"row product-video\">
                                <div class=\"col-sm-7\">
                                    <p class=\"video-text\"> ";
        // line 62
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 10, array(), "array"), "body", array());
        echo " </p>                   
                                </div>
                                <div class=\"col-sm-5\">
                                    <iframe width=\"200\" height=\"112\" src=\"";
        // line 65
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 10, array(), "array"), "url", array());
        echo "\" frameborder=\"0\" allowfullscreen></iframe>
                                </div>
                            </div>
                            <p class=\"border-video\">&nbsp;</p>
                            <div class=\"row product-video\">
                                <div class=\"col-sm-7\">
                                    <p class=\"video-text\"> ";
        // line 71
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 11, array(), "array"), "body", array());
        echo " </p>
                                </div>
                                <div class=\"col-sm-5\">
                                    <iframe width=\"200\" height=\"112\" src=\"";
        // line 74
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 11, array(), "array"), "url", array());
        echo "\" frameborder=\"0\" allowfullscreen></iframe>
                                </div>
                            </div>
                            <p class=\"border\">&nbsp;</p>
                            <div class=\"row product-video\">
                                <div class=\"col-sm-7\">
                                    <p class=\"video-text\"> ";
        // line 80
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 12, array(), "array"), "body", array());
        echo " </p>
                                </div>
                                <div class=\"col-sm-5\">
                                    <iframe width=\"200\" height=\"112\" src=\"";
        // line 83
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 12, array(), "array"), "url", array());
        echo "\" frameborder=\"0\" allowfullscreen></iframe>
                                </div>
                            </div>
                            <p class=\"border\">&nbsp;</p>
                            <div class=\"row product-video\">
                                <div class=\"col-sm-7\">
                                    <p class=\"video-text\"> ";
        // line 89
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 13, array(), "array"), "body", array());
        echo " </p>
                                </div>
                                <div class=\"col-sm-5\">
                                    <iframe width=\"200\" height=\"112\" src=\"";
        // line 92
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 13, array(), "array"), "url", array());
        echo "\" frameborder=\"0\" allowfullscreen></iframe>
                                </div>
                            </div>
                            <p class=\"border\">&nbsp;</p>
                            ";
        // line 96
        echo $this->getAttribute($this->getAttribute((isset($context["tableMethodes"]) ? $context["tableMethodes"] : $this->getContext($context, "tableMethodes")), 14, array(), "array"), "body", array());
        echo "
                        </div>
                    </div>

                </div>

                <div class=\"row\">
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 104
        echo $this->env->getExtension('routing')->getPath("kay_histoire");
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfFeature"]) ? $context["sfFeature"] : $this->getContext($context, "sfFeature")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"Monsieur Kayser - Notre histoire\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 107
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfFeature"]) ? $context["sfFeature"] : $this->getContext($context, "sfFeature")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 108
        echo $this->getAttribute((isset($context["sfFeature"]) ? $context["sfFeature"] : $this->getContext($context, "sfFeature")), "body", array());
        echo "</p>
                            </div>
                        </div></a>
                    </div>
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 113
        echo $this->env->getExtension('routing')->getPath("kay_philosophie");
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfUn"]) ? $context["sfUn"] : $this->getContext($context, "sfUn")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"Du bon pain ! - Philosophie\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 116
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfUn"]) ? $context["sfUn"] : $this->getContext($context, "sfUn")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 117
        echo $this->getAttribute((isset($context["sfUn"]) ? $context["sfUn"] : $this->getContext($context, "sfUn")), "body", array());
        echo "</p>
                            </div>
                        </div></a>
                    </div>
                    <div class=\"col-sm-4\">
                        <a href=\"";
        // line 122
        echo $this->env->getExtension('routing')->getPath("kay_equipes");
        echo "\" class=\"tile-link\"><div class=\"row tile over sized\">
                            <img src=\"";
        // line 123
        echo twig_escape_filter($this->env, $this->env->getExtension('liip_imagine')->filter($this->getAttribute($this->getAttribute((isset($context["sfTrois"]) ? $context["sfTrois"] : $this->getContext($context, "sfTrois")), "image", array()), "webPath", array()), "kay_bloc"), "html", null, true);
        echo "\" alt=\"Nos compagnons - Nos équipes\">
                            <div class=\"col-sm-12\">
                                <h4>";
        // line 125
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, $this->getAttribute((isset($context["sfTrois"]) ? $context["sfTrois"] : $this->getContext($context, "sfTrois")), "title", array())), "html", null, true);
        echo "</h4>
                                <p>";
        // line 126
        echo $this->getAttribute((isset($context["sfTrois"]) ? $context["sfTrois"] : $this->getContext($context, "sfTrois")), "body", array());
        echo "</p>
                            </div>
                        </div></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 136
    public function block_javascripts($context, array $blocks = array())
    {
        // line 137
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
    jQuery(function(\$) {
        // Affix
        \$('#myAffix').affix({ offset: { top: -100, } });
        // Active
        \$('#gnav-savoirfaire, #dropnav-methodes, #subnav-methodes').addClass('active');
    })
    </script>
";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Default:methodes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  303 => 137,  300 => 136,  287 => 126,  283 => 125,  278 => 123,  274 => 122,  266 => 117,  262 => 116,  257 => 114,  253 => 113,  245 => 108,  241 => 107,  236 => 105,  232 => 104,  221 => 96,  214 => 92,  208 => 89,  199 => 83,  193 => 80,  184 => 74,  178 => 71,  169 => 65,  163 => 62,  154 => 56,  148 => 53,  139 => 47,  133 => 44,  127 => 41,  123 => 40,  119 => 39,  115 => 38,  111 => 37,  107 => 36,  103 => 35,  99 => 34,  95 => 33,  91 => 32,  87 => 31,  83 => 30,  75 => 25,  68 => 20,  66 => 19,  59 => 17,  53 => 16,  49 => 15,  42 => 10,  39 => 9,  33 => 5,  30 => 4,  11 => 2,);
    }
}
