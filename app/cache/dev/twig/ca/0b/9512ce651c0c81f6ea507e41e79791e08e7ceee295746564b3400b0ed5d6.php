<?php

/* KayserPlatformBundle:Header:subnav-locator.html.twig */
class __TwigTemplate_ca0b9512ce651c0c81f6ea507e41e79791e08e7ceee295746564b3400b0ed5d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h4 class=\"hidden-sm hidden-xs\">";
        echo "</h4>
<ul class=\"subnav hidden-sm hidden-xs\">
    <li><a href=\"#\" class=\"locator-link\" id=\"locator-FR\" data-location=\"48.8566140, 2.3522219\" data-zoom=\"4\">";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("FRANCE", array(), "Menu"), "html", null, true);
        echo "</a></li>
    <ul>
        <li><a href=\"";
        // line 5
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=paris\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PARIS", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 6
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=avignon\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("AVIGNON", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=beausoleil\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("BEAUSOLEIL", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 8
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=lyon\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("LYON", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 9
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=menton\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("MENTON", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=lille\" class=\"locator-link\">LILLE</a></li>
    </ul>
    <li><a href=\"#\" id=\"locator-INTL\">";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("INTERNATIONAL", array(), "Menu"), "html", null, true);
        echo "</a></li>
    <ul>
        <li><a href=\"";
        // line 14
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=saudiarabia\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ARABES", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=cambodia\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CAMBODGE", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 16
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=chile\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CHILI", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=colombia\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("COLOMBIE", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=southcorea\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("COREE", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"#\" class=\"locator-link gray\">";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("EGYPT", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=emirates\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ARABES2", array(), "Menu"), "html", null, true);
        echo "</a></li>
        ";
        // line 22
        echo "        <li><a href=\"";
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=hongkong\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("HONG KONG", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 23
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=indonesia\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("INDONESIE", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 24
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=japan\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("JAPON", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <!-- <li><a href=\"";
        // line 25
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=lebanon\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("LIBAN", array(), "Menu"), "html", null, true);
        echo "</a></li> -->
        <li><a href=\"";
        // line 26
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=morocco\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("LIBAN", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 27
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=mexico\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("MEXIQUE", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 28
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=unitedstates\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("NEW YORK", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 29
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=philippines\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PHILIPPINES", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 30
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=portugal\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PORTUGAL", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 31
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=rdc\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("REP", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 32
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=senegal\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("SENEGAL", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=singapore\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("SINGAPOUR", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=taiwan\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("TAIWAN", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 35
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=thailand\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("THAILANDE", array(), "Menu"), "html", null, true);
        echo "</a></li>
        <li><a href=\"";
        // line 36
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "?loc=tunisia\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("TUNISIE", array(), "Menu"), "html", null, true);
        echo "</a></li>
    </ul>
    <li><a id=\"localize\" href=\"";
        // line 38
        echo $this->env->getExtension('routing')->getPath("kay_noustrouver");
        echo "\" class=\"locator-link\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("POSITION", array(), "Menu"), "html", null, true);
        echo "</a></li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "KayserPlatformBundle:Header:subnav-locator.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 38,  194 => 36,  188 => 35,  182 => 34,  176 => 33,  170 => 32,  164 => 31,  158 => 30,  152 => 29,  146 => 28,  140 => 27,  134 => 26,  128 => 25,  122 => 24,  116 => 23,  109 => 22,  103 => 20,  99 => 19,  93 => 18,  87 => 17,  81 => 16,  75 => 15,  69 => 14,  64 => 12,  59 => 10,  53 => 9,  47 => 8,  41 => 7,  35 => 6,  29 => 5,  24 => 3,  19 => 1,);
    }
}
