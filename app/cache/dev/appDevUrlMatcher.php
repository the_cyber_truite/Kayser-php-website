<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            if (0 === strpos($pathinfo, '/_configurator')) {
                // _configurator_home
                if (rtrim($pathinfo, '/') === '/_configurator') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_configurator_home');
                    }

                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::checkAction',  '_route' => '_configurator_home',);
                }

                // _configurator_step
                if (0 === strpos($pathinfo, '/_configurator/step') && preg_match('#^/_configurator/step/(?P<index>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_configurator_step')), array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::stepAction',));
                }

                // _configurator_final
                if ($pathinfo === '/_configurator/final') {
                    return array (  '_controller' => 'Sensio\\Bundle\\DistributionBundle\\Controller\\ConfiguratorController::finalAction',  '_route' => '_configurator_final',);
                }

            }

        }

        // logout
        if ($pathinfo === '/logout') {
            return array('_route' => 'logout');
        }

        // kay_homepage
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'kay_homepage');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_homepage')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::indexAction',));
        }

        // kay_savoirfaire
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/savoir\\-faire$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_savoirfaire')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::savoirfaireAction',));
        }

        // kay_histoire
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/histoire$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_histoire')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::histoireAction',));
        }

        // kay_philosophie
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/philosophie$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_philosophie')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::philosophieAction',));
        }

        // kay_methodes
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/methodes$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_methodes')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::methodesAction',));
        }

        // kay_equipes
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/equipes$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_equipes')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::equipesAction',));
        }

        // kay_produits
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/produits$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_produits')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::produitsAction',));
        }

        // kay_produits_carte
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/produits/(?P<slug>patisseries|sales)/carte$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_produits_carte')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::carteAction',));
        }

        // kay_produits_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/produits/(?P<category>[^/]++)(?:/(?P<subcategory>[^/]++))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_produits_list')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::produitsListAction',  'subcategory' => 'all',));
        }

        // kay_videos
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/videos$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_videos')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::videosAction',));
        }

        // kay_noustrouver
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/nous\\-trouver$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_noustrouver')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::trouverAction',));
        }

        // kay_contact
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/contact(?:(?P<trailingSlash>[/]{0,1}))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_contact')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::contactAction',  'trailingSlash' => '/',  'slug' => NULL,));
        }

        // kay_candidature
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/candidature(?:(?P<trailingSlash>[/]{0,1}))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_candidature')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::candidatureAction',  'trailingSlash' => '/',  'slug' => NULL,));
        }

        // kay_credits
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/informations\\-legales$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_credits')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::creditsAction',));
        }

        // kay_annonces
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/annonces(?:(?P<trailingSlash>[/]{0,1})(?:(?P<slug>[^/]++))?)?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_annonces')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::annoncesAction',  'trailingSlash' => '/',  'slug' => NULL,));
        }

        // kay_clients
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/clients\\-professionnels(?:/(?P<slug>hotels-restaurants|entreprises)(?:(?P<trailingSlash>[/]{0,1}))?)?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_clients')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::contactAction',  'trailingSlash' => '/',  'slug' => 'hotels-restaurants',));
        }

        // kay_stores
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/stores$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_stores')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::storesAction',));
        }

        // kay_faq
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/faq$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'kay_faq')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::faqAction',));
        }

        // sonata_admin_redirect
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'sonata_admin_redirect');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_redirect')), array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::redirectAction',  'route' => 'sonata_admin_dashboard',  'permanent' => 'true',));
        }

        // sonata_admin_dashboard
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/dashboard$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_dashboard')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',));
        }

        // sonata_admin_retrieve_form_element
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/core/get\\-form\\-field\\-element$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_retrieve_form_element')), array (  '_controller' => 'sonata.admin.controller.admin:retrieveFormFieldElementAction',));
        }

        // sonata_admin_append_form_element
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/core/append\\-form\\-field\\-element$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_append_form_element')), array (  '_controller' => 'sonata.admin.controller.admin:appendFormFieldElementAction',));
        }

        // sonata_admin_short_object_information
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/core/get\\-short\\-object\\-description(?:\\.(?P<_format>html|json))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_short_object_information')), array (  '_controller' => 'sonata.admin.controller.admin:getShortObjectDescriptionAction',  '_format' => 'html',));
        }

        // sonata_admin_set_object_field_value
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/core/set\\-object\\-field\\-value$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_set_object_field_value')), array (  '_controller' => 'sonata.admin.controller.admin:setObjectFieldValueAction',));
        }

        // sonata_admin_search
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/search$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_search')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::searchAction',));
        }

        // sonata_admin_retrieve_autocomplete_items
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/core/get\\-autocomplete\\-items$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_retrieve_autocomplete_items')), array (  '_controller' => 'sonata.admin.controller.admin:retrieveAutocompleteItemsAction',));
        }

        // admin_kayser_platform_blockimage_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/blockimage/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_blockimage_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.image',  '_sonata_name' => 'admin_kayser_platform_blockimage_list',));
        }

        // admin_kayser_platform_blockimage_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/blockimage/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_blockimage_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.image',  '_sonata_name' => 'admin_kayser_platform_blockimage_create',));
        }

        // admin_kayser_platform_blockimage_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/blockimage/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_blockimage_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.image',  '_sonata_name' => 'admin_kayser_platform_blockimage_batch',));
        }

        // admin_kayser_platform_blockimage_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/blockimage/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_blockimage_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.image',  '_sonata_name' => 'admin_kayser_platform_blockimage_edit',));
        }

        // admin_kayser_platform_blockimage_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/blockimage/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_blockimage_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.image',  '_sonata_name' => 'admin_kayser_platform_blockimage_delete',));
        }

        // admin_kayser_platform_blockimage_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/blockimage/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_blockimage_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.image',  '_sonata_name' => 'admin_kayser_platform_blockimage_show',));
        }

        // admin_kayser_platform_blockimage_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/blockimage/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_blockimage_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.image',  '_sonata_name' => 'admin_kayser_platform_blockimage_export',));
        }

        // admin_kayser_platform_block_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/block/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_block_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.block',  '_sonata_name' => 'admin_kayser_platform_block_list',));
        }

        // admin_kayser_platform_block_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/block/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_block_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.block',  '_sonata_name' => 'admin_kayser_platform_block_create',));
        }

        // admin_kayser_platform_block_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/block/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_block_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.block',  '_sonata_name' => 'admin_kayser_platform_block_batch',));
        }

        // admin_kayser_platform_block_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/block/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_block_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.block',  '_sonata_name' => 'admin_kayser_platform_block_edit',));
        }

        // admin_kayser_platform_block_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/block/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_block_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.block',  '_sonata_name' => 'admin_kayser_platform_block_delete',));
        }

        // admin_kayser_platform_block_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/block/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_block_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.block',  '_sonata_name' => 'admin_kayser_platform_block_show',));
        }

        // admin_kayser_platform_block_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/block/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_block_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.block',  '_sonata_name' => 'admin_kayser_platform_block_export',));
        }

        // admin_kayser_platform_histoire_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/histoire/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_histoire_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.histoire',  '_sonata_name' => 'admin_kayser_platform_histoire_list',));
        }

        // admin_kayser_platform_histoire_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/histoire/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_histoire_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.histoire',  '_sonata_name' => 'admin_kayser_platform_histoire_create',));
        }

        // admin_kayser_platform_histoire_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/histoire/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_histoire_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.histoire',  '_sonata_name' => 'admin_kayser_platform_histoire_batch',));
        }

        // admin_kayser_platform_histoire_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/histoire/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_histoire_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.histoire',  '_sonata_name' => 'admin_kayser_platform_histoire_edit',));
        }

        // admin_kayser_platform_histoire_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/histoire/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_histoire_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.histoire',  '_sonata_name' => 'admin_kayser_platform_histoire_delete',));
        }

        // admin_kayser_platform_histoire_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/histoire/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_histoire_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.histoire',  '_sonata_name' => 'admin_kayser_platform_histoire_show',));
        }

        // admin_kayser_platform_histoire_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/histoire/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_histoire_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.histoire',  '_sonata_name' => 'admin_kayser_platform_histoire_export',));
        }

        // admin_kayser_platform_philosophie_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/philosophie/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_philosophie_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.philosophie',  '_sonata_name' => 'admin_kayser_platform_philosophie_list',));
        }

        // admin_kayser_platform_philosophie_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/philosophie/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_philosophie_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.philosophie',  '_sonata_name' => 'admin_kayser_platform_philosophie_create',));
        }

        // admin_kayser_platform_philosophie_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/philosophie/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_philosophie_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.philosophie',  '_sonata_name' => 'admin_kayser_platform_philosophie_batch',));
        }

        // admin_kayser_platform_philosophie_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/philosophie/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_philosophie_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.philosophie',  '_sonata_name' => 'admin_kayser_platform_philosophie_edit',));
        }

        // admin_kayser_platform_philosophie_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/philosophie/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_philosophie_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.philosophie',  '_sonata_name' => 'admin_kayser_platform_philosophie_delete',));
        }

        // admin_kayser_platform_philosophie_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/philosophie/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_philosophie_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.philosophie',  '_sonata_name' => 'admin_kayser_platform_philosophie_show',));
        }

        // admin_kayser_platform_philosophie_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/philosophie/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_philosophie_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.philosophie',  '_sonata_name' => 'admin_kayser_platform_philosophie_export',));
        }

        // admin_kayser_platform_methodes_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/methodes/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_methodes_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.methodes',  '_sonata_name' => 'admin_kayser_platform_methodes_list',));
        }

        // admin_kayser_platform_methodes_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/methodes/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_methodes_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.methodes',  '_sonata_name' => 'admin_kayser_platform_methodes_create',));
        }

        // admin_kayser_platform_methodes_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/methodes/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_methodes_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.methodes',  '_sonata_name' => 'admin_kayser_platform_methodes_batch',));
        }

        // admin_kayser_platform_methodes_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/methodes/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_methodes_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.methodes',  '_sonata_name' => 'admin_kayser_platform_methodes_edit',));
        }

        // admin_kayser_platform_methodes_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/methodes/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_methodes_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.methodes',  '_sonata_name' => 'admin_kayser_platform_methodes_delete',));
        }

        // admin_kayser_platform_methodes_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/methodes/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_methodes_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.methodes',  '_sonata_name' => 'admin_kayser_platform_methodes_show',));
        }

        // admin_kayser_platform_methodes_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/methodes/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_methodes_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.methodes',  '_sonata_name' => 'admin_kayser_platform_methodes_export',));
        }

        // admin_kayser_platform_equipes_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/equipes/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_equipes_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.equipes',  '_sonata_name' => 'admin_kayser_platform_equipes_list',));
        }

        // admin_kayser_platform_equipes_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/equipes/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_equipes_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.equipes',  '_sonata_name' => 'admin_kayser_platform_equipes_create',));
        }

        // admin_kayser_platform_equipes_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/equipes/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_equipes_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.equipes',  '_sonata_name' => 'admin_kayser_platform_equipes_batch',));
        }

        // admin_kayser_platform_equipes_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/equipes/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_equipes_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.equipes',  '_sonata_name' => 'admin_kayser_platform_equipes_edit',));
        }

        // admin_kayser_platform_equipes_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/equipes/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_equipes_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.equipes',  '_sonata_name' => 'admin_kayser_platform_equipes_delete',));
        }

        // admin_kayser_platform_equipes_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/equipes/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_equipes_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.equipes',  '_sonata_name' => 'admin_kayser_platform_equipes_show',));
        }

        // admin_kayser_platform_equipes_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/equipes/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_equipes_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.equipes',  '_sonata_name' => 'admin_kayser_platform_equipes_export',));
        }

        // admin_kayser_platform_product_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/product/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_product_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.produit',  '_sonata_name' => 'admin_kayser_platform_product_list',));
        }

        // admin_kayser_platform_product_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/product/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_product_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.produit',  '_sonata_name' => 'admin_kayser_platform_product_create',));
        }

        // admin_kayser_platform_product_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/product/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_product_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.produit',  '_sonata_name' => 'admin_kayser_platform_product_batch',));
        }

        // admin_kayser_platform_product_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/product/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_product_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.produit',  '_sonata_name' => 'admin_kayser_platform_product_edit',));
        }

        // admin_kayser_platform_product_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/product/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_product_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.produit',  '_sonata_name' => 'admin_kayser_platform_product_delete',));
        }

        // admin_kayser_platform_product_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/product/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_product_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.produit',  '_sonata_name' => 'admin_kayser_platform_product_show',));
        }

        // admin_kayser_platform_product_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/product/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_product_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.produit',  '_sonata_name' => 'admin_kayser_platform_product_export',));
        }

        // admin_kayser_platform_productimage_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/productimage/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_productimage_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.imageproduit',  '_sonata_name' => 'admin_kayser_platform_productimage_list',));
        }

        // admin_kayser_platform_productimage_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/productimage/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_productimage_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.imageproduit',  '_sonata_name' => 'admin_kayser_platform_productimage_create',));
        }

        // admin_kayser_platform_productimage_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/productimage/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_productimage_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.imageproduit',  '_sonata_name' => 'admin_kayser_platform_productimage_batch',));
        }

        // admin_kayser_platform_productimage_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/productimage/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_productimage_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.imageproduit',  '_sonata_name' => 'admin_kayser_platform_productimage_edit',));
        }

        // admin_kayser_platform_productimage_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/productimage/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_productimage_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.imageproduit',  '_sonata_name' => 'admin_kayser_platform_productimage_delete',));
        }

        // admin_kayser_platform_productimage_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/productimage/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_productimage_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.imageproduit',  '_sonata_name' => 'admin_kayser_platform_productimage_show',));
        }

        // admin_kayser_platform_productimage_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/productimage/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_productimage_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.imageproduit',  '_sonata_name' => 'admin_kayser_platform_productimage_export',));
        }

        // admin_kayser_platform_patisserie_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/patisserie/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_patisserie_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.patisserie',  '_sonata_name' => 'admin_kayser_platform_patisserie_list',));
        }

        // admin_kayser_platform_patisserie_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/patisserie/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_patisserie_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.patisserie',  '_sonata_name' => 'admin_kayser_platform_patisserie_create',));
        }

        // admin_kayser_platform_patisserie_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/patisserie/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_patisserie_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.patisserie',  '_sonata_name' => 'admin_kayser_platform_patisserie_batch',));
        }

        // admin_kayser_platform_patisserie_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/patisserie/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_patisserie_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.patisserie',  '_sonata_name' => 'admin_kayser_platform_patisserie_edit',));
        }

        // admin_kayser_platform_patisserie_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/patisserie/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_patisserie_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.patisserie',  '_sonata_name' => 'admin_kayser_platform_patisserie_delete',));
        }

        // admin_kayser_platform_patisserie_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/patisserie/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_patisserie_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.patisserie',  '_sonata_name' => 'admin_kayser_platform_patisserie_show',));
        }

        // admin_kayser_platform_patisserie_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/patisserie/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_patisserie_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.patisserie',  '_sonata_name' => 'admin_kayser_platform_patisserie_export',));
        }

        // admin_kayser_platform_sales_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/sales/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_sales_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.sales',  '_sonata_name' => 'admin_kayser_platform_sales_list',));
        }

        // admin_kayser_platform_sales_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/sales/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_sales_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.sales',  '_sonata_name' => 'admin_kayser_platform_sales_create',));
        }

        // admin_kayser_platform_sales_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/sales/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_sales_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.sales',  '_sonata_name' => 'admin_kayser_platform_sales_batch',));
        }

        // admin_kayser_platform_sales_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/sales/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_sales_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.sales',  '_sonata_name' => 'admin_kayser_platform_sales_edit',));
        }

        // admin_kayser_platform_sales_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/sales/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_sales_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.sales',  '_sonata_name' => 'admin_kayser_platform_sales_delete',));
        }

        // admin_kayser_platform_sales_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/sales/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_sales_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.sales',  '_sonata_name' => 'admin_kayser_platform_sales_show',));
        }

        // admin_kayser_platform_sales_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/sales/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_sales_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.sales',  '_sonata_name' => 'admin_kayser_platform_sales_export',));
        }

        // admin_kayser_platform_locations_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/locations/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_locations_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.locations',  '_sonata_name' => 'admin_kayser_platform_locations_list',));
        }

        // admin_kayser_platform_locations_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/locations/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_locations_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.locations',  '_sonata_name' => 'admin_kayser_platform_locations_create',));
        }

        // admin_kayser_platform_locations_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/locations/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_locations_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.locations',  '_sonata_name' => 'admin_kayser_platform_locations_batch',));
        }

        // admin_kayser_platform_locations_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/locations/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_locations_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.locations',  '_sonata_name' => 'admin_kayser_platform_locations_edit',));
        }

        // admin_kayser_platform_locations_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/locations/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_locations_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.locations',  '_sonata_name' => 'admin_kayser_platform_locations_delete',));
        }

        // admin_kayser_platform_locations_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/locations/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_locations_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.locations',  '_sonata_name' => 'admin_kayser_platform_locations_show',));
        }

        // admin_kayser_platform_locations_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/locations/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_locations_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.locations',  '_sonata_name' => 'admin_kayser_platform_locations_export',));
        }

        // admin_kayser_platform_candidature_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/candidature/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_candidature_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.candidature',  '_sonata_name' => 'admin_kayser_platform_candidature_list',));
        }

        // admin_kayser_platform_candidature_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/candidature/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_candidature_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.candidature',  '_sonata_name' => 'admin_kayser_platform_candidature_create',));
        }

        // admin_kayser_platform_candidature_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/candidature/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_candidature_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.candidature',  '_sonata_name' => 'admin_kayser_platform_candidature_batch',));
        }

        // admin_kayser_platform_candidature_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/candidature/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_candidature_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.candidature',  '_sonata_name' => 'admin_kayser_platform_candidature_edit',));
        }

        // admin_kayser_platform_candidature_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/candidature/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_candidature_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.candidature',  '_sonata_name' => 'admin_kayser_platform_candidature_delete',));
        }

        // admin_kayser_platform_candidature_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/candidature/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_candidature_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.candidature',  '_sonata_name' => 'admin_kayser_platform_candidature_show',));
        }

        // admin_kayser_platform_candidature_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/candidature/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_candidature_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.candidature',  '_sonata_name' => 'admin_kayser_platform_candidature_export',));
        }

        // admin_kayser_platform_annonces_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/annonces/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_annonces_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.annonces',  '_sonata_name' => 'admin_kayser_platform_annonces_list',));
        }

        // admin_kayser_platform_annonces_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/annonces/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_annonces_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.annonces',  '_sonata_name' => 'admin_kayser_platform_annonces_create',));
        }

        // admin_kayser_platform_annonces_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/annonces/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_annonces_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.annonces',  '_sonata_name' => 'admin_kayser_platform_annonces_batch',));
        }

        // admin_kayser_platform_annonces_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/annonces/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_annonces_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.annonces',  '_sonata_name' => 'admin_kayser_platform_annonces_edit',));
        }

        // admin_kayser_platform_annonces_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/annonces/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_annonces_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.annonces',  '_sonata_name' => 'admin_kayser_platform_annonces_delete',));
        }

        // admin_kayser_platform_annonces_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/annonces/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_annonces_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.annonces',  '_sonata_name' => 'admin_kayser_platform_annonces_show',));
        }

        // admin_kayser_platform_annonces_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/annonces/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_annonces_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.annonces',  '_sonata_name' => 'admin_kayser_platform_annonces_export',));
        }

        // admin_kayser_platform_annoncestext_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/annoncestext/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_annoncestext_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.annoncestexte',  '_sonata_name' => 'admin_kayser_platform_annoncestext_list',));
        }

        // admin_kayser_platform_annoncestext_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/annoncestext/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_annoncestext_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.annoncestexte',  '_sonata_name' => 'admin_kayser_platform_annoncestext_create',));
        }

        // admin_kayser_platform_annoncestext_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/annoncestext/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_annoncestext_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.annoncestexte',  '_sonata_name' => 'admin_kayser_platform_annoncestext_batch',));
        }

        // admin_kayser_platform_annoncestext_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/annoncestext/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_annoncestext_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.annoncestexte',  '_sonata_name' => 'admin_kayser_platform_annoncestext_edit',));
        }

        // admin_kayser_platform_annoncestext_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/annoncestext/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_annoncestext_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.annoncestexte',  '_sonata_name' => 'admin_kayser_platform_annoncestext_delete',));
        }

        // admin_kayser_platform_annoncestext_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/annoncestext/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_annoncestext_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.annoncestexte',  '_sonata_name' => 'admin_kayser_platform_annoncestext_show',));
        }

        // admin_kayser_platform_annoncestext_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/annoncestext/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_annoncestext_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.annoncestexte',  '_sonata_name' => 'admin_kayser_platform_annoncestext_export',));
        }

        // admin_kayser_platform_contacttext_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/contacttext/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_contacttext_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.contact',  '_sonata_name' => 'admin_kayser_platform_contacttext_list',));
        }

        // admin_kayser_platform_contacttext_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/contacttext/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_contacttext_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.contact',  '_sonata_name' => 'admin_kayser_platform_contacttext_create',));
        }

        // admin_kayser_platform_contacttext_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/contacttext/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_contacttext_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.contact',  '_sonata_name' => 'admin_kayser_platform_contacttext_batch',));
        }

        // admin_kayser_platform_contacttext_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/contacttext/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_contacttext_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.contact',  '_sonata_name' => 'admin_kayser_platform_contacttext_edit',));
        }

        // admin_kayser_platform_contacttext_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/contacttext/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_contacttext_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.contact',  '_sonata_name' => 'admin_kayser_platform_contacttext_delete',));
        }

        // admin_kayser_platform_contacttext_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/contacttext/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_contacttext_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.contact',  '_sonata_name' => 'admin_kayser_platform_contacttext_show',));
        }

        // admin_kayser_platform_contacttext_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/contacttext/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_contacttext_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.contact',  '_sonata_name' => 'admin_kayser_platform_contacttext_export',));
        }

        // admin_kayser_platform_faq_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/faq/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_faq_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.faq',  '_sonata_name' => 'admin_kayser_platform_faq_list',));
        }

        // admin_kayser_platform_faq_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/faq/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_faq_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.faq',  '_sonata_name' => 'admin_kayser_platform_faq_create',));
        }

        // admin_kayser_platform_faq_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/faq/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_faq_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.faq',  '_sonata_name' => 'admin_kayser_platform_faq_batch',));
        }

        // admin_kayser_platform_faq_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/faq/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_faq_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.faq',  '_sonata_name' => 'admin_kayser_platform_faq_edit',));
        }

        // admin_kayser_platform_faq_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/faq/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_faq_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.faq',  '_sonata_name' => 'admin_kayser_platform_faq_delete',));
        }

        // admin_kayser_platform_faq_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/faq/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_faq_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.faq',  '_sonata_name' => 'admin_kayser_platform_faq_show',));
        }

        // admin_kayser_platform_faq_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/faq/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_faq_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.faq',  '_sonata_name' => 'admin_kayser_platform_faq_export',));
        }

        // admin_kayser_platform_hotelrestaurant_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/hotelrestaurant/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_hotelrestaurant_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.hotel',  '_sonata_name' => 'admin_kayser_platform_hotelrestaurant_list',));
        }

        // admin_kayser_platform_hotelrestaurant_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/hotelrestaurant/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_hotelrestaurant_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.hotel',  '_sonata_name' => 'admin_kayser_platform_hotelrestaurant_create',));
        }

        // admin_kayser_platform_hotelrestaurant_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/hotelrestaurant/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_hotelrestaurant_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.hotel',  '_sonata_name' => 'admin_kayser_platform_hotelrestaurant_batch',));
        }

        // admin_kayser_platform_hotelrestaurant_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/hotelrestaurant/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_hotelrestaurant_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.hotel',  '_sonata_name' => 'admin_kayser_platform_hotelrestaurant_edit',));
        }

        // admin_kayser_platform_hotelrestaurant_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/hotelrestaurant/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_hotelrestaurant_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.hotel',  '_sonata_name' => 'admin_kayser_platform_hotelrestaurant_delete',));
        }

        // admin_kayser_platform_hotelrestaurant_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/hotelrestaurant/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_hotelrestaurant_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.hotel',  '_sonata_name' => 'admin_kayser_platform_hotelrestaurant_show',));
        }

        // admin_kayser_platform_hotelrestaurant_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/hotelrestaurant/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_hotelrestaurant_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.hotel',  '_sonata_name' => 'admin_kayser_platform_hotelrestaurant_export',));
        }

        // admin_kayser_platform_entreprise_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/entreprise/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_entreprise_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.entreprise',  '_sonata_name' => 'admin_kayser_platform_entreprise_list',));
        }

        // admin_kayser_platform_entreprise_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/entreprise/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_entreprise_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.entreprise',  '_sonata_name' => 'admin_kayser_platform_entreprise_create',));
        }

        // admin_kayser_platform_entreprise_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/entreprise/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_entreprise_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.entreprise',  '_sonata_name' => 'admin_kayser_platform_entreprise_batch',));
        }

        // admin_kayser_platform_entreprise_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/entreprise/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_entreprise_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.entreprise',  '_sonata_name' => 'admin_kayser_platform_entreprise_edit',));
        }

        // admin_kayser_platform_entreprise_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/entreprise/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_entreprise_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.entreprise',  '_sonata_name' => 'admin_kayser_platform_entreprise_delete',));
        }

        // admin_kayser_platform_entreprise_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/entreprise/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_entreprise_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.entreprise',  '_sonata_name' => 'admin_kayser_platform_entreprise_show',));
        }

        // admin_kayser_platform_entreprise_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/entreprise/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_entreprise_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.entreprise',  '_sonata_name' => 'admin_kayser_platform_entreprise_export',));
        }

        // admin_kayser_platform_informations_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/informations/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_informations_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.informations',  '_sonata_name' => 'admin_kayser_platform_informations_list',));
        }

        // admin_kayser_platform_informations_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/informations/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_informations_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.informations',  '_sonata_name' => 'admin_kayser_platform_informations_create',));
        }

        // admin_kayser_platform_informations_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/informations/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_informations_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.informations',  '_sonata_name' => 'admin_kayser_platform_informations_batch',));
        }

        // admin_kayser_platform_informations_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/informations/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_informations_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.informations',  '_sonata_name' => 'admin_kayser_platform_informations_edit',));
        }

        // admin_kayser_platform_informations_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/informations/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_informations_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.informations',  '_sonata_name' => 'admin_kayser_platform_informations_delete',));
        }

        // admin_kayser_platform_informations_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/informations/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_informations_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.informations',  '_sonata_name' => 'admin_kayser_platform_informations_show',));
        }

        // admin_kayser_platform_informations_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/informations/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_informations_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.informations',  '_sonata_name' => 'admin_kayser_platform_informations_export',));
        }

        // admin_kayser_platform_engblock_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engblock/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engblock_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.engblock',  '_sonata_name' => 'admin_kayser_platform_engblock_list',));
        }

        // admin_kayser_platform_engblock_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engblock/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engblock_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.engblock',  '_sonata_name' => 'admin_kayser_platform_engblock_create',));
        }

        // admin_kayser_platform_engblock_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engblock/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engblock_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.engblock',  '_sonata_name' => 'admin_kayser_platform_engblock_batch',));
        }

        // admin_kayser_platform_engblock_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engblock/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engblock_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.engblock',  '_sonata_name' => 'admin_kayser_platform_engblock_edit',));
        }

        // admin_kayser_platform_engblock_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engblock/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engblock_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.engblock',  '_sonata_name' => 'admin_kayser_platform_engblock_delete',));
        }

        // admin_kayser_platform_engblock_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engblock/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engblock_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.engblock',  '_sonata_name' => 'admin_kayser_platform_engblock_show',));
        }

        // admin_kayser_platform_engblock_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engblock/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engblock_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.engblock',  '_sonata_name' => 'admin_kayser_platform_engblock_export',));
        }

        // admin_kayser_platform_enghistoire_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enghistoire/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enghistoire_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.enghistoire',  '_sonata_name' => 'admin_kayser_platform_enghistoire_list',));
        }

        // admin_kayser_platform_enghistoire_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enghistoire/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enghistoire_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.enghistoire',  '_sonata_name' => 'admin_kayser_platform_enghistoire_create',));
        }

        // admin_kayser_platform_enghistoire_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enghistoire/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enghistoire_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.enghistoire',  '_sonata_name' => 'admin_kayser_platform_enghistoire_batch',));
        }

        // admin_kayser_platform_enghistoire_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enghistoire/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enghistoire_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.enghistoire',  '_sonata_name' => 'admin_kayser_platform_enghistoire_edit',));
        }

        // admin_kayser_platform_enghistoire_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enghistoire/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enghistoire_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.enghistoire',  '_sonata_name' => 'admin_kayser_platform_enghistoire_delete',));
        }

        // admin_kayser_platform_enghistoire_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enghistoire/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enghistoire_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.enghistoire',  '_sonata_name' => 'admin_kayser_platform_enghistoire_show',));
        }

        // admin_kayser_platform_enghistoire_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enghistoire/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enghistoire_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.enghistoire',  '_sonata_name' => 'admin_kayser_platform_enghistoire_export',));
        }

        // admin_kayser_platform_engphilosophie_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engphilosophie/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engphilosophie_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.engphilosophie',  '_sonata_name' => 'admin_kayser_platform_engphilosophie_list',));
        }

        // admin_kayser_platform_engphilosophie_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engphilosophie/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engphilosophie_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.engphilosophie',  '_sonata_name' => 'admin_kayser_platform_engphilosophie_create',));
        }

        // admin_kayser_platform_engphilosophie_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engphilosophie/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engphilosophie_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.engphilosophie',  '_sonata_name' => 'admin_kayser_platform_engphilosophie_batch',));
        }

        // admin_kayser_platform_engphilosophie_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engphilosophie/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engphilosophie_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.engphilosophie',  '_sonata_name' => 'admin_kayser_platform_engphilosophie_edit',));
        }

        // admin_kayser_platform_engphilosophie_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engphilosophie/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engphilosophie_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.engphilosophie',  '_sonata_name' => 'admin_kayser_platform_engphilosophie_delete',));
        }

        // admin_kayser_platform_engphilosophie_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engphilosophie/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engphilosophie_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.engphilosophie',  '_sonata_name' => 'admin_kayser_platform_engphilosophie_show',));
        }

        // admin_kayser_platform_engphilosophie_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engphilosophie/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engphilosophie_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.engphilosophie',  '_sonata_name' => 'admin_kayser_platform_engphilosophie_export',));
        }

        // admin_kayser_platform_engmethodes_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engmethodes/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engmethodes_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.engmethodes',  '_sonata_name' => 'admin_kayser_platform_engmethodes_list',));
        }

        // admin_kayser_platform_engmethodes_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engmethodes/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engmethodes_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.engmethodes',  '_sonata_name' => 'admin_kayser_platform_engmethodes_create',));
        }

        // admin_kayser_platform_engmethodes_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engmethodes/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engmethodes_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.engmethodes',  '_sonata_name' => 'admin_kayser_platform_engmethodes_batch',));
        }

        // admin_kayser_platform_engmethodes_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engmethodes/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engmethodes_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.engmethodes',  '_sonata_name' => 'admin_kayser_platform_engmethodes_edit',));
        }

        // admin_kayser_platform_engmethodes_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engmethodes/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engmethodes_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.engmethodes',  '_sonata_name' => 'admin_kayser_platform_engmethodes_delete',));
        }

        // admin_kayser_platform_engmethodes_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engmethodes/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engmethodes_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.engmethodes',  '_sonata_name' => 'admin_kayser_platform_engmethodes_show',));
        }

        // admin_kayser_platform_engmethodes_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engmethodes/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engmethodes_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.engmethodes',  '_sonata_name' => 'admin_kayser_platform_engmethodes_export',));
        }

        // admin_kayser_platform_engequipes_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engequipes/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engequipes_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.engequipes',  '_sonata_name' => 'admin_kayser_platform_engequipes_list',));
        }

        // admin_kayser_platform_engequipes_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engequipes/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engequipes_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.engequipes',  '_sonata_name' => 'admin_kayser_platform_engequipes_create',));
        }

        // admin_kayser_platform_engequipes_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engequipes/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engequipes_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.engequipes',  '_sonata_name' => 'admin_kayser_platform_engequipes_batch',));
        }

        // admin_kayser_platform_engequipes_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engequipes/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engequipes_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.engequipes',  '_sonata_name' => 'admin_kayser_platform_engequipes_edit',));
        }

        // admin_kayser_platform_engequipes_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engequipes/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engequipes_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.engequipes',  '_sonata_name' => 'admin_kayser_platform_engequipes_delete',));
        }

        // admin_kayser_platform_engequipes_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engequipes/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engequipes_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.engequipes',  '_sonata_name' => 'admin_kayser_platform_engequipes_show',));
        }

        // admin_kayser_platform_engequipes_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engequipes/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engequipes_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.engequipes',  '_sonata_name' => 'admin_kayser_platform_engequipes_export',));
        }

        // admin_kayser_platform_engproduct_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engproduct/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engproduct_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.engproduit',  '_sonata_name' => 'admin_kayser_platform_engproduct_list',));
        }

        // admin_kayser_platform_engproduct_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engproduct/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engproduct_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.engproduit',  '_sonata_name' => 'admin_kayser_platform_engproduct_create',));
        }

        // admin_kayser_platform_engproduct_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engproduct/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engproduct_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.engproduit',  '_sonata_name' => 'admin_kayser_platform_engproduct_batch',));
        }

        // admin_kayser_platform_engproduct_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engproduct/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engproduct_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.engproduit',  '_sonata_name' => 'admin_kayser_platform_engproduct_edit',));
        }

        // admin_kayser_platform_engproduct_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engproduct/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engproduct_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.engproduit',  '_sonata_name' => 'admin_kayser_platform_engproduct_delete',));
        }

        // admin_kayser_platform_engproduct_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engproduct/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engproduct_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.engproduit',  '_sonata_name' => 'admin_kayser_platform_engproduct_show',));
        }

        // admin_kayser_platform_engproduct_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engproduct/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engproduct_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.engproduit',  '_sonata_name' => 'admin_kayser_platform_engproduct_export',));
        }

        // admin_kayser_platform_engpatisserie_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engpatisserie/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engpatisserie_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.engpatisserie',  '_sonata_name' => 'admin_kayser_platform_engpatisserie_list',));
        }

        // admin_kayser_platform_engpatisserie_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engpatisserie/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engpatisserie_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.engpatisserie',  '_sonata_name' => 'admin_kayser_platform_engpatisserie_create',));
        }

        // admin_kayser_platform_engpatisserie_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engpatisserie/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engpatisserie_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.engpatisserie',  '_sonata_name' => 'admin_kayser_platform_engpatisserie_batch',));
        }

        // admin_kayser_platform_engpatisserie_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engpatisserie/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engpatisserie_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.engpatisserie',  '_sonata_name' => 'admin_kayser_platform_engpatisserie_edit',));
        }

        // admin_kayser_platform_engpatisserie_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engpatisserie/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engpatisserie_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.engpatisserie',  '_sonata_name' => 'admin_kayser_platform_engpatisserie_delete',));
        }

        // admin_kayser_platform_engpatisserie_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engpatisserie/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engpatisserie_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.engpatisserie',  '_sonata_name' => 'admin_kayser_platform_engpatisserie_show',));
        }

        // admin_kayser_platform_engpatisserie_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engpatisserie/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engpatisserie_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.engpatisserie',  '_sonata_name' => 'admin_kayser_platform_engpatisserie_export',));
        }

        // admin_kayser_platform_engsales_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engsales/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engsales_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.engsales',  '_sonata_name' => 'admin_kayser_platform_engsales_list',));
        }

        // admin_kayser_platform_engsales_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engsales/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engsales_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.engsales',  '_sonata_name' => 'admin_kayser_platform_engsales_create',));
        }

        // admin_kayser_platform_engsales_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engsales/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engsales_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.engsales',  '_sonata_name' => 'admin_kayser_platform_engsales_batch',));
        }

        // admin_kayser_platform_engsales_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engsales/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engsales_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.engsales',  '_sonata_name' => 'admin_kayser_platform_engsales_edit',));
        }

        // admin_kayser_platform_engsales_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engsales/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engsales_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.engsales',  '_sonata_name' => 'admin_kayser_platform_engsales_delete',));
        }

        // admin_kayser_platform_engsales_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engsales/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engsales_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.engsales',  '_sonata_name' => 'admin_kayser_platform_engsales_show',));
        }

        // admin_kayser_platform_engsales_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engsales/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engsales_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.engsales',  '_sonata_name' => 'admin_kayser_platform_engsales_export',));
        }

        // admin_kayser_platform_engcandidature_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engcandidature/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engcandidature_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.engcandidature',  '_sonata_name' => 'admin_kayser_platform_engcandidature_list',));
        }

        // admin_kayser_platform_engcandidature_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engcandidature/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engcandidature_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.engcandidature',  '_sonata_name' => 'admin_kayser_platform_engcandidature_create',));
        }

        // admin_kayser_platform_engcandidature_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engcandidature/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engcandidature_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.engcandidature',  '_sonata_name' => 'admin_kayser_platform_engcandidature_batch',));
        }

        // admin_kayser_platform_engcandidature_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engcandidature/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engcandidature_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.engcandidature',  '_sonata_name' => 'admin_kayser_platform_engcandidature_edit',));
        }

        // admin_kayser_platform_engcandidature_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engcandidature/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engcandidature_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.engcandidature',  '_sonata_name' => 'admin_kayser_platform_engcandidature_delete',));
        }

        // admin_kayser_platform_engcandidature_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engcandidature/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engcandidature_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.engcandidature',  '_sonata_name' => 'admin_kayser_platform_engcandidature_show',));
        }

        // admin_kayser_platform_engcandidature_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engcandidature/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engcandidature_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.engcandidature',  '_sonata_name' => 'admin_kayser_platform_engcandidature_export',));
        }

        // admin_kayser_platform_engannonces_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engannonces/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engannonces_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.engannonces',  '_sonata_name' => 'admin_kayser_platform_engannonces_list',));
        }

        // admin_kayser_platform_engannonces_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engannonces/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engannonces_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.engannonces',  '_sonata_name' => 'admin_kayser_platform_engannonces_create',));
        }

        // admin_kayser_platform_engannonces_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engannonces/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engannonces_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.engannonces',  '_sonata_name' => 'admin_kayser_platform_engannonces_batch',));
        }

        // admin_kayser_platform_engannonces_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engannonces/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engannonces_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.engannonces',  '_sonata_name' => 'admin_kayser_platform_engannonces_edit',));
        }

        // admin_kayser_platform_engannonces_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engannonces/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engannonces_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.engannonces',  '_sonata_name' => 'admin_kayser_platform_engannonces_delete',));
        }

        // admin_kayser_platform_engannonces_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engannonces/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engannonces_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.engannonces',  '_sonata_name' => 'admin_kayser_platform_engannonces_show',));
        }

        // admin_kayser_platform_engannonces_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engannonces/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engannonces_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.engannonces',  '_sonata_name' => 'admin_kayser_platform_engannonces_export',));
        }

        // admin_kayser_platform_engannoncestext_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engannoncestext/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engannoncestext_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.engannoncestexte',  '_sonata_name' => 'admin_kayser_platform_engannoncestext_list',));
        }

        // admin_kayser_platform_engannoncestext_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engannoncestext/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engannoncestext_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.engannoncestexte',  '_sonata_name' => 'admin_kayser_platform_engannoncestext_create',));
        }

        // admin_kayser_platform_engannoncestext_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engannoncestext/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engannoncestext_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.engannoncestexte',  '_sonata_name' => 'admin_kayser_platform_engannoncestext_batch',));
        }

        // admin_kayser_platform_engannoncestext_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engannoncestext/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engannoncestext_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.engannoncestexte',  '_sonata_name' => 'admin_kayser_platform_engannoncestext_edit',));
        }

        // admin_kayser_platform_engannoncestext_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engannoncestext/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engannoncestext_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.engannoncestexte',  '_sonata_name' => 'admin_kayser_platform_engannoncestext_delete',));
        }

        // admin_kayser_platform_engannoncestext_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engannoncestext/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engannoncestext_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.engannoncestexte',  '_sonata_name' => 'admin_kayser_platform_engannoncestext_show',));
        }

        // admin_kayser_platform_engannoncestext_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engannoncestext/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engannoncestext_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.engannoncestexte',  '_sonata_name' => 'admin_kayser_platform_engannoncestext_export',));
        }

        // admin_kayser_platform_engcontacttext_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engcontacttext/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engcontacttext_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.engcontact',  '_sonata_name' => 'admin_kayser_platform_engcontacttext_list',));
        }

        // admin_kayser_platform_engcontacttext_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engcontacttext/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engcontacttext_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.engcontact',  '_sonata_name' => 'admin_kayser_platform_engcontacttext_create',));
        }

        // admin_kayser_platform_engcontacttext_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engcontacttext/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engcontacttext_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.engcontact',  '_sonata_name' => 'admin_kayser_platform_engcontacttext_batch',));
        }

        // admin_kayser_platform_engcontacttext_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engcontacttext/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engcontacttext_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.engcontact',  '_sonata_name' => 'admin_kayser_platform_engcontacttext_edit',));
        }

        // admin_kayser_platform_engcontacttext_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engcontacttext/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engcontacttext_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.engcontact',  '_sonata_name' => 'admin_kayser_platform_engcontacttext_delete',));
        }

        // admin_kayser_platform_engcontacttext_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engcontacttext/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engcontacttext_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.engcontact',  '_sonata_name' => 'admin_kayser_platform_engcontacttext_show',));
        }

        // admin_kayser_platform_engcontacttext_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engcontacttext/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engcontacttext_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.engcontact',  '_sonata_name' => 'admin_kayser_platform_engcontacttext_export',));
        }

        // admin_kayser_platform_engfaq_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engfaq/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engfaq_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.engfaq',  '_sonata_name' => 'admin_kayser_platform_engfaq_list',));
        }

        // admin_kayser_platform_engfaq_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engfaq/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engfaq_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.engfaq',  '_sonata_name' => 'admin_kayser_platform_engfaq_create',));
        }

        // admin_kayser_platform_engfaq_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engfaq/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engfaq_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.engfaq',  '_sonata_name' => 'admin_kayser_platform_engfaq_batch',));
        }

        // admin_kayser_platform_engfaq_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engfaq/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engfaq_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.engfaq',  '_sonata_name' => 'admin_kayser_platform_engfaq_edit',));
        }

        // admin_kayser_platform_engfaq_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engfaq/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engfaq_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.engfaq',  '_sonata_name' => 'admin_kayser_platform_engfaq_delete',));
        }

        // admin_kayser_platform_engfaq_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engfaq/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engfaq_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.engfaq',  '_sonata_name' => 'admin_kayser_platform_engfaq_show',));
        }

        // admin_kayser_platform_engfaq_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engfaq/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engfaq_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.engfaq',  '_sonata_name' => 'admin_kayser_platform_engfaq_export',));
        }

        // admin_kayser_platform_enghotelrestaurant_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enghotelrestaurant/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enghotelrestaurant_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.enghotel',  '_sonata_name' => 'admin_kayser_platform_enghotelrestaurant_list',));
        }

        // admin_kayser_platform_enghotelrestaurant_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enghotelrestaurant/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enghotelrestaurant_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.enghotel',  '_sonata_name' => 'admin_kayser_platform_enghotelrestaurant_create',));
        }

        // admin_kayser_platform_enghotelrestaurant_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enghotelrestaurant/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enghotelrestaurant_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.enghotel',  '_sonata_name' => 'admin_kayser_platform_enghotelrestaurant_batch',));
        }

        // admin_kayser_platform_enghotelrestaurant_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enghotelrestaurant/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enghotelrestaurant_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.enghotel',  '_sonata_name' => 'admin_kayser_platform_enghotelrestaurant_edit',));
        }

        // admin_kayser_platform_enghotelrestaurant_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enghotelrestaurant/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enghotelrestaurant_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.enghotel',  '_sonata_name' => 'admin_kayser_platform_enghotelrestaurant_delete',));
        }

        // admin_kayser_platform_enghotelrestaurant_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enghotelrestaurant/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enghotelrestaurant_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.enghotel',  '_sonata_name' => 'admin_kayser_platform_enghotelrestaurant_show',));
        }

        // admin_kayser_platform_enghotelrestaurant_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enghotelrestaurant/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enghotelrestaurant_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.enghotel',  '_sonata_name' => 'admin_kayser_platform_enghotelrestaurant_export',));
        }

        // admin_kayser_platform_engentreprise_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engentreprise/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engentreprise_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.engentreprise',  '_sonata_name' => 'admin_kayser_platform_engentreprise_list',));
        }

        // admin_kayser_platform_engentreprise_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engentreprise/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engentreprise_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.engentreprise',  '_sonata_name' => 'admin_kayser_platform_engentreprise_create',));
        }

        // admin_kayser_platform_engentreprise_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engentreprise/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engentreprise_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.engentreprise',  '_sonata_name' => 'admin_kayser_platform_engentreprise_batch',));
        }

        // admin_kayser_platform_engentreprise_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engentreprise/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engentreprise_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.engentreprise',  '_sonata_name' => 'admin_kayser_platform_engentreprise_edit',));
        }

        // admin_kayser_platform_engentreprise_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engentreprise/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engentreprise_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.engentreprise',  '_sonata_name' => 'admin_kayser_platform_engentreprise_delete',));
        }

        // admin_kayser_platform_engentreprise_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engentreprise/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engentreprise_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.engentreprise',  '_sonata_name' => 'admin_kayser_platform_engentreprise_show',));
        }

        // admin_kayser_platform_engentreprise_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/engentreprise/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_engentreprise_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.engentreprise',  '_sonata_name' => 'admin_kayser_platform_engentreprise_export',));
        }

        // admin_kayser_platform_enginformations_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enginformations/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enginformations_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.enginformations',  '_sonata_name' => 'admin_kayser_platform_enginformations_list',));
        }

        // admin_kayser_platform_enginformations_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enginformations/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enginformations_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.enginformations',  '_sonata_name' => 'admin_kayser_platform_enginformations_create',));
        }

        // admin_kayser_platform_enginformations_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enginformations/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enginformations_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.enginformations',  '_sonata_name' => 'admin_kayser_platform_enginformations_batch',));
        }

        // admin_kayser_platform_enginformations_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enginformations/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enginformations_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.enginformations',  '_sonata_name' => 'admin_kayser_platform_enginformations_edit',));
        }

        // admin_kayser_platform_enginformations_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enginformations/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enginformations_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.enginformations',  '_sonata_name' => 'admin_kayser_platform_enginformations_delete',));
        }

        // admin_kayser_platform_enginformations_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enginformations/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enginformations_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.enginformations',  '_sonata_name' => 'admin_kayser_platform_enginformations_show',));
        }

        // admin_kayser_platform_enginformations_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/enginformations/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_enginformations_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.enginformations',  '_sonata_name' => 'admin_kayser_platform_enginformations_export',));
        }

        // admin_kayser_platform_esblock_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esblock/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esblock_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.esblock',  '_sonata_name' => 'admin_kayser_platform_esblock_list',));
        }

        // admin_kayser_platform_esblock_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esblock/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esblock_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.esblock',  '_sonata_name' => 'admin_kayser_platform_esblock_create',));
        }

        // admin_kayser_platform_esblock_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esblock/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esblock_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.esblock',  '_sonata_name' => 'admin_kayser_platform_esblock_batch',));
        }

        // admin_kayser_platform_esblock_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esblock/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esblock_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.esblock',  '_sonata_name' => 'admin_kayser_platform_esblock_edit',));
        }

        // admin_kayser_platform_esblock_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esblock/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esblock_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.esblock',  '_sonata_name' => 'admin_kayser_platform_esblock_delete',));
        }

        // admin_kayser_platform_esblock_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esblock/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esblock_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.esblock',  '_sonata_name' => 'admin_kayser_platform_esblock_show',));
        }

        // admin_kayser_platform_esblock_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esblock/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esblock_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.esblock',  '_sonata_name' => 'admin_kayser_platform_esblock_export',));
        }

        // admin_kayser_platform_eshistoire_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/eshistoire/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_eshistoire_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.eshistoire',  '_sonata_name' => 'admin_kayser_platform_eshistoire_list',));
        }

        // admin_kayser_platform_eshistoire_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/eshistoire/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_eshistoire_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.eshistoire',  '_sonata_name' => 'admin_kayser_platform_eshistoire_create',));
        }

        // admin_kayser_platform_eshistoire_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/eshistoire/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_eshistoire_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.eshistoire',  '_sonata_name' => 'admin_kayser_platform_eshistoire_batch',));
        }

        // admin_kayser_platform_eshistoire_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/eshistoire/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_eshistoire_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.eshistoire',  '_sonata_name' => 'admin_kayser_platform_eshistoire_edit',));
        }

        // admin_kayser_platform_eshistoire_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/eshistoire/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_eshistoire_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.eshistoire',  '_sonata_name' => 'admin_kayser_platform_eshistoire_delete',));
        }

        // admin_kayser_platform_eshistoire_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/eshistoire/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_eshistoire_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.eshistoire',  '_sonata_name' => 'admin_kayser_platform_eshistoire_show',));
        }

        // admin_kayser_platform_eshistoire_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/eshistoire/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_eshistoire_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.eshistoire',  '_sonata_name' => 'admin_kayser_platform_eshistoire_export',));
        }

        // admin_kayser_platform_esphilosophie_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esphilosophie/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esphilosophie_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.esphilosophie',  '_sonata_name' => 'admin_kayser_platform_esphilosophie_list',));
        }

        // admin_kayser_platform_esphilosophie_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esphilosophie/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esphilosophie_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.esphilosophie',  '_sonata_name' => 'admin_kayser_platform_esphilosophie_create',));
        }

        // admin_kayser_platform_esphilosophie_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esphilosophie/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esphilosophie_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.esphilosophie',  '_sonata_name' => 'admin_kayser_platform_esphilosophie_batch',));
        }

        // admin_kayser_platform_esphilosophie_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esphilosophie/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esphilosophie_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.esphilosophie',  '_sonata_name' => 'admin_kayser_platform_esphilosophie_edit',));
        }

        // admin_kayser_platform_esphilosophie_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esphilosophie/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esphilosophie_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.esphilosophie',  '_sonata_name' => 'admin_kayser_platform_esphilosophie_delete',));
        }

        // admin_kayser_platform_esphilosophie_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esphilosophie/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esphilosophie_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.esphilosophie',  '_sonata_name' => 'admin_kayser_platform_esphilosophie_show',));
        }

        // admin_kayser_platform_esphilosophie_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esphilosophie/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esphilosophie_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.esphilosophie',  '_sonata_name' => 'admin_kayser_platform_esphilosophie_export',));
        }

        // admin_kayser_platform_esmethodes_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esmethodes/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esmethodes_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.esmethodes',  '_sonata_name' => 'admin_kayser_platform_esmethodes_list',));
        }

        // admin_kayser_platform_esmethodes_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esmethodes/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esmethodes_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.esmethodes',  '_sonata_name' => 'admin_kayser_platform_esmethodes_create',));
        }

        // admin_kayser_platform_esmethodes_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esmethodes/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esmethodes_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.esmethodes',  '_sonata_name' => 'admin_kayser_platform_esmethodes_batch',));
        }

        // admin_kayser_platform_esmethodes_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esmethodes/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esmethodes_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.esmethodes',  '_sonata_name' => 'admin_kayser_platform_esmethodes_edit',));
        }

        // admin_kayser_platform_esmethodes_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esmethodes/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esmethodes_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.esmethodes',  '_sonata_name' => 'admin_kayser_platform_esmethodes_delete',));
        }

        // admin_kayser_platform_esmethodes_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esmethodes/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esmethodes_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.esmethodes',  '_sonata_name' => 'admin_kayser_platform_esmethodes_show',));
        }

        // admin_kayser_platform_esmethodes_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esmethodes/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esmethodes_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.esmethodes',  '_sonata_name' => 'admin_kayser_platform_esmethodes_export',));
        }

        // admin_kayser_platform_esequipes_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esequipes/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esequipes_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.esequipes',  '_sonata_name' => 'admin_kayser_platform_esequipes_list',));
        }

        // admin_kayser_platform_esequipes_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esequipes/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esequipes_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.esequipes',  '_sonata_name' => 'admin_kayser_platform_esequipes_create',));
        }

        // admin_kayser_platform_esequipes_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esequipes/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esequipes_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.esequipes',  '_sonata_name' => 'admin_kayser_platform_esequipes_batch',));
        }

        // admin_kayser_platform_esequipes_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esequipes/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esequipes_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.esequipes',  '_sonata_name' => 'admin_kayser_platform_esequipes_edit',));
        }

        // admin_kayser_platform_esequipes_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esequipes/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esequipes_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.esequipes',  '_sonata_name' => 'admin_kayser_platform_esequipes_delete',));
        }

        // admin_kayser_platform_esequipes_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esequipes/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esequipes_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.esequipes',  '_sonata_name' => 'admin_kayser_platform_esequipes_show',));
        }

        // admin_kayser_platform_esequipes_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esequipes/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esequipes_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.esequipes',  '_sonata_name' => 'admin_kayser_platform_esequipes_export',));
        }

        // admin_kayser_platform_esproduct_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esproduct/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esproduct_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.esproduit',  '_sonata_name' => 'admin_kayser_platform_esproduct_list',));
        }

        // admin_kayser_platform_esproduct_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esproduct/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esproduct_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.esproduit',  '_sonata_name' => 'admin_kayser_platform_esproduct_create',));
        }

        // admin_kayser_platform_esproduct_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esproduct/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esproduct_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.esproduit',  '_sonata_name' => 'admin_kayser_platform_esproduct_batch',));
        }

        // admin_kayser_platform_esproduct_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esproduct/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esproduct_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.esproduit',  '_sonata_name' => 'admin_kayser_platform_esproduct_edit',));
        }

        // admin_kayser_platform_esproduct_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esproduct/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esproduct_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.esproduit',  '_sonata_name' => 'admin_kayser_platform_esproduct_delete',));
        }

        // admin_kayser_platform_esproduct_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esproduct/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esproduct_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.esproduit',  '_sonata_name' => 'admin_kayser_platform_esproduct_show',));
        }

        // admin_kayser_platform_esproduct_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esproduct/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esproduct_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.esproduit',  '_sonata_name' => 'admin_kayser_platform_esproduct_export',));
        }

        // admin_kayser_platform_espatisserie_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/espatisserie/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_espatisserie_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.espatisserie',  '_sonata_name' => 'admin_kayser_platform_espatisserie_list',));
        }

        // admin_kayser_platform_espatisserie_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/espatisserie/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_espatisserie_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.espatisserie',  '_sonata_name' => 'admin_kayser_platform_espatisserie_create',));
        }

        // admin_kayser_platform_espatisserie_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/espatisserie/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_espatisserie_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.espatisserie',  '_sonata_name' => 'admin_kayser_platform_espatisserie_batch',));
        }

        // admin_kayser_platform_espatisserie_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/espatisserie/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_espatisserie_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.espatisserie',  '_sonata_name' => 'admin_kayser_platform_espatisserie_edit',));
        }

        // admin_kayser_platform_espatisserie_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/espatisserie/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_espatisserie_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.espatisserie',  '_sonata_name' => 'admin_kayser_platform_espatisserie_delete',));
        }

        // admin_kayser_platform_espatisserie_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/espatisserie/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_espatisserie_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.espatisserie',  '_sonata_name' => 'admin_kayser_platform_espatisserie_show',));
        }

        // admin_kayser_platform_espatisserie_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/espatisserie/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_espatisserie_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.espatisserie',  '_sonata_name' => 'admin_kayser_platform_espatisserie_export',));
        }

        // admin_kayser_platform_essales_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/essales/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_essales_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.essales',  '_sonata_name' => 'admin_kayser_platform_essales_list',));
        }

        // admin_kayser_platform_essales_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/essales/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_essales_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.essales',  '_sonata_name' => 'admin_kayser_platform_essales_create',));
        }

        // admin_kayser_platform_essales_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/essales/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_essales_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.essales',  '_sonata_name' => 'admin_kayser_platform_essales_batch',));
        }

        // admin_kayser_platform_essales_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/essales/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_essales_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.essales',  '_sonata_name' => 'admin_kayser_platform_essales_edit',));
        }

        // admin_kayser_platform_essales_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/essales/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_essales_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.essales',  '_sonata_name' => 'admin_kayser_platform_essales_delete',));
        }

        // admin_kayser_platform_essales_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/essales/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_essales_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.essales',  '_sonata_name' => 'admin_kayser_platform_essales_show',));
        }

        // admin_kayser_platform_essales_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/essales/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_essales_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.essales',  '_sonata_name' => 'admin_kayser_platform_essales_export',));
        }

        // admin_kayser_platform_escandidature_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/escandidature/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_escandidature_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.escandidature',  '_sonata_name' => 'admin_kayser_platform_escandidature_list',));
        }

        // admin_kayser_platform_escandidature_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/escandidature/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_escandidature_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.escandidature',  '_sonata_name' => 'admin_kayser_platform_escandidature_create',));
        }

        // admin_kayser_platform_escandidature_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/escandidature/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_escandidature_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.escandidature',  '_sonata_name' => 'admin_kayser_platform_escandidature_batch',));
        }

        // admin_kayser_platform_escandidature_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/escandidature/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_escandidature_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.escandidature',  '_sonata_name' => 'admin_kayser_platform_escandidature_edit',));
        }

        // admin_kayser_platform_escandidature_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/escandidature/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_escandidature_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.escandidature',  '_sonata_name' => 'admin_kayser_platform_escandidature_delete',));
        }

        // admin_kayser_platform_escandidature_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/escandidature/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_escandidature_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.escandidature',  '_sonata_name' => 'admin_kayser_platform_escandidature_show',));
        }

        // admin_kayser_platform_escandidature_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/escandidature/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_escandidature_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.escandidature',  '_sonata_name' => 'admin_kayser_platform_escandidature_export',));
        }

        // admin_kayser_platform_esannonces_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esannonces/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esannonces_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.esannonces',  '_sonata_name' => 'admin_kayser_platform_esannonces_list',));
        }

        // admin_kayser_platform_esannonces_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esannonces/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esannonces_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.esannonces',  '_sonata_name' => 'admin_kayser_platform_esannonces_create',));
        }

        // admin_kayser_platform_esannonces_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esannonces/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esannonces_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.esannonces',  '_sonata_name' => 'admin_kayser_platform_esannonces_batch',));
        }

        // admin_kayser_platform_esannonces_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esannonces/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esannonces_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.esannonces',  '_sonata_name' => 'admin_kayser_platform_esannonces_edit',));
        }

        // admin_kayser_platform_esannonces_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esannonces/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esannonces_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.esannonces',  '_sonata_name' => 'admin_kayser_platform_esannonces_delete',));
        }

        // admin_kayser_platform_esannonces_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esannonces/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esannonces_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.esannonces',  '_sonata_name' => 'admin_kayser_platform_esannonces_show',));
        }

        // admin_kayser_platform_esannonces_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esannonces/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esannonces_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.esannonces',  '_sonata_name' => 'admin_kayser_platform_esannonces_export',));
        }

        // admin_kayser_platform_esannoncestext_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esannoncestext/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esannoncestext_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.esannoncestexte',  '_sonata_name' => 'admin_kayser_platform_esannoncestext_list',));
        }

        // admin_kayser_platform_esannoncestext_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esannoncestext/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esannoncestext_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.esannoncestexte',  '_sonata_name' => 'admin_kayser_platform_esannoncestext_create',));
        }

        // admin_kayser_platform_esannoncestext_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esannoncestext/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esannoncestext_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.esannoncestexte',  '_sonata_name' => 'admin_kayser_platform_esannoncestext_batch',));
        }

        // admin_kayser_platform_esannoncestext_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esannoncestext/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esannoncestext_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.esannoncestexte',  '_sonata_name' => 'admin_kayser_platform_esannoncestext_edit',));
        }

        // admin_kayser_platform_esannoncestext_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esannoncestext/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esannoncestext_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.esannoncestexte',  '_sonata_name' => 'admin_kayser_platform_esannoncestext_delete',));
        }

        // admin_kayser_platform_esannoncestext_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esannoncestext/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esannoncestext_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.esannoncestexte',  '_sonata_name' => 'admin_kayser_platform_esannoncestext_show',));
        }

        // admin_kayser_platform_esannoncestext_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esannoncestext/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esannoncestext_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.esannoncestexte',  '_sonata_name' => 'admin_kayser_platform_esannoncestext_export',));
        }

        // admin_kayser_platform_escontacttext_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/escontacttext/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_escontacttext_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.escontact',  '_sonata_name' => 'admin_kayser_platform_escontacttext_list',));
        }

        // admin_kayser_platform_escontacttext_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/escontacttext/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_escontacttext_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.escontact',  '_sonata_name' => 'admin_kayser_platform_escontacttext_create',));
        }

        // admin_kayser_platform_escontacttext_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/escontacttext/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_escontacttext_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.escontact',  '_sonata_name' => 'admin_kayser_platform_escontacttext_batch',));
        }

        // admin_kayser_platform_escontacttext_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/escontacttext/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_escontacttext_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.escontact',  '_sonata_name' => 'admin_kayser_platform_escontacttext_edit',));
        }

        // admin_kayser_platform_escontacttext_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/escontacttext/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_escontacttext_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.escontact',  '_sonata_name' => 'admin_kayser_platform_escontacttext_delete',));
        }

        // admin_kayser_platform_escontacttext_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/escontacttext/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_escontacttext_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.escontact',  '_sonata_name' => 'admin_kayser_platform_escontacttext_show',));
        }

        // admin_kayser_platform_escontacttext_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/escontacttext/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_escontacttext_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.escontact',  '_sonata_name' => 'admin_kayser_platform_escontacttext_export',));
        }

        // admin_kayser_platform_esfaq_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esfaq/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esfaq_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.esfaq',  '_sonata_name' => 'admin_kayser_platform_esfaq_list',));
        }

        // admin_kayser_platform_esfaq_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esfaq/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esfaq_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.esfaq',  '_sonata_name' => 'admin_kayser_platform_esfaq_create',));
        }

        // admin_kayser_platform_esfaq_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esfaq/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esfaq_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.esfaq',  '_sonata_name' => 'admin_kayser_platform_esfaq_batch',));
        }

        // admin_kayser_platform_esfaq_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esfaq/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esfaq_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.esfaq',  '_sonata_name' => 'admin_kayser_platform_esfaq_edit',));
        }

        // admin_kayser_platform_esfaq_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esfaq/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esfaq_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.esfaq',  '_sonata_name' => 'admin_kayser_platform_esfaq_delete',));
        }

        // admin_kayser_platform_esfaq_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esfaq/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esfaq_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.esfaq',  '_sonata_name' => 'admin_kayser_platform_esfaq_show',));
        }

        // admin_kayser_platform_esfaq_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esfaq/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esfaq_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.esfaq',  '_sonata_name' => 'admin_kayser_platform_esfaq_export',));
        }

        // admin_kayser_platform_eshotelrestaurant_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/eshotelrestaurant/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_eshotelrestaurant_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.eshotel',  '_sonata_name' => 'admin_kayser_platform_eshotelrestaurant_list',));
        }

        // admin_kayser_platform_eshotelrestaurant_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/eshotelrestaurant/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_eshotelrestaurant_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.eshotel',  '_sonata_name' => 'admin_kayser_platform_eshotelrestaurant_create',));
        }

        // admin_kayser_platform_eshotelrestaurant_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/eshotelrestaurant/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_eshotelrestaurant_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.eshotel',  '_sonata_name' => 'admin_kayser_platform_eshotelrestaurant_batch',));
        }

        // admin_kayser_platform_eshotelrestaurant_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/eshotelrestaurant/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_eshotelrestaurant_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.eshotel',  '_sonata_name' => 'admin_kayser_platform_eshotelrestaurant_edit',));
        }

        // admin_kayser_platform_eshotelrestaurant_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/eshotelrestaurant/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_eshotelrestaurant_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.eshotel',  '_sonata_name' => 'admin_kayser_platform_eshotelrestaurant_delete',));
        }

        // admin_kayser_platform_eshotelrestaurant_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/eshotelrestaurant/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_eshotelrestaurant_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.eshotel',  '_sonata_name' => 'admin_kayser_platform_eshotelrestaurant_show',));
        }

        // admin_kayser_platform_eshotelrestaurant_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/eshotelrestaurant/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_eshotelrestaurant_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.eshotel',  '_sonata_name' => 'admin_kayser_platform_eshotelrestaurant_export',));
        }

        // admin_kayser_platform_esentreprise_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esentreprise/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esentreprise_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.esentreprise',  '_sonata_name' => 'admin_kayser_platform_esentreprise_list',));
        }

        // admin_kayser_platform_esentreprise_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esentreprise/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esentreprise_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.esentreprise',  '_sonata_name' => 'admin_kayser_platform_esentreprise_create',));
        }

        // admin_kayser_platform_esentreprise_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esentreprise/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esentreprise_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.esentreprise',  '_sonata_name' => 'admin_kayser_platform_esentreprise_batch',));
        }

        // admin_kayser_platform_esentreprise_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esentreprise/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esentreprise_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.esentreprise',  '_sonata_name' => 'admin_kayser_platform_esentreprise_edit',));
        }

        // admin_kayser_platform_esentreprise_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esentreprise/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esentreprise_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.esentreprise',  '_sonata_name' => 'admin_kayser_platform_esentreprise_delete',));
        }

        // admin_kayser_platform_esentreprise_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esentreprise/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esentreprise_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.esentreprise',  '_sonata_name' => 'admin_kayser_platform_esentreprise_show',));
        }

        // admin_kayser_platform_esentreprise_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esentreprise/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esentreprise_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.esentreprise',  '_sonata_name' => 'admin_kayser_platform_esentreprise_export',));
        }

        // admin_kayser_platform_esinformations_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esinformations/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esinformations_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.esinformations',  '_sonata_name' => 'admin_kayser_platform_esinformations_list',));
        }

        // admin_kayser_platform_esinformations_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esinformations/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esinformations_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.esinformations',  '_sonata_name' => 'admin_kayser_platform_esinformations_create',));
        }

        // admin_kayser_platform_esinformations_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esinformations/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esinformations_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.esinformations',  '_sonata_name' => 'admin_kayser_platform_esinformations_batch',));
        }

        // admin_kayser_platform_esinformations_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esinformations/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esinformations_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.esinformations',  '_sonata_name' => 'admin_kayser_platform_esinformations_edit',));
        }

        // admin_kayser_platform_esinformations_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esinformations/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esinformations_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.esinformations',  '_sonata_name' => 'admin_kayser_platform_esinformations_delete',));
        }

        // admin_kayser_platform_esinformations_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esinformations/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esinformations_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.esinformations',  '_sonata_name' => 'admin_kayser_platform_esinformations_show',));
        }

        // admin_kayser_platform_esinformations_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/esinformations/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_esinformations_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.esinformations',  '_sonata_name' => 'admin_kayser_platform_esinformations_export',));
        }

        // admin_kayser_platform_jpblock_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpblock/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpblock_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jpblock',  '_sonata_name' => 'admin_kayser_platform_jpblock_list',));
        }

        // admin_kayser_platform_jpblock_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpblock/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpblock_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jpblock',  '_sonata_name' => 'admin_kayser_platform_jpblock_create',));
        }

        // admin_kayser_platform_jpblock_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpblock/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpblock_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jpblock',  '_sonata_name' => 'admin_kayser_platform_jpblock_batch',));
        }

        // admin_kayser_platform_jpblock_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpblock/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpblock_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jpblock',  '_sonata_name' => 'admin_kayser_platform_jpblock_edit',));
        }

        // admin_kayser_platform_jpblock_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpblock/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpblock_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jpblock',  '_sonata_name' => 'admin_kayser_platform_jpblock_delete',));
        }

        // admin_kayser_platform_jpblock_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpblock/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpblock_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jpblock',  '_sonata_name' => 'admin_kayser_platform_jpblock_show',));
        }

        // admin_kayser_platform_jpblock_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpblock/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpblock_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jpblock',  '_sonata_name' => 'admin_kayser_platform_jpblock_export',));
        }

        // admin_kayser_platform_jphistoire_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jphistoire/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jphistoire_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jphistoire',  '_sonata_name' => 'admin_kayser_platform_jphistoire_list',));
        }

        // admin_kayser_platform_jphistoire_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jphistoire/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jphistoire_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jphistoire',  '_sonata_name' => 'admin_kayser_platform_jphistoire_create',));
        }

        // admin_kayser_platform_jphistoire_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jphistoire/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jphistoire_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jphistoire',  '_sonata_name' => 'admin_kayser_platform_jphistoire_batch',));
        }

        // admin_kayser_platform_jphistoire_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jphistoire/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jphistoire_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jphistoire',  '_sonata_name' => 'admin_kayser_platform_jphistoire_edit',));
        }

        // admin_kayser_platform_jphistoire_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jphistoire/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jphistoire_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jphistoire',  '_sonata_name' => 'admin_kayser_platform_jphistoire_delete',));
        }

        // admin_kayser_platform_jphistoire_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jphistoire/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jphistoire_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jphistoire',  '_sonata_name' => 'admin_kayser_platform_jphistoire_show',));
        }

        // admin_kayser_platform_jphistoire_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jphistoire/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jphistoire_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jphistoire',  '_sonata_name' => 'admin_kayser_platform_jphistoire_export',));
        }

        // admin_kayser_platform_jpphilosophie_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpphilosophie/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpphilosophie_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jpphilosophie',  '_sonata_name' => 'admin_kayser_platform_jpphilosophie_list',));
        }

        // admin_kayser_platform_jpphilosophie_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpphilosophie/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpphilosophie_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jpphilosophie',  '_sonata_name' => 'admin_kayser_platform_jpphilosophie_create',));
        }

        // admin_kayser_platform_jpphilosophie_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpphilosophie/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpphilosophie_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jpphilosophie',  '_sonata_name' => 'admin_kayser_platform_jpphilosophie_batch',));
        }

        // admin_kayser_platform_jpphilosophie_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpphilosophie/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpphilosophie_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jpphilosophie',  '_sonata_name' => 'admin_kayser_platform_jpphilosophie_edit',));
        }

        // admin_kayser_platform_jpphilosophie_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpphilosophie/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpphilosophie_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jpphilosophie',  '_sonata_name' => 'admin_kayser_platform_jpphilosophie_delete',));
        }

        // admin_kayser_platform_jpphilosophie_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpphilosophie/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpphilosophie_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jpphilosophie',  '_sonata_name' => 'admin_kayser_platform_jpphilosophie_show',));
        }

        // admin_kayser_platform_jpphilosophie_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpphilosophie/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpphilosophie_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jpphilosophie',  '_sonata_name' => 'admin_kayser_platform_jpphilosophie_export',));
        }

        // admin_kayser_platform_jpmethodes_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpmethodes/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpmethodes_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jpmethodes',  '_sonata_name' => 'admin_kayser_platform_jpmethodes_list',));
        }

        // admin_kayser_platform_jpmethodes_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpmethodes/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpmethodes_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jpmethodes',  '_sonata_name' => 'admin_kayser_platform_jpmethodes_create',));
        }

        // admin_kayser_platform_jpmethodes_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpmethodes/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpmethodes_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jpmethodes',  '_sonata_name' => 'admin_kayser_platform_jpmethodes_batch',));
        }

        // admin_kayser_platform_jpmethodes_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpmethodes/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpmethodes_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jpmethodes',  '_sonata_name' => 'admin_kayser_platform_jpmethodes_edit',));
        }

        // admin_kayser_platform_jpmethodes_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpmethodes/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpmethodes_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jpmethodes',  '_sonata_name' => 'admin_kayser_platform_jpmethodes_delete',));
        }

        // admin_kayser_platform_jpmethodes_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpmethodes/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpmethodes_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jpmethodes',  '_sonata_name' => 'admin_kayser_platform_jpmethodes_show',));
        }

        // admin_kayser_platform_jpmethodes_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpmethodes/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpmethodes_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jpmethodes',  '_sonata_name' => 'admin_kayser_platform_jpmethodes_export',));
        }

        // admin_kayser_platform_jpequipes_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpequipes/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpequipes_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jpequipes',  '_sonata_name' => 'admin_kayser_platform_jpequipes_list',));
        }

        // admin_kayser_platform_jpequipes_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpequipes/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpequipes_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jpequipes',  '_sonata_name' => 'admin_kayser_platform_jpequipes_create',));
        }

        // admin_kayser_platform_jpequipes_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpequipes/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpequipes_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jpequipes',  '_sonata_name' => 'admin_kayser_platform_jpequipes_batch',));
        }

        // admin_kayser_platform_jpequipes_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpequipes/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpequipes_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jpequipes',  '_sonata_name' => 'admin_kayser_platform_jpequipes_edit',));
        }

        // admin_kayser_platform_jpequipes_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpequipes/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpequipes_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jpequipes',  '_sonata_name' => 'admin_kayser_platform_jpequipes_delete',));
        }

        // admin_kayser_platform_jpequipes_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpequipes/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpequipes_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jpequipes',  '_sonata_name' => 'admin_kayser_platform_jpequipes_show',));
        }

        // admin_kayser_platform_jpequipes_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpequipes/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpequipes_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jpequipes',  '_sonata_name' => 'admin_kayser_platform_jpequipes_export',));
        }

        // admin_kayser_platform_jpproduct_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpproduct/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpproduct_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jpproduit',  '_sonata_name' => 'admin_kayser_platform_jpproduct_list',));
        }

        // admin_kayser_platform_jpproduct_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpproduct/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpproduct_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jpproduit',  '_sonata_name' => 'admin_kayser_platform_jpproduct_create',));
        }

        // admin_kayser_platform_jpproduct_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpproduct/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpproduct_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jpproduit',  '_sonata_name' => 'admin_kayser_platform_jpproduct_batch',));
        }

        // admin_kayser_platform_jpproduct_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpproduct/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpproduct_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jpproduit',  '_sonata_name' => 'admin_kayser_platform_jpproduct_edit',));
        }

        // admin_kayser_platform_jpproduct_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpproduct/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpproduct_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jpproduit',  '_sonata_name' => 'admin_kayser_platform_jpproduct_delete',));
        }

        // admin_kayser_platform_jpproduct_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpproduct/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpproduct_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jpproduit',  '_sonata_name' => 'admin_kayser_platform_jpproduct_show',));
        }

        // admin_kayser_platform_jpproduct_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpproduct/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpproduct_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jpproduit',  '_sonata_name' => 'admin_kayser_platform_jpproduct_export',));
        }

        // admin_kayser_platform_jppatisserie_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jppatisserie/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jppatisserie_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jppatisserie',  '_sonata_name' => 'admin_kayser_platform_jppatisserie_list',));
        }

        // admin_kayser_platform_jppatisserie_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jppatisserie/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jppatisserie_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jppatisserie',  '_sonata_name' => 'admin_kayser_platform_jppatisserie_create',));
        }

        // admin_kayser_platform_jppatisserie_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jppatisserie/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jppatisserie_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jppatisserie',  '_sonata_name' => 'admin_kayser_platform_jppatisserie_batch',));
        }

        // admin_kayser_platform_jppatisserie_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jppatisserie/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jppatisserie_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jppatisserie',  '_sonata_name' => 'admin_kayser_platform_jppatisserie_edit',));
        }

        // admin_kayser_platform_jppatisserie_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jppatisserie/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jppatisserie_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jppatisserie',  '_sonata_name' => 'admin_kayser_platform_jppatisserie_delete',));
        }

        // admin_kayser_platform_jppatisserie_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jppatisserie/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jppatisserie_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jppatisserie',  '_sonata_name' => 'admin_kayser_platform_jppatisserie_show',));
        }

        // admin_kayser_platform_jppatisserie_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jppatisserie/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jppatisserie_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jppatisserie',  '_sonata_name' => 'admin_kayser_platform_jppatisserie_export',));
        }

        // admin_kayser_platform_jpsales_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpsales/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpsales_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jpsales',  '_sonata_name' => 'admin_kayser_platform_jpsales_list',));
        }

        // admin_kayser_platform_jpsales_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpsales/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpsales_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jpsales',  '_sonata_name' => 'admin_kayser_platform_jpsales_create',));
        }

        // admin_kayser_platform_jpsales_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpsales/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpsales_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jpsales',  '_sonata_name' => 'admin_kayser_platform_jpsales_batch',));
        }

        // admin_kayser_platform_jpsales_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpsales/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpsales_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jpsales',  '_sonata_name' => 'admin_kayser_platform_jpsales_edit',));
        }

        // admin_kayser_platform_jpsales_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpsales/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpsales_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jpsales',  '_sonata_name' => 'admin_kayser_platform_jpsales_delete',));
        }

        // admin_kayser_platform_jpsales_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpsales/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpsales_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jpsales',  '_sonata_name' => 'admin_kayser_platform_jpsales_show',));
        }

        // admin_kayser_platform_jpsales_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpsales/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpsales_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jpsales',  '_sonata_name' => 'admin_kayser_platform_jpsales_export',));
        }

        // admin_kayser_platform_jpcandidature_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpcandidature/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpcandidature_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jpcandidature',  '_sonata_name' => 'admin_kayser_platform_jpcandidature_list',));
        }

        // admin_kayser_platform_jpcandidature_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpcandidature/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpcandidature_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jpcandidature',  '_sonata_name' => 'admin_kayser_platform_jpcandidature_create',));
        }

        // admin_kayser_platform_jpcandidature_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpcandidature/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpcandidature_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jpcandidature',  '_sonata_name' => 'admin_kayser_platform_jpcandidature_batch',));
        }

        // admin_kayser_platform_jpcandidature_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpcandidature/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpcandidature_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jpcandidature',  '_sonata_name' => 'admin_kayser_platform_jpcandidature_edit',));
        }

        // admin_kayser_platform_jpcandidature_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpcandidature/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpcandidature_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jpcandidature',  '_sonata_name' => 'admin_kayser_platform_jpcandidature_delete',));
        }

        // admin_kayser_platform_jpcandidature_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpcandidature/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpcandidature_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jpcandidature',  '_sonata_name' => 'admin_kayser_platform_jpcandidature_show',));
        }

        // admin_kayser_platform_jpcandidature_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpcandidature/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpcandidature_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jpcandidature',  '_sonata_name' => 'admin_kayser_platform_jpcandidature_export',));
        }

        // admin_kayser_platform_jpannonces_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpannonces/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpannonces_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jpannonces',  '_sonata_name' => 'admin_kayser_platform_jpannonces_list',));
        }

        // admin_kayser_platform_jpannonces_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpannonces/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpannonces_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jpannonces',  '_sonata_name' => 'admin_kayser_platform_jpannonces_create',));
        }

        // admin_kayser_platform_jpannonces_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpannonces/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpannonces_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jpannonces',  '_sonata_name' => 'admin_kayser_platform_jpannonces_batch',));
        }

        // admin_kayser_platform_jpannonces_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpannonces/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpannonces_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jpannonces',  '_sonata_name' => 'admin_kayser_platform_jpannonces_edit',));
        }

        // admin_kayser_platform_jpannonces_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpannonces/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpannonces_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jpannonces',  '_sonata_name' => 'admin_kayser_platform_jpannonces_delete',));
        }

        // admin_kayser_platform_jpannonces_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpannonces/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpannonces_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jpannonces',  '_sonata_name' => 'admin_kayser_platform_jpannonces_show',));
        }

        // admin_kayser_platform_jpannonces_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpannonces/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpannonces_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jpannonces',  '_sonata_name' => 'admin_kayser_platform_jpannonces_export',));
        }

        // admin_kayser_platform_jpannoncestext_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpannoncestext/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpannoncestext_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jpannoncestexte',  '_sonata_name' => 'admin_kayser_platform_jpannoncestext_list',));
        }

        // admin_kayser_platform_jpannoncestext_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpannoncestext/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpannoncestext_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jpannoncestexte',  '_sonata_name' => 'admin_kayser_platform_jpannoncestext_create',));
        }

        // admin_kayser_platform_jpannoncestext_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpannoncestext/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpannoncestext_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jpannoncestexte',  '_sonata_name' => 'admin_kayser_platform_jpannoncestext_batch',));
        }

        // admin_kayser_platform_jpannoncestext_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpannoncestext/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpannoncestext_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jpannoncestexte',  '_sonata_name' => 'admin_kayser_platform_jpannoncestext_edit',));
        }

        // admin_kayser_platform_jpannoncestext_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpannoncestext/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpannoncestext_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jpannoncestexte',  '_sonata_name' => 'admin_kayser_platform_jpannoncestext_delete',));
        }

        // admin_kayser_platform_jpannoncestext_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpannoncestext/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpannoncestext_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jpannoncestexte',  '_sonata_name' => 'admin_kayser_platform_jpannoncestext_show',));
        }

        // admin_kayser_platform_jpannoncestext_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpannoncestext/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpannoncestext_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jpannoncestexte',  '_sonata_name' => 'admin_kayser_platform_jpannoncestext_export',));
        }

        // admin_kayser_platform_jpcontacttext_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpcontacttext/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpcontacttext_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jpcontact',  '_sonata_name' => 'admin_kayser_platform_jpcontacttext_list',));
        }

        // admin_kayser_platform_jpcontacttext_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpcontacttext/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpcontacttext_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jpcontact',  '_sonata_name' => 'admin_kayser_platform_jpcontacttext_create',));
        }

        // admin_kayser_platform_jpcontacttext_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpcontacttext/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpcontacttext_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jpcontact',  '_sonata_name' => 'admin_kayser_platform_jpcontacttext_batch',));
        }

        // admin_kayser_platform_jpcontacttext_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpcontacttext/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpcontacttext_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jpcontact',  '_sonata_name' => 'admin_kayser_platform_jpcontacttext_edit',));
        }

        // admin_kayser_platform_jpcontacttext_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpcontacttext/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpcontacttext_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jpcontact',  '_sonata_name' => 'admin_kayser_platform_jpcontacttext_delete',));
        }

        // admin_kayser_platform_jpcontacttext_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpcontacttext/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpcontacttext_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jpcontact',  '_sonata_name' => 'admin_kayser_platform_jpcontacttext_show',));
        }

        // admin_kayser_platform_jpcontacttext_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpcontacttext/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpcontacttext_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jpcontact',  '_sonata_name' => 'admin_kayser_platform_jpcontacttext_export',));
        }

        // admin_kayser_platform_jpfaq_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpfaq/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpfaq_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jpfaq',  '_sonata_name' => 'admin_kayser_platform_jpfaq_list',));
        }

        // admin_kayser_platform_jpfaq_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpfaq/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpfaq_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jpfaq',  '_sonata_name' => 'admin_kayser_platform_jpfaq_create',));
        }

        // admin_kayser_platform_jpfaq_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpfaq/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpfaq_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jpfaq',  '_sonata_name' => 'admin_kayser_platform_jpfaq_batch',));
        }

        // admin_kayser_platform_jpfaq_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpfaq/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpfaq_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jpfaq',  '_sonata_name' => 'admin_kayser_platform_jpfaq_edit',));
        }

        // admin_kayser_platform_jpfaq_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpfaq/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpfaq_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jpfaq',  '_sonata_name' => 'admin_kayser_platform_jpfaq_delete',));
        }

        // admin_kayser_platform_jpfaq_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpfaq/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpfaq_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jpfaq',  '_sonata_name' => 'admin_kayser_platform_jpfaq_show',));
        }

        // admin_kayser_platform_jpfaq_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpfaq/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpfaq_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jpfaq',  '_sonata_name' => 'admin_kayser_platform_jpfaq_export',));
        }

        // admin_kayser_platform_jphotelrestaurant_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jphotelrestaurant/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jphotelrestaurant_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jphotel',  '_sonata_name' => 'admin_kayser_platform_jphotelrestaurant_list',));
        }

        // admin_kayser_platform_jphotelrestaurant_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jphotelrestaurant/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jphotelrestaurant_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jphotel',  '_sonata_name' => 'admin_kayser_platform_jphotelrestaurant_create',));
        }

        // admin_kayser_platform_jphotelrestaurant_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jphotelrestaurant/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jphotelrestaurant_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jphotel',  '_sonata_name' => 'admin_kayser_platform_jphotelrestaurant_batch',));
        }

        // admin_kayser_platform_jphotelrestaurant_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jphotelrestaurant/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jphotelrestaurant_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jphotel',  '_sonata_name' => 'admin_kayser_platform_jphotelrestaurant_edit',));
        }

        // admin_kayser_platform_jphotelrestaurant_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jphotelrestaurant/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jphotelrestaurant_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jphotel',  '_sonata_name' => 'admin_kayser_platform_jphotelrestaurant_delete',));
        }

        // admin_kayser_platform_jphotelrestaurant_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jphotelrestaurant/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jphotelrestaurant_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jphotel',  '_sonata_name' => 'admin_kayser_platform_jphotelrestaurant_show',));
        }

        // admin_kayser_platform_jphotelrestaurant_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jphotelrestaurant/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jphotelrestaurant_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jphotel',  '_sonata_name' => 'admin_kayser_platform_jphotelrestaurant_export',));
        }

        // admin_kayser_platform_jpentreprise_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpentreprise/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpentreprise_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jpentreprise',  '_sonata_name' => 'admin_kayser_platform_jpentreprise_list',));
        }

        // admin_kayser_platform_jpentreprise_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpentreprise/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpentreprise_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jpentreprise',  '_sonata_name' => 'admin_kayser_platform_jpentreprise_create',));
        }

        // admin_kayser_platform_jpentreprise_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpentreprise/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpentreprise_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jpentreprise',  '_sonata_name' => 'admin_kayser_platform_jpentreprise_batch',));
        }

        // admin_kayser_platform_jpentreprise_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpentreprise/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpentreprise_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jpentreprise',  '_sonata_name' => 'admin_kayser_platform_jpentreprise_edit',));
        }

        // admin_kayser_platform_jpentreprise_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpentreprise/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpentreprise_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jpentreprise',  '_sonata_name' => 'admin_kayser_platform_jpentreprise_delete',));
        }

        // admin_kayser_platform_jpentreprise_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpentreprise/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpentreprise_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jpentreprise',  '_sonata_name' => 'admin_kayser_platform_jpentreprise_show',));
        }

        // admin_kayser_platform_jpentreprise_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpentreprise/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpentreprise_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jpentreprise',  '_sonata_name' => 'admin_kayser_platform_jpentreprise_export',));
        }

        // admin_kayser_platform_jpinformations_list
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpinformations/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpinformations_list')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'sonata.admin.jpinformations',  '_sonata_name' => 'admin_kayser_platform_jpinformations_list',));
        }

        // admin_kayser_platform_jpinformations_create
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpinformations/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpinformations_create')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'sonata.admin.jpinformations',  '_sonata_name' => 'admin_kayser_platform_jpinformations_create',));
        }

        // admin_kayser_platform_jpinformations_batch
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpinformations/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpinformations_batch')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'sonata.admin.jpinformations',  '_sonata_name' => 'admin_kayser_platform_jpinformations_batch',));
        }

        // admin_kayser_platform_jpinformations_edit
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpinformations/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpinformations_edit')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'sonata.admin.jpinformations',  '_sonata_name' => 'admin_kayser_platform_jpinformations_edit',));
        }

        // admin_kayser_platform_jpinformations_delete
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpinformations/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpinformations_delete')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'sonata.admin.jpinformations',  '_sonata_name' => 'admin_kayser_platform_jpinformations_delete',));
        }

        // admin_kayser_platform_jpinformations_show
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpinformations/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpinformations_show')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'sonata.admin.jpinformations',  '_sonata_name' => 'admin_kayser_platform_jpinformations_show',));
        }

        // admin_kayser_platform_jpinformations_export
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/admin/kayser/platform/jpinformations/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_kayser_platform_jpinformations_export')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'sonata.admin.jpinformations',  '_sonata_name' => 'admin_kayser_platform_jpinformations_export',));
        }

        // login
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/login$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'login')), array (  '_controller' => 'Kayser\\PlatformBundle\\Controller\\DefaultController::loginAction',));
        }

        // login_check
        if (preg_match('#^/(?P<_locale>fr|en|es|jp)/login_check$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'login_check')), array ());
        }

        if (0 === strpos($pathinfo, '/media/cache/resolve')) {
            // liip_imagine_filter_runtime
            if (preg_match('#^/media/cache/resolve/(?P<filter>[A-z0-9_\\-]*)/rc/(?P<hash>[^/]++)/(?P<path>.+)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_liip_imagine_filter_runtime;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'liip_imagine_filter_runtime')), array (  '_controller' => 'liip_imagine.controller:filterRuntimeAction',));
            }
            not_liip_imagine_filter_runtime:

            // liip_imagine_filter
            if (preg_match('#^/media/cache/resolve/(?P<filter>[A-z0-9_\\-]*)/(?P<path>.+)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_liip_imagine_filter;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'liip_imagine_filter')), array (  '_controller' => 'liip_imagine.controller:filterAction',));
            }
            not_liip_imagine_filter:

        }

        if (0 === strpos($pathinfo, '/trad')) {
            if (0 === strpos($pathinfo, '/trad/translations')) {
                // lexik_translation_list
                if ($pathinfo === '/trad/translations') {
                    if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'HEAD'));
                        goto not_lexik_translation_list;
                    }

                    return array (  '_controller' => 'Lexik\\Bundle\\TranslationBundle\\Controller\\RestController::listAction',  '_route' => 'lexik_translation_list',);
                }
                not_lexik_translation_list:

                // lexik_translation_update
                if (preg_match('#^/trad/translations/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'PUT') {
                        $allow[] = 'PUT';
                        goto not_lexik_translation_update;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'lexik_translation_update')), array (  '_controller' => 'Lexik\\Bundle\\TranslationBundle\\Controller\\RestController::updateAction',));
                }
                not_lexik_translation_update:

            }

            // lexik_translation_grid
            if ($pathinfo === '/trad/grid') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_lexik_translation_grid;
                }

                return array (  '_controller' => 'Lexik\\Bundle\\TranslationBundle\\Controller\\TranslationController::gridAction',  '_route' => 'lexik_translation_grid',);
            }
            not_lexik_translation_grid:

            // lexik_translation_new
            if ($pathinfo === '/trad/new') {
                return array (  '_controller' => 'Lexik\\Bundle\\TranslationBundle\\Controller\\TranslationController::newAction',  '_route' => 'lexik_translation_new',);
            }

            // lexik_translation_invalidate_cache
            if ($pathinfo === '/trad/invalidate-cache') {
                return array (  '_controller' => 'Lexik\\Bundle\\TranslationBundle\\Controller\\TranslationController::invalidateCacheAction',  '_route' => 'lexik_translation_invalidate_cache',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
