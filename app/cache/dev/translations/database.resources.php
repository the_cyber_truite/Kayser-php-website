<?php return array (
  0 => 
  array (
    'locale' => 'en',
    'domain' => 'LexikTranslationBundle',
  ),
  1 => 
  array (
    'locale' => 'en',
    'domain' => 'Menu',
  ),
  2 => 
  array (
    'locale' => 'en',
    'domain' => 'PiedDePage',
  ),
  3 => 
  array (
    'locale' => 'en',
    'domain' => 'security',
  ),
  4 => 
  array (
    'locale' => 'en',
    'domain' => 'SonataAdminBundle',
  ),
  5 => 
  array (
    'locale' => 'en',
    'domain' => 'SonataCoreBundle',
  ),
  6 => 
  array (
    'locale' => 'en',
    'domain' => 'validators',
  ),
  7 => 
  array (
    'locale' => 'fr',
    'domain' => 'LexikTranslationBundle',
  ),
  8 => 
  array (
    'locale' => 'fr',
    'domain' => 'Menu',
  ),
  9 => 
  array (
    'locale' => 'fr',
    'domain' => 'PiedDePage',
  ),
  10 => 
  array (
    'locale' => 'fr',
    'domain' => 'security',
  ),
  11 => 
  array (
    'locale' => 'fr',
    'domain' => 'SonataAdminBundle',
  ),
  12 => 
  array (
    'locale' => 'fr',
    'domain' => 'SonataCoreBundle',
  ),
  13 => 
  array (
    'locale' => 'fr',
    'domain' => 'validators',
  ),
);