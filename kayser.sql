-- phpMyAdmin SQL Dump
-- version 4.1.9
-- http://www.phpmyadmin.net
--
-- Host: maisonkamysql.mysql.db
-- Generation Time: Mar 18, 2015 at 11:50 AM
-- Server version: 5.1.73-2+squeeze+build1+1-log
-- PHP Version: 5.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `maisonkamysql`
--

-- --------------------------------------------------------

--
-- Table structure for table `Contact`
--

CREATE TABLE IF NOT EXISTS `Contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sujet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `genre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kay_annonces`
--

CREATE TABLE IF NOT EXISTS `kay_annonces` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creationdate` datetime NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `overview` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `kay_annonces`
--

INSERT INTO `kay_annonces` (`id`, `slug`, `creationdate`, `title`, `overview`, `content`, `order`) VALUES
(1, 'apprentis-boulangers', '2015-02-01 00:00:00', 'Apprentis boulangers H/F', 'Dans le cadre d’un contrat d’apprentissage et sous la responsabilité du\r\nChef Boulanger de la boutique, vous serez formé(e) au Métier de Boulanger\r\nen alternance.', '<b>1. Production</b>\n<br/>\n	• Confectionner les pains et les viennoiseries en mettant en œuvre lestechniques de fabrication de la Maison :<br/>\n	– Préparer, malaxer, pétrir les pâtes<br/>\n	– Façonner les pâtes<br/>\n	– Mettre en œuvre et surveiller le processus de fabrication<br/>\n	– Cuire <br/>\n	\n\n	• Gérer la production<br/>\n	• Connaître les produits de l’enseigne\n<br/>\n<br/>\n<b>2. Hygiène – sécurité</b>\n<br/>\n\n	• Respecter et garantir les règles d’hygiène et de sécurité,<br/>\n	• Nettoyer et entretenir les équipements et les locaux,<br/>\n	• Assurer la maintenance courante du matériel et équipements automatisés ou mécanisés.<br/>\n<br/><br/>\n<b>Profil</b><br/>\nVous êtes de nature dynamique, rigoureuse et motivée et vous avez envie de rejoindre un groupe renommé et en pleine croissance.<br/>\nEnvoyez votre candidature CV + lettre de motivation à<br/> <a hreh="mailto:recrutement@maison-kayser.com">recrutement@maison-kayser.com</a> à l’attention de Mlle Boguslawski Julie.', 3),
(2, 'responsables-boutiques', '2015-02-02 00:00:00', 'Responsables de boutique H/F', 'Représentant de notre savoir-faire et de notre savoir-être vous dirigez une\r\néquipe de vente et de boulangers selon les critères de notre Maison.', 'En relation directe avec le Directeur d’exploitation, vos missions seront :\n<b>1. Communication</b>\n<br/>\n	– Garantir l’image, la qualité des produits et de l’accueil<br/><br/>\n	– Assurer la transmission des informations<br/>\n	– Promouvoir les produits de l’enseigne et les nouveautés<br/>\n<br/>\n<br/>\n<b>2. Formation et Management</b>\n<br/>\n	– Participer à la formation de votre équipe<br/>\n	– Encadrer l’équipe de vente et de production (fournil sur place)<br/>\n	– Motiver et challenger votre équipe.<br/>\n	– Accompagner et développer les compétences de vos collaborateurs pour favoriser leur évolution.<br/>\n	– Piloter les indicateurs du quotidien en les mettant à profit sur le Terrain.<br/>\n<br/>\n<br/>\n<b>3. Hygiène – sécurité - maintenance</b>\n<br/>\n	– Respecter et garantir les règles d’hygiène et de sécurité<br/>\n	– Sensibiliser votre équipe aux règles élémentaires d’hygiène<br/>\n	– Garantir le bon entretien des locaux, du matériel et des outils de travail<br/>\n<br/>\n<br/>\n<b>4. Mission contrôle qualité</b>\n<br/>\n	– S’assurer de la qualité de l’ensemble des produits.<br/>\n	– Veiller au respect des règles et standards de la Maison Kayser<br/>\n	– Veiller au respect des bons procédés de fabrication<br/>\n<br/>\n<br/>\n\n<b>Profil</b><br/>\nPlus que votre formation, c’est votre motivation, votre passion et votre expérience (3 ans et plus) dans la gestion d’un centre de profit qui feront\nla différence.\nRejoignez un groupe dynamique et en pleine croissance pour vous investir dans une mission passionnante. Intérêt pour la grande consommation et les\ncultures culinaires.\nVous aimez être sur le terrain pour gérer les problématiques du quotidien et vous faîtes preuve d’anticipation. Dynamique avec un sens commercial affirmé\net doté d’un fort leadership, vous bénéficiez d’horaires variables (ouverture/fermeture) sans coupure avec deux jours de repos par semaine.\nEnvoyez votre candidature (CV + lettre de motivation) à <br> <a href="mailto:recrutement@maison-kayser.com">recrutement@maison-kayser.com</a> à l’attention de Mlle Boguslawski Julie.\n', 2),
(3, 'vendeurs', '2015-02-04 00:00:00', 'Vendeurs H/F', 'Vous êtes de nature dynamique, souriante, rigoureuse avec un sens\r\ndu relationnel très développé, alors venez rejoindre un groupe en pleine\r\ncroissance pour vous investir dans une mission passionnant!', 'En relation directe avec le Responsable de Magasin et son Adjoint, vos missions seront :<br/>\n<b>1. Vente</b>\n<br/>\n	– Accueillir la clientèle selon les standards de la Maison<br/>\n	– Mettre en œuvre les techniques de vente de la Maison<br/>\n	– Répondre aux besoins de la clientèle<br/>\n	– Présenter les produits et conseiller la clientèle<br/>\n	– Développer les ventes en étant force de proposition<br/>\n	– Gérer les opérations d’encaissement<br/>\n<br/>\n<br/>\n<b>2. Exploitation</b>\n<br/>\n	– Garantir l’image, la qualité des produits et de l’accueil<br/>\n	– Connaître et mettre en valeur les produits de la Maison<br/>\n	– Réassortir les produits en boutique,<br/>\n	– Réaliser le nettoyage de la boutique<br/>\n<br/>\n<br/>\n<b>3. Hygiène & sécurité</b>\n<br/>\n	– Respecter et garantir les règles d’hygiène et de sécurité<br/>\n<br/>\n<br/>\n\n<b>Profil</b><br/>\nCAP/BEP vente ou CAP métiers de bouche, Bac Professionnel avec une première expérience dans le métier.\nVous êtes de nature dynamique, souriante, rigoureuse avec un sens du relationnel très développé alors venez rejoindre un groupe en pleine\ncroissance pour vous investir dans une mission passionnante. Bonne présentation et bonne élocution indispensables.\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kay_block`
--

CREATE TABLE IF NOT EXISTS `kay_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_E56FE4173DA5256D` (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `kay_block`
--

INSERT INTO `kay_block` (`id`, `image_id`, `title`, `body`, `link`) VALUES
(1, 1, 'La maison Kayser', '<p>A Paris, Lyon, Menton, Beausoleil ou encore Tokyo, S&eacute;oul, New-York ou Mexico, retrouvez-nos boulangeries&nbsp;!</p>', '/nous-trouver'),
(2, 2, 'Notre Savoir-faire', '<p>Si la Maison Kayser s&rsquo;est affirm&eacute;e au fil du temps comme une r&eacute;f&eacute;rence en terme de qualit&eacute; de produit, aussi bien pour ses pains que ses viennoiseries ou p&acirc;tisseries, c&rsquo;est le fruit d&rsquo;un savoir-faire toujours renouvel&eacute; et compl&eacute;t&eacute;.</p>', '/savoir-faire'),
(3, 3, 'Nos produits', '<p>Pains au levain, ou sans gluten, viennoiseries au Beurre de Charentes, g&acirc;teaux de voyage, tartes, petits g&acirc;teaux et sandwichs originaux, d&eacute;couvrez nos produits !</p>', '/produits'),
(4, 4, 'Boulangerie en ligne', '<p>En construction,&nbsp;&agrave; d&eacute;couvrir prochainement.</p>', '/'),
(5, 5, 'Notre histoire', '<p>Si l&#39;histoire de la Maison Kayser a commenc&eacute; &agrave; s&#39;&eacute;crire au 8 rue Monge un certain vendredi 13 septembre 1996, ce n&rsquo;est en d&eacute;finitive que la suite logique d&#39;un parcours entam&eacute; par un homme depuis son plus jeune &acirc;ge.</p>', '/histoire'),
(6, 6, 'Philosophie', '<p>Le bon pain ne saurait &ecirc;tre r&eacute;alis&eacute; sans une certaine philosophie, des valeurs et des fondements sur lesquels s&rsquo;appuient nos boulangers chaque jour. D&egrave;s le d&eacute;but, Eric Kayser a b&acirc;ti son entreprise sur des bases solides et singuli&egrave;res.</p>', '/philosophie'),
(7, 7, 'Nos méthodes', '<p>Ce qui a distingu&eacute; Eric Kayser d&egrave;s le d&eacute;but, et encore aujourd&rsquo;hui, c&rsquo;est sa volont&eacute; d&rsquo;utiliser le levain naturel pour faire pousser ses pains et viennoiseries.</p>', '/methodes'),
(8, 8, 'Nos équipes', '<p>Eric Kayser a su s&rsquo;entourer d&egrave;s le d&eacute;but de femmes et d&rsquo;hommes passionn&eacute;s, partageant la m&ecirc;me vision que lui du bon pain et du travail artisanal. Si aujourd&rsquo;hui la Maison Kayser est en mesure d&rsquo;offrir des produits de qualit&eacute; &agrave; travers le monde, c&rsquo;est gr&acirc;ce &agrave; l&rsquo;implication de nos collaborateurs, aussi bien au sein de nos fournils qu&rsquo;en boutique.</p>', '/equipes'),
(9, 9, 'Pains au levain & sans gluten', '<p>Tous fabriqu&eacute;s &agrave; base de levain naturel dans chacune de nos boutiques, ce sont 15 &agrave; 20 pains diff&eacute;rents qui sont propos&eacute;s &agrave; tous vos instants gourmands. Osez la vari&eacute;t&eacute; et laissez-vous surprendre !</p>', '/produits/pains'),
(10, 10, 'Viennoiseries & brioches', '<p>Elles sont &eacute;galement r&eacute;alis&eacute;es &agrave; partir de levain naturel, pour un croustillant et une conservation exceptionnels. Un g&eacute;n&eacute;reux beurre extrafin de Touraine vient compl&eacute;ter les recettes pour votre plus grand plaisir !</p>', '/produits/viennoiseries'),
(11, 11, 'Pâtisseries', '<p>Retrouvez chaque jour nos p&acirc;tisseries gourmandes. En plus des grands classiques disponibles toute l&#39;ann&eacute;e, les p&acirc;tissiers de la Maison inventent pour vous, &agrave; chaque saison, d&#39;authentiques collections de p&acirc;tisseries &quot;haute couture&quot;. Alliances surprenantes de saveurs et &eacute;l&eacute;gance des ar&ocirc;mes surprendront les plus exigeants d&#39;entre vous.</p>', '/produits/patisseries/carte'),
(12, 12, 'Les salés', '<p>Pour un repas complet mariant fraicheur et gourmandise, d&eacute;gustez nos sandwichs (chauds ou froids) &agrave; base de pains fabriqu&eacute; dans nos fournils. D&eacute;couvrez &eacute;galement nos quiches, soupes et salades &eacute;labor&eacute;s au gr&eacute; des l&eacute;gumes de saison. Des produits sains et naturels pour combler tous les app&eacute;tits.</p>', '/produits/sales/carte');

-- --------------------------------------------------------

--
-- Table structure for table `kay_blockimage`
--

CREATE TABLE IF NOT EXISTS `kay_blockimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fileExtension` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `kay_blockimage`
--

INSERT INTO `kay_blockimage` (`id`, `name`, `url`, `fileExtension`) VALUES
(1, '8c07ca37300943c826a402289b52d6b5.jpeg', 'img/blocks/8c07ca37300943c826a402289b52d6b5.jpeg', 'jpeg'),
(2, 'ae6161552ea390703175714ce8f77f4b.jpeg', 'img/blocks/ae6161552ea390703175714ce8f77f4b.jpeg', 'jpeg'),
(3, 'e46269f1fbdf8b65d9a92110df5c235e.jpeg', 'img/blocks/e46269f1fbdf8b65d9a92110df5c235e.jpeg', 'jpeg'),
(4, '73284e2c2723e76aa7425056f5745b8d.jpeg', 'img/blocks/73284e2c2723e76aa7425056f5745b8d.jpeg', 'jpeg'),
(5, '50b6d85d671aaa076dd11beeb221727f.jpeg', 'img/blocks/50b6d85d671aaa076dd11beeb221727f.jpeg', 'jpeg'),
(6, '9237a1797ef1ea1213bd4971484d740b.jpeg', 'img/blocks/9237a1797ef1ea1213bd4971484d740b.jpeg', 'jpeg'),
(7, '55f88ade55fb53411c6d3d3409f44fa1.jpeg', 'img/blocks/55f88ade55fb53411c6d3d3409f44fa1.jpeg', 'jpeg'),
(8, 'd3a76d560b1ef70ef4c3467f728b2d71.jpeg', 'img/blocks/d3a76d560b1ef70ef4c3467f728b2d71.jpeg', 'jpeg'),
(9, '2cbfc9d1775348b0128b719d2ef8a2df.jpeg', 'img/blocks/2cbfc9d1775348b0128b719d2ef8a2df.jpeg', 'jpeg'),
(10, '8f16dd6b1073dea12b980aeadd86c578.jpeg', 'img/blocks/8f16dd6b1073dea12b980aeadd86c578.jpeg', 'jpeg'),
(11, '018e6ca89a16ac870ca65db914a03fac.jpeg', 'img/blocks/018e6ca89a16ac870ca65db914a03fac.jpeg', 'jpeg'),
(12, 'd6a07d7ba9cd1be5659c436eba53c094.jpeg', 'img/blocks/d6a07d7ba9cd1be5659c436eba53c094.jpeg', 'jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `kay_locations`
--

CREATE TABLE IF NOT EXISTS `kay_locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `street` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zip_code` int(11) NOT NULL,
  `state` varchar(50) NOT NULL,
  `country` varchar(255) NOT NULL,
  `opening` varchar(50) NOT NULL,
  `day_off` varchar(50) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `coordsy` varchar(20) NOT NULL,
  `coordsx` varchar(20) NOT NULL,
  `sans_gluten` varchar(1) NOT NULL,
  `restaurant` varchar(1) NOT NULL,
  `cafe` varchar(1) NOT NULL,
  `upcomming` varchar(1) NOT NULL,
  `location` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=148 ;

--
-- Dumping data for table `kay_locations`
--

INSERT INTO `kay_locations` (`id`, `name`, `street`, `city`, `zip_code`, `state`, `country`, `opening`, `day_off`, `phone_number`, `email`, `coordsy`, `coordsx`, `sans_gluten`, `restaurant`, `cafe`, `upcomming`, `location`) VALUES
(1, 'Aeon mall-big', 'Aeon Mall, shop 075, ground floor  #132, Samdech Sothearos Blvd (3)', 'Phnom Penh', 12301, '', 'Cambodge', '<i>7:00am-12:00am</i>', '', '', '', '11.5448729', '104.89216680000004', '', 'Y', '', '', 'Cambodia'),
(2, 'Aeon mall-small', 'Aeon Mall, shop 069, ground floor  #132, Samdech Sothearos Blvd (3)', 'Phnom Penh', 12301, '', 'Cambodge', '<i>9:00am-10:00pm</i>', '', '077 573 097 ', '', '11.552957', '104.932378', '', '', 'Y', '', 'Cambodia'),
(3, 'COMING SOON', '#29, Street 63, Sangkat Boeung Keng Kang 1', 'Phnom Penh', 0, '', 'Cambodge', '', '', '', '', '11.5448729', '104.89216680000004', '', '', 'Y', 'Y', 'Cambodia'),
(4, 'White Mansion', '#26, street 240 (corner street 51)', 'Phnom Penh', 12207, '', 'Cambodge', '', '', '', '', '11.5422495', '104.93111090000002', '', '', '', '', 'Cambodia'),
(5, 'Augusto Leguia', 'Augusto Leguia Norte n°34 - Las Condes', 'Santiago', 0, '', 'Chile', '', '', '', '', '-33.4149698\n', '-70.59794779999999', '', '', 'Y', '', 'Chile'),
(6, 'Mall VIVO Los Trapenses', 'José Alcalde Delano, Lo Barnechea', 'Santiago', 0, '', 'Chile', '', '', '', '', '-33.3569086\n', '-70.53566660000001\n', '', '', 'Y', '', 'Chile'),
(7, 'Bogota', 'calle 81 # 9-85, barrio el Nogal , Bogota', 'Bogota', 0, '', 'Colombia', '<i>7AM - 20PM</i>\n', '', ' +(571) 3000 743', ' erickayserbogota@yahoo.com', '4.664515199999999\n', '-74.05258679999997\n', '', 'Y', '', 'Y', 'Colombia'),
(8, 'COMING SOON', '', 'Cairo', 0, '', 'Egypt', '', '', '', '', '43.94700419999999', '4.820959600000037', '', '', '', 'Y', 'Egypt'),
(9, 'AVIGNON TGV', 'Gare avignon TGV - Place de l''Europe', 'Avignon', 84000, '', 'France', '<i>Ouvert de 5h30 à 21h</i>', 'x', '04 32 76 34 74', '342733@relay.fr', '43.9455279', '4.7927119', '', 'Y', '', '', 'Avignon'),
(10, 'BEAUSOLEIL', '2 rue du Marché, 2  Avenue du Général de Gaulle', 'Beausoleil', 6240, '', 'France', '<i>Ouvert de 7h à 20h</i>', 'x', '04 93 78 46 68', 'beausoleil@maison-kayser.com', '43.743171', '7.426555', '', '', 'Y', '', 'Beausoleil'),
(11, 'LEVALLOIS LIBERATION', '5 Place de la libération 92100 Levallois-Perret', 'Levallois-Perret', 92100, '', 'France', '<i>Ouvert de 7h à 20h</i>', 'Dimanche', '01 47 58 5049', 'levallois.liberation@maison-kayser.com', '48.89321700000001', '2.287864000000013', '', '', '', '', 'Levallois-Perret'),
(12, 'LEVALLOIS TREBOIS', '19 rue trébois 92100 Levallois-Perret', 'Levallois-Perret', 92100, '', 'France', '<i>Ouvert de 7h à 20h</i>', 'Dimanche après-midi - et lundi', '01 40 87 15 16', 'levallois@maison-kayser.com', '50.63752849999999', '3.0709229999999934', '', '', '', '', 'Levallois-Perret'),
(13, 'LILLES FLANDRES - COMING APRIL 2015', 'Gare Lille Flandres', 'Lille', 59800, '', 'France', '', '', '', '', '45.7595704', '4.827440199999955', '', '', 'Y', '', 'Lille'),
(14, 'Lyon L''Epiaison', '1 avenue Adolphe Max', 'Lyon', 69005, '', 'France', '<i>Ouvert de 6h à 20h</i>', 'dimanche', '04 78 37 46 36', 'fournildelopera@gmail.com', '45.7684559', '4.836309099999994', '', '', '', '', 'Lyon'),
(15, 'Lyon Opéra', '15 Place Louis Pradel', 'Lyon', 69, '', 'France', '<i>Ouvert de 6h à 20h</i>', 'dimanche', '04 78 29 96 99', 'fournildelopera@gmail.com', '45.7695794', '4.855288299999984', '', '', '', '', 'Lyon'),
(16, 'Lyon Vitton', '27  Rue Ney', 'Lyon', 69006, '', 'France', '<i>Ouvert de 7h à 20h</i>', 'dimanche', '04 72 37 00 32', 'fournildelopera@gmail.com', '43.7747976', '7.496832400000017', '', '', '', '', 'Lyon'),
(17, 'MENTON BIOVES', '1 Rue Partouneaux', 'Menton', 6500, '', 'France', '<i>Ouvert de 7h à 19h</i>', 'Dimanche à partir de 12h30', '04 93 28 25 81', 'ekmenton@gmail.com', '43.774481', '7.497540000000072', '', '', 'Y', '', 'Menton'),
(18, 'MENTON marché couvert ', 'Quai de Mont Léon - Halles Municipales - Marché couvert', 'Menton', 6500, '', 'France', '<i>Ouvert de 7h15 à 13h15</i>', 'Lundi', '04 93 28 00 11', 'ekmenton@gmail.com', '43.8071111', '7.4853709999999865', '', '', '', '', 'Menton'),
(19, 'MENTON MONTI', 'Hameau de Monti, Route de Sospel,', 'Menton', 6500, '', 'France', '<i>Ouvert de 6h30 à 19h</i>', 'x', '04 93 96 59 77', 'ekmenton@gmail.com', '43.797509', '7.491036', '', '', 'Y', '', 'Menton'),
(20, 'ORLY AEROPORT', 'Aerogare Orly ouest - Hall 4 (zône réservée)', 'Orly aérogare', 94546, '', 'France', '<i>Ouvert de 5h30 à 21h30</i>', 'x', '01 74 22 12 65', '362004@relay.fr', '48.7283271', '2.360132', '', '', '', '', 'Orly'),
(21, '14 RUE MONGE', '14 Rue Monge', 'Paris', 75005, '', 'France', '<i>Ouvert de 8h à 20h15</i>', 'Lundi', '01 44 07 17 81', '14monge@maison-kayser.com', '48.849118', '2.349906', 'Y', '', 'Y', '', 'Paris'),
(22, '8 RUE MONGE', '8 Rue Monge', 'Paris', 75005, '', 'France', '<i>Ouvert de 6h30 à 20h30</i>', 'mardi', '01 44 07 01 42', '8monge@maison-kayser.com', '48.849347', '2.349434', '', '', '', '', 'Paris'),
(23, 'ASSAS', '87 rue d''assas', 'Paris', 75006, '', 'France', '<i>Ouvert de 7h à 20h</i>', 'Dimanche', '01 43 54 92 31', 'assas@maison-kayser.com', '48.842377', '2.335391', '', '', '', '', 'Paris'),
(24, 'BAC', '18 rue du Bac', 'Paris', 75007, '', 'France', '<i>Ouvert de 7h à 20h30</i>', 'Lundi', '01 42 61 27 63', 'bac@maison-kayser.com', '48.858458', '2.328318', '', '', 'Y', '', 'Paris'),
(25, 'BEAUGRENELLE', '12 Rue Linois', 'Paris', 75015, '', 'France', '<i>Ouvert de 10h à 21h <br>jeudi: 10h-20h</i>', 'Dimanche', '01 45 71 06 36 ', 'beaugrenelle@maison-kayser.com', '48.848777', '2.282264', '', '', 'Y', '', 'Paris'),
(26, 'BERCY VILLAGE', '41 cour saint emilion', 'Paris', 75012, '', 'France', '<i>Ouvert de 7h à 23h<br>dimanche: 7h à 21h</i>', 'x', '01 43 46 08 89', 'bercy@maison-kayser.com', '48.832951', '2.386095', 'Y', 'Y', '', '', 'Paris'),
(27, 'BOURSE', '5 place de la Bourse', 'Paris', 75002, '', 'France', '<i>Ouvert de 7h à 20h</i>', 'Dimanche', '01 42 44 10 30', 'bourse@maison-kayser.com', '48.868891', '2.341339', '', '', 'Y', '', 'Paris'),
(28, 'COMMERCE', '79 rue du commerce', 'Paris', 75015, '', 'France', '<i>Ouvert de 7h à 20h30</i>', 'Dimanche', '01 44 19 88 54', '79commerce@maison-kayser.com', '48.844978', '2.294279', '', '', '', '', 'Paris'),
(29, 'DUROC', '1 Bd du Montparnasse', 'Paris', 75006, '', 'France', '<i>Ouvert de 7h à 20h30</i>', 'Dimanche', '01 47 83 75 39', 'duroc@maison-kayser.com', '48.846876', '2.317002', '', '', 'Y', '', 'Paris'),
(30, 'ETOILE', '19 avenue des Ternes', 'Paris', 75017, '', 'France', '<i>Ouvert de 7h à 20h15</i>', 'Dimanche', '01 43 80 23 28', 'ternes@maison-kayser.com', '48.878077', '2.295787', '', '', '', '', 'Paris'),
(31, 'LOUVRE - PALAIS ROYAL', '4 rue de l''échelle', 'Paris', 75001, '', 'France', '<i>Ouvert de 7h à 20h30</i>', 'x', '01 40 15 01 31', 'echelle@maison-kayser.com', '48.864261', '2.334365', 'Y', '', 'Y', '', 'Paris'),
(32, 'MALESHERBES', '85 Bd Malesherbes', 'Paris', 75008, '', 'France', '<i>Ouvert de 7h à 20h15</i>', 'Dimanche', '01 45 22 70 30', 'malesherbes@maison-kayser.com', '48.877794', '2.315987', '', 'Y', '', '', 'Paris'),
(33, 'MONTPARNASSE', '27 rue du départ ', 'Paris', 75014, '', 'France', '<i>Ouvert de 6h15 à 20h15</i>', 'Dimanche', '01 43 270841', 'montparnasse@maison-kayser.com', '48.841781', '2.322477', '', '', 'Y', '', 'Paris'),
(34, 'PHILHARMONIE - L''Atelier d''Eric Kayser', '221 Avenue Jean Jaurès', 'Paris', 75019, '', 'France', '', '', '', '', '48.889371', '2.393686', '', '', '', '', 'Paris'),
(35, 'MOZART', '79 avenue Mozart', 'Paris', 75016, '', 'France', '<i>Ouvert de 7h à 20h</i>', 'Dimanche', '01 42 88 03 29', 'mozart@maison-kayser.com', '48.853171', '2.268793', '', '', 'Y', '', 'Paris'),
(36, 'ODEON', '10 rue de l''ancienne comédie', 'Paris', 75006, '', 'France', '<i>Ouvert de 7h à 20h30</i>', 'Dimanche', '01 43 25 71 60', 'odeon@maison-kayser.com', '48.853465', '2.338360', '', '', 'Y', '', 'Paris'),
(37, 'PETITS CARREAUX', '16 rue des Petits carreaux', 'Paris', 75002, '', 'France', '<i>Ouvert de 7h à 20h</i>', 'Dimanche', '01 42 33 76 48', 'petitscarreaux@maison-kayser.com', '48.867204', '2.347395', '', '', '', '', 'Paris'),
(38, 'PORT TOLBIAC', '77 quai panhard et levassor', 'Paris', 75013, '', 'France', '<i>Ouvert de 7h à 20h</i>', 'Dimanche', '01 56 61 11 06', 'bibliotheque@maison-kayser.com', '48.831342', '2.381148', '', '', 'Y', '', 'Paris'),
(39, 'VENDOME', '33 rue Daniel Casanova', 'Paris', 75001, '', 'France', '<i>Ouvert de 7h à 20h30</i>', 'Dimanche', '01 42 97 59 29', 'vendome@maison-kayser.com', '48.868366', '2.330801', '', '', 'Y', '', 'Paris'),
(40, 'SAINT GERMAIN EN LAYE', '8 Rue de Pologne', 'St Germain en Laye', 78100, '', 'France', '<i>Ouvert de 7h30 à 20h</i>', 'Dimanche après-midi - et lundi', '01 34 51 03 06', 'stgermain@maison-kayser.com', '48.897232', '2.090410', '', '', 'Y', '', 'Paris'),
(41, '248 QRE', 'G/F, 248 Queen''s Road East', 'Wan Chai', 0, '', 'Hong-Kong', '<i>7:30-8:30</i>', '', '852 31 07 13 80', 'contact@maison-kayser.com.hk', '22.274719\n', '22.274719\n', '', '', 'Y', '', 'hongkong'),
(42, 'Caine Road', '38 Caine Road', 'Mid-levels', 0, '', 'Hong-Kong', '<i>7:30-9:30</i>', '', '852 2178 2566', 'contact@maison-kayser.com.hk', '22.2807585\n', '114.17358999999999\n', '', '', 'Y', '', 'hongkong'),
(43, 'Happy Valley', '60 Ventris road', 'Happy Valley', 0, '', 'Hong-Kong', '<i>7:30-9:45</i>', '', '852 3107 0008', 'contact@maison-kayser.com.hk', '22.270285\n', '114.18531000000007\n', '', 'Y', '', '', 'hongkong'),
(44, 'Harbour City', 'Harbour City, Ocean Terminal, Ground Floor, Shop 14-15', 'Kowloon', 0, '', 'Hong-Kong', '<i>9:00-10:00</i>', '', ' 852 2736 2884', 'contact@maison-kayser.com.hk', '22.297952', '114.168228', '', 'Y', '', '', 'hongkong'),
(45, 'French Embassy', 'IFI Building Ground Floor\nJl. Mh Thamrin no 20', 'Jakarta', 10350, '', 'Indonesia', '<i>7:30-17:30</i>', 'Sundays', '', 'ek3@erickayser.co.id', '-6.223610799999999', '106.79897679999999', '', '', 'Y', '', 'Indonesia'),
(46, 'Plaza Senayan', 'Plaza Senayan Level 3, unit 317 A, Jl. Asia Afrika No.8', 'Jakarta', 10270, '', 'Indonesia', '<i>10:00-22:00</i>', '', '2157905365', 'ek2@erickayser.co.id', '-6.223610799999999\n', '106.79897679999999\n', '', 'Y', '', '', 'Indonesia'),
(47, 'Abeno Harukas', '1F Abeno Harukas Tower Bldg., 1-1-43 Abenosuji, Abeno-ku, Osaka-shi', 'Osaka', 545, '', 'Japan', '<i>10:00-21:00</i>', 'Depends on Abeno Harukas', '81-6-6654-6190', '', '34.6458932\n', '135.5138905\n', '', 'Y', '', '', 'Japan'),
(48, 'Coredo Nihonbashi', 'B1F COREDO Nihonbashi, 1-4-1 Nihonbashi, Chuo-ku', 'Tokyo', 103, '', 'Japan', '<i>7:00-20:00</i>', 'Depends on CORED Nihonbashi', '81-3-3516-0030', '', '35.6807271\n', '139.77375540000003\n', '', 'Y', '', '', 'Japan'),
(49, 'Denenchofu', '1F Tokyu Square Garden Site Annex, 2-62-3 Denenchofu, Ota-ku', 'Tokyo', 145, '', 'Japan', '<i>7:30-21:00</i>', 'unfixed', '81-3-3722-5093', '', '35.5895134\n', '139.67353130000004\n', '', 'Y', '', '', 'Japan'),
(50, 'Fukuoka Daïmaru ', 'B2F Daimaru Fukuoka Tenjin, 1-4-1 Tenjin, Chuo-ku, Fukuoka-shi', 'Fukuoka', 810, '', 'Japan', '<i>10:00-20:00</i>', 'Depends on Daimaru', '81-92-718-3058', '', '33.5886493\n', '130.40117940000005\n', '', '', '', '', 'Japan'),
(51, 'Gotanda', '1F Park Tower Grand Sky, 2-10-1 Higashi-Gotanda, Shinagawa-ku', 'Tokyo', 141, '', 'Japan', '<i>8:00-20:00</i>', 'New Year''s day,2,3 January', '81-3-6450-3705', '', '35.6272776\n', '139.72560699999997\n', '', '', '', '', 'Japan'),
(52, 'Hiroo Plaza', '1F Hiroo Plaza, 5-6-6 Hiroo, Shibuya-ku', 'Tokyo', 150, '', 'Japan', '<i>10:00-21:00</i>', 'Depends on Hiroo Plaza', '81-3-6721-9011', '', '35.65019590000001\n', '139.72102719999998\n', '', '', '', '', 'Japan'),
(53, 'Ikebukuro', '1F Sunshine City Alpa, 3-1-2 Higashi-Ikebukuro, Toshima-ku ', 'Tokyo', 170, '', 'Japan', '<i>8:00-21:00</i>', 'Twice a year (not fixed)', '81-3-3980-8880', '', '35.730789\n', '139.72049919999995\n', '', 'Y', '', '', 'Japan'),
(54, 'Jiyugaoka', '1F Fullel With Jiyugaoka, 1-6-9 Jiyugaoka, Meguro-ku', 'Tokyo', 152, '', 'Japan', '<i>10:00-22:00</i>', 'unfixed', '81-3-6421-4850', '', '35.60831599999999\n', '139.66812490000007\n', '', '', '', '', 'Japan'),
(55, 'Kagurazaka', '1F Osaki Bldg., 41 Tansu-cho, Shinjuku-ku', 'Tokyo', 162, '', 'Japan', '<i>8:00-20:00</i>', 'New Year''s day, 2nd January', '81-3-6457-5678', '', '35.7007152\n', '139.73486619999994\n', '', '', 'Y', '', 'Japan'),
(56, 'Kawasaki', '1F LAZONA Kawasaki, 72-1 Horikawa-cho, Saiwai-ku, Kawasaki-shi', 'Kanagawa', 212, '', 'Japan', '<i>7:00-21:00</i>', 'Depends on Lazona Kawasaki', '81-44-874-8388', '', '35.5319029\n', '139.69588220000003\n', '', '', 'Y', '', 'Japan'),
(57, 'Kintetsu Shin-Honten', 'B2 Floor, Abeno Harukas Wing Bldg., 1-1-43 Abenosuji Abeno-ku Osaka-shi', 'Osaka', 545, '', 'Japan', '<i>10:00-21:00</i>', 'Depends on Abeno Harukas', '81-6-6625-2712', '', '34.6458932\n', '135.5138905\n', '', '', '', '', 'Japan'),
(58, 'Makuhari', 'COMING IN JULY 2015', 'Chiba', 0, '', 'Japan', '', '', '', '', '35.6766524', '139.7651227', '', 'Y', '', 'Y', 'Japan'),
(59, 'Marunouchi', '1F Iiyo, 1-4-1 Marunouchi, Chiyoda-ku', 'Tokyo', 100, '', 'Japan', '<i>Ouvert de 8:00 à 22:30</i>', 'Depends on Iiyo', '81-3-6269-9411', '', '35.6766524\n', '139.7651227\n', '', 'Y', '', '', 'Japan'),
(60, 'Matsuya Ginza', 'B1F Matsuya Ginza, 3-6-1 Ginza, Chuo-ku', 'Tokyo', 104, '', 'Japan', '<i>10:00-20:00</i>', 'Depends on Matsuya Ginza', '81-3-3567-1211', '', '35.67208919999999\n', '139.77059199999997\n', '', '', '', '', 'Japan'),
(61, 'Nagoya', '1F Nagoya Central Garden, 2-1-20 Takami, Chikusa-ku, Nagoya-shi', 'Aichi', 464, '', 'Japan', '<i>9:00-20:00</i>', 'New Year''s day', '81-52-757-3188', '', '35.172669\n', '136.94741939999994\n', '', '', 'Y', '', 'Japan'),
(62, 'Nagoya Takashimaya', 'B2F Nagoya Takashimaya, 1-1-4 Meieki, Nakamura-ku, Nagoya-shi', 'Aichi', 450, '', 'Japan', '<i>10:00-20:00</i>', 'Depends on Nagoya Takashimaya', '81-52-566-8105', '', '35.1712998\n', '136.88265030000002\n', '', '', '', '', 'Japan'),
(63, 'Nihonbashi Takashimaya', 'B1F Nihonbashi Takashimaya, 2-4-1 Nihonbashi, Chuo-ku', 'Tokyo', 103, '', 'Japan', '<i>10:00-20:00</i>', 'Depends on Nihonbashi Takashimaya', '81-3-3211-4111', '', '35.6807271\n', '139.77375540000003\n', '', '', '', '', 'Japan'),
(64, 'Sagamioono', '2F bono Sagamiono, 3-2 Sagamiono, Minami-ku, Sagamihara-shi', 'Kanagawa', 252, '', 'Japan', '<i>10:00-21:00</i>', 'Depends on bono Sagamiono', '81-42-705-8481', '', '35.5298142\n', '139.43336679999993\n', '', 'Y', '', '', 'Japan'),
(65, 'Seibu Ikebukuro', 'B1F Seibu Ikebukuro, 1-28-1 Minami-Ikebukuro, Toshima-ku', 'Tokyo', 171, '', 'Japan', '<i>10:00-21:00</i>', 'Depends on Seibu Ikebukuro', '81-3-5949-5011', '', '35.7254176\n', '139.71283819999996\n', '', '', '', '', 'Japan'),
(66, 'Sendai Izumi Park Town', '1F Izumi Park Town TAPIO South Tower, 6-4-1 Teraoka, Izumi-ku, Sendai-shi', 'Miyagi', 981, '', 'Japan', '<i>10:00-20:00</i>', 'Depends on Izumi Park Town TAPIO', '81-22-355-6755', '', '38.3426358\n', '140.83615180000004\n', '', '', '', '', 'Japan'),
(67, 'Sendai Mitsukoshi', 'B1F Sendai Mitsukoshi, 4-8-15 Ichiban-cho, Aoba-ku, Sendai-shi', 'Miyagi', 980, '', 'Japan', '<i>10:00-19:00</i>', 'Wednesdays', '81-22-225-7111', '', '38.2651376\n', '140.87115849999998\n', '', '', '', '', 'Japan'),
(68, 'Shinagawa', 'COMING IN APRIL 2015', 'Tokyo', 0, '', 'Japan', '', '', '', '', '35.6772533', '139.70742470000005', '', '', '', 'Y', 'Japan'),
(69, 'Shinjuku Takashimaya', 'B1F Shinjuku Takashimaya, 5-24-2 Sendagaya, Shibuya-ku', 'Tokyo', 151, '', 'Japan', '<i>10:00-20:00</i>', 'Depends on Shinjuku Takashimaya', '81-3-3358-0904', '', '35.6772533\n', '139.70742470000005\n', '', '', '', '', 'Japan'),
(70, 'Sogo Omiya', 'B1F 1-6-2 Sakuragi-cho Omiya-ku Saitama-shi', 'Tokyo', 330, '', 'Japan', '<i>10:00-20:00</i>', 'Depends on Sogo Omiya81-48-782-6615', '', '', '35.64274109999999', '139.73515550000002', '', '', '', '', 'Japan'),
(71, 'Takanawa', '1-4-21 Takanawa, Minato-ku', 'Tokyo', 108, '', 'Japan', '<i>8:00-20:00</i>', 'New Year''s day, 2nd January', '81-3-5420-9683', '', '35.6435388\n', '139.7359649\n', '', '', 'Y', '', 'Japan'),
(72, 'Tama Plaza', 'B1F Tokyu Department Store Tama Plaza, 1-7 Utsukushigaoka, Aoba-ku, Yokohama-shi', 'Kanagawa', 225, '', 'Japan', '<i>10:00-20:00</i>', 'Depends on Tokyu Tama Plaza', '81-45-903-2743', '', '35.5885554\n', '139.54536659999997\n', '', '', '', '', 'Japan'),
(73, 'Tokyo Daïmaru ', 'B1F Daimaru Tokyo, 1-9-1 Marunouchi, Chiyoda-ku', 'Tokyo', 100, '', 'Japan', '<i>10:00-20:00</i>', 'Depends on Daimaru Tokyo', '81-3-3201-5120', '', '35.6766524\n', '139.7651227\n', '', '', '', '', 'Japan'),
(74, 'Tokyo Midtown', 'B1F Tokyo Midtown Galleria, 9-7-4 Akasaka, Minato-ku', 'Tokyo', 107, '', 'Japan', '<i>11:00-21:00</i>', 'Depends on Tokyo Midtown', '81-3-6804-6285', '', '35.6662026\n', '139.7312591\n', '', '', '', '', 'Japan'),
(75, 'Toranomon', 'Garden house 2nd floor, 1-23-3 Toranomon, Minato-ku, Tokyo ', 'Tokyo', 105, '', 'Japan', '<i>8:00-22:00</i>', 'Depends on Toranomon Hills', '81-3-6257-3976', '', '35.6621396\n', '139.7438942\n', '', 'Y', '', '', 'Japan'),
(76, 'Yokohama Sogo ', 'B2F Sogo Yokohama, 2-18-1 Takashima, Nishi-ku, Yokohama-shi', 'Kanagawa', 220, '', 'Japan', '<i>10:00-20:00</i>', 'Depends on Sogo Yokohama', '81-45-465-2111', '', '35.4622889\n', '139.62228989999994\n', '', '', '', '', 'Japan'),
(77, 'Tanger', 'Rue des Amoureux', 'Tanger', 0, '', 'Morocco', '<i>7:00AM to 11:00PM</i>', '', ' +212.539.331.683', 'eric.kayser.tanger@gmail.com ', '35.785622', '-5.823696', '', '', 'Y', '', 'Morocco'),
(78, 'Aéropuerto T2', 'Aeropuerto Internacional de Mexico', 'Mexico City, DF', 0, '', 'Mexico', '', 'None', '', '', '19.436111\n', '-99.07194400000003\n', '', 'Y', '', '', 'Mexico'),
(79, 'PERISUR', 'Centro comercial de Perisur, Anillo periférico sur 4690, local 371, segundo piso, Col. Jardines del pedregal de San Ángel, Del. Coyoacán, 04500 México DF', 'Mexico City, DF', 4500, '', 'Mexico', '', 'None', '', '', '19.3051456\n', '-99.1940586\n', '', 'Y', '', '', 'Mexico'),
(80, 'Reforma', 'Paseo de la Reforma', 'Mexico City, DF', 6600, '', 'Mexico', '', 'None', '', '', '19.426480', '-99.194150', '', 'Y', '', '', 'Mexico'),
(81, 'Promenade', 'F&B-L11, Lower Level, Promenade, Greenhills Shopping Center, Ortigas Avenue, San Juan', 'Manila', 0, '', 'Philippines', '', '', '941 0738', 'info@maison-kayser.com.ph', '14.5995124', '120.9842195', '', '', 'Y', '', 'Philippines'),
(82, 'Rockwell', 'Concourse Level, Rustan''s Supercenter Inc., Rockwell Powerplant Mall, J.P. Rizal Makati City.', 'Manila', 0, '', 'Philippines', '', '', '890 0850', 'info@maison-kayser.com.ph', '14.5995124', '120.9842195', '', '', '', '', 'Philippines'),
(83, 'Makati', 'Ground Floor Rustan''s Building Ayala Ave. Center Makati 1229', 'Manila', 0, '', 'Philippines', '', '', '828 1798', 'info@maison-kayser.com.ph', '14.5995124', '120.9842195', '', '', '', '', 'Philippines'),
(84, 'SSI Fort', 'Market Place - SSI Northwest Bldg. 5th Ave. Bonifacio High Street, BGC, Taguig City', 'Manila', 0, '', 'Philippines', '', '', '90 53 01 13 15', 'info@maison-kayser.com.ph', '14.550791', '121.050274', '', '', '', '', 'Philippines'),
(85, 'Amoreiras Plaza', 'Rue Silva Calvalho 321, Loja C', 'Lisbonne', 1250, '', 'Portugal', '<i>7:30AM to 8:30PM</i>', 'December 25th and January 1st.', ' +351.211.927.894', 'contacto@erickayser.pt', '38.7121737', '-9.13963590000003', '', '', 'Y', '', 'Portugal'),
(86, 'CARMO', 'Rua do Carmo, 70', 'Lisbonne', 1200, '', 'Portugal', '<i>7:30AM to 8:30PM</i>', 'December 25th and January 1st.', ' +351.211.927.894', 'contacto@erickayser.pt', '38.7131021\n', '-9.142189899999948\n', '', 'Y', '', '', 'Portugal'),
(87, 'CASCAIS', 'Cascais shopping center', 'Cascais', 0, '', 'Portugal', '', '', '', '', '38.6970565\n', '-9.422294500000021\n', '', '', '', '', 'Portugal'),
(88, 'CORTE INGLES', 'Shopping Shop Corte Ingles, Avenida António Augusto de Aguiar 31', 'Lisbonne', 1069, '', 'Portugal', '', '', '', 'contacto@erickayser.pt', '38.733566', ' -9.1537750', '', '', '', '', 'Portugal'),
(89, 'EK Kinshasa', '208 Avenue de l''Equateur', 'Kinshassa', 0, '', 'RDC', '<i>07:00 - 19:00</i>', 'None', '', 'info@maison-kayser.cd', '-4.301483', '15.310480', '', '', 'Y', '', 'RDC'),
(91, 'GRANADA MALL - Coming soon', 'Granada Mall, Shudada District', 'Riyadh', 0, '', 'Saudi Arabia', '<i>10:00AM–11:00PM<br>\nFriday: 1:00PM–11:00PM</i>', '', '+966.555.618.645', 'Erickayser-sa@dg.sa', '24.633333', '46.71666700000003', '', '', 'Y', '', 'SaudiArabia'),
(92, 'Al Moussa Center - Coming soon', 'TAKHASSOUSI Street – Al Moussa Center', 'Riyadh', 0, '', 'Saudi Arabia', '<i>9:00AM – 12 :00AM</i>', 'None', '+966.555.618.645', 'Erickayser-sa@dg.sa', '24.693335\n', '46.689867\n', '', '', 'Y', '', 'SaudiArabia'),
(93, 'DAKAR', '3 Boulevard de la République', 'Dakar', 1262, '', 'Sénégal', '', '', ' +221.33.872.8182', '', '14.6649217\n', '-17.43489420000003\n', '', '', 'Y', '', 'Senegal'),
(94, 'Grandstand', '200 Turf Club Road, #01-11 The Grandstand', 'Singapore', 287994, '', 'Singapore', '<i>09:00-19:00</i>', 'None', '65-6468-7172', '', '1.3391158\n', '103.7927985\n', '', '', '', '', 'Singapore'),
(95, 'Scotts Square', '6 Scotts Road, Scotts Square #B1-09', 'Singapore', 228209, '', 'Singapore', '<i>08:00-22:00</i>', 'None', '65 66 36 36 72', '-', '1.305811\n', '103.83272699999998\n', '', 'Y', '', '', 'Singapore'),
(96, 'Weelock Place', '501 Orchard Road, B2-01', 'Singapore', 238880, '', 'Singapore', '<i>08:00-22:00</i>', 'None', '65 62 35 45 68', '', '1.305055', '103.830802', '', 'Y', '', '', 'Singapore'),
(97, '45th floor, 63 tower', '50 Yeouido, Yeongdaungp-Gu', 'Seoul', 0, '', 'South Corea', '', '', '', '', '37.561970', '126.991953', '', '', 'Y', '', 'southcorea'),
(98, 'GF, 63 Tower', '50 Yeouido, Yeongdaungp-Gu', 'Seoul', 0, '', 'South Corea', '', '', ' +82.2.789.5687', '', '37.561971', '126.991954', '', '', 'Y', '', 'southcorea'),
(99, 'Galleria West B1', '494 Apguejeong, Apguejeong-Gu', 'Seoul', 0, '', 'South Corea', '', '', '', '', '37.5646275', '126.97806190000006', '', '', 'Y', '', 'southcorea'),
(100, 'Plaza Hotel Arcade', '23 Taepyeongno 2 ga', 'Seoul', 0, '', 'South Corea', '', '', ' +82.2.310.7500', '', '37.565811', '126.977605', '', '', 'Y', '', 'southcorea'),
(101, 'Breeze Centre', 'B2F, No.39, Sec.1, Fu-Xing S. Road', 'Taipei City', 105, '', 'Taiwan', '<i>10:00-22:00</i>', 'None', '(02)6600-8888#7507\n(', 'mk@breeze.com.tw', '25.047324', '121.544587', '', '', '', '', 'Taiwan'),
(102, 'COMING SOON', '03/1 Soi Sukhumvit 55, Sukhumvit Road, North Klongton Wattana', 'Bangkok', 10110, '', 'Thailand', '', '', '', '', '13.736114', ' 100.602155', '', 'Y', '', 'Y', 'Thailand'),
(103, 'La Marsa', '2 Avenue de l''Indépendance', 'Tunis - La Marsa', 2070, '', 'Tunisie', '', 'None', '', '', '36.876968', '10.326704', '', '', 'Y', '', 'tunisia'),
(104, 'Dubaï Mall', 'Doha Road - Lower Level', 'Dubaï', 0, '', 'United Arab Emirates', '', '', '', '', '25.2048493', '55.270782800000006', '', 'Y', '', '', 'emirates'),
(105, 'Dubaï Mall - Galeries Lafayette', 'Doha Road - Lower Level', 'Dubaï', 0, '', 'United Arab Emirates', '', '', '', '', '25.2048493\n', '55.270782800000006\n', '', '', 'Y', '', 'emirates'),
(106, 'COMING SOON', '326 Bleecker street', 'NewYork, NY', 0, '', 'USA', '', '', '', '', '40.7522778', '-73.98262019999999', '', '', '', 'Y', 'unitedstates'),
(107, 'Bryan Park', '8 W, 40th St', 'NewYork, NY', 10018, '', 'USA', '<i>USA</i>', ' 6:30AM-7:30PM', '', '(212) 354-2300', '40.7674767', '-73.9813421', '', 'Y', '', '', 'unitedstates'),
(108, 'Columbus Circle', '1800 Broadway', 'NewYork, NY', 10019, '', 'USA', '<i>USA</i>', ' 7AM-10PM', '', '(212) 245-4100', '40.7680353\n', '-73.982371\n', '', 'Y', '', '', 'unitedstates'),
(109, 'Flatiron', '921 Broadway (21st St)', 'NewYork, NY', 10010, '', 'USA', '<i>USA</i>', ' 7AM-10PM', '', ' (212) 979-1600', '40.7388319\n', '-73.9815337\n', '', 'Y', '', '', 'unitedstates'),
(110, 'UES 3rd@74th', '1292 Third Avenue (bet. 74th and 75th St)', 'NewYork, NY', 10021, '', 'USA', '<i>USA</i>', ' 7AM-10PM', '', '(212) 744-3100', '40.7711827\n', '-73.95959519999997\n', '', 'Y', '', '', 'unitedstates'),
(111, 'UES 3rd@87th', '1535 Third Av (Bw 86th & 87th)', 'NewYork, NY', 10075, '', 'USA', '<i>USA</i>', ' 7AM-10PM', '', '(212)348-8400', '40.7728432\n', '-73.9558204\n', '', 'Y', '', '', 'unitedstates'),
(112, 'Upper West Side', '2161 Broadway (Bw 76th & 77th)', 'NewYork, NY', 10024, '', 'USA', '<i>USA</i>', ' 7AM-10PM', '', '(212) 873-5900', '40.782399', '-73.981447', '', 'Y', '', '', 'unitedstates'),
(147, 'Almada Mall', 'Almada Shopping Center', 'Almada', 0, '', 'Portugal\r\n', '', '', '', '', '38.6570801', '-9.17869799999994', '', '', '', '', 'Portugal');

-- --------------------------------------------------------

--
-- Table structure for table `kay_pagecontent`
--

CREATE TABLE IF NOT EXISTS `kay_pagecontent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `kay_pagecontent`
--

INSERT INTO `kay_pagecontent` (`id`, `title`, `body`) VALUES
(1, 'Notre histoire', '<p>Si l&rsquo;histoire de la <a href="#">Maison Kayser</a> a <s>commenc&eacute;</s> &agrave; s&rsquo;&eacute;crire au 8 rue Monge un certain vendredi 13 septembre 1996, ce n&rsquo;est en d&eacute;finitive que la suite logique d&rsquo;un parcours entam&eacute; par un homme depuis son plus jeune &acirc;ge.</p>\r\n\r\n<p>Eric Kayser a re&ccedil;u le go&ucirc;t du pain en h&eacute;ritage, en naissant dans une famille de boulangers. En rejoignant tr&egrave;s t&ocirc;t les Compagnons du Devoir, il a non seulement perp&eacute;tu&eacute; la tradition mais d&eacute;velopp&eacute; son go&ucirc;t pour la transmission et l&rsquo;&eacute;volution de la connaissance.</p>\r\n\r\n<h3>REDONNER DU GO&Ucirc;T AU PAIN</h3>\r\n\r\n<p>Bien peu croyaient au retour d&rsquo;un pain de haute qualit&eacute; au d&eacute;but des ann&eacute;es 1990. Eric Kayser a &eacute;t&eacute; de ceux-ci et a pari&eacute; sur le d&eacute;veloppement du pain dit &laquo; de tradition &raquo;, r&eacute;alis&eacute; &agrave; partir de farines d&eacute;pourvues d&rsquo;additif et &eacute;labor&eacute;es &agrave; partir de bl&eacute;s s&eacute;lectionn&eacute;s.<br />\r\nIl a souhait&eacute; &eacute;galement remettre en lumi&egrave;re le travail sur levain naturel, un agent de fermentation vivant, d&eacute;veloppant le go&ucirc;t et la conservation du pain.</p>\r\n\r\n<p>Pour ce faire, c&rsquo;est au sein de l&rsquo;INBP - Institut National de la Boulangerie-P&acirc;tisserie - qu&rsquo;il officiera en tant que formateur pendant pr&egrave;s de 10 ans, puis au sein de sa propre entit&eacute; de conseil. Sa parole et ses m&eacute;thodes sont entendues et reproduites &agrave; travers le monde, l&rsquo;occasion d&rsquo;un &eacute;change enrichissant pour les boulangers autant que pour ce globe-trotter du pain. Il fait ainsi rayonner le savoir-faire fran&ccedil;ais et s&rsquo;inscrit dans une d&eacute;marche de partage, essentielle pour lui.</p>\r\n\r\n<p>En 1994, l&rsquo;invention du Fermentolevain, une machine permettant de maintenir un levain liquide &agrave; temp&eacute;rature constante et ainsi faciliter sa mise en oeuvre, pose la clef de voute de son travail et de sa qu&ecirc;te d&rsquo;un pain &agrave; la mie cr&eacute;meuse, aux ar&ocirc;mes de c&eacute;r&eacute;ales et de fruits secs... avec une excellente conservation.</p>\r\n\r\n<h4>CONSTRUIRE UNE ENTREPRISE ARTISANALE DYNAMIQUE ET OUVERTE SUR LE MONDE</h4>\r\n\r\n<p>Encore plus que par sa pr&eacute;c&eacute;dente activit&eacute; d&rsquo;enseignant, Eric Kayser est parvenu &agrave; transmettre sa vision de la boulangerie en cr&eacute;ant une entreprise aujourd&rsquo;hui largement pr&eacute;sente &agrave; l&rsquo;international.! Ainsi, c&rsquo;est au Japon que la Maison Kayser s&rsquo;est install&eacute;e d&egrave;s 2002 et compte d&eacute;sormais 32 boutiques, faisant de ce pays notre principale zone d&rsquo;implantation.<br />\r\nL&rsquo;implication et le talent des ouvriers et chefs japonais inspirent de nombreux collaborateurs du groupe, qu&rsquo;ils soient fran&ccedil;ais, portugais, philippins, am&eacute;ricains, chiliens... m&ecirc;me si c&rsquo;est &agrave; Paris que continue &agrave; vibrer le coeur de l&rsquo;entreprise : en effet, chacun des chefs y sont form&eacute;s avant de prendre leur poste, au travers d&rsquo;un processus pouvant durer jusqu&rsquo;&agrave; un an.</p>\r\n\r\n<p>La &laquo; griffe &raquo; Kayser s&rsquo;est imprim&eacute;e d&egrave;s le d&eacute;but en ouvrant des boulangeries adapt&eacute;es &agrave; leurs villes et quartiers, o&ugrave; le pain est produit sur place tout au long de la journ&eacute;e. Un engagement qui symbolise le haut niveau d&rsquo;exigence de notre enseigne, mais aussi la profonde volont&eacute; de ne pas cr&eacute;er une simple &laquo; chaine &raquo;. Au contraire, chaque point de vente est un v&eacute;ritable lieu de vie, fluide, clair et accueillant, o&ugrave; l&rsquo;on prend plaisir &agrave; se rendre et &agrave; se rencontrer tout au long de la journ&eacute;e.</p>\r\n\r\n<p>Dans chaque pays, les produits s&rsquo;adaptent aux cultures et aux go&ucirc;ts des locaux : si notre baguette Monge reste une signature indissociable de la Maison Kayser, les pains se font parfois moelleux, color&eacute;s, petits... les formats et les saveurs varient mais notre savoir-faire, la s&eacute;lection des mati&egrave;res premi&egrave;res et le travail sur levain naturel demeurent.</p>\r\n\r\n<h4>CONTINUER &Agrave; PARTAGER L&rsquo;AMOUR DU BON PAIN</h4>\r\n\r\n<p>M&ecirc;me si l&rsquo;on pourrait consid&eacute;rer que le chemin parcouru est d&eacute;j&agrave; important, Eric Kayser voit au contraire de nouvelles opportunit&eacute;s et ouvertures sur des territoires o&ugrave; le pain fran&ccedil;ais n&rsquo;a pas encore p&eacute;n&eacute;tr&eacute; comme il l&rsquo;a fait ailleurs.<br />\r\nC&rsquo;est ainsi que la Chine, Hong Kong ou New York ont rejoint la liste de nos implantations au cours de ces derniers mois. A chaque minute, une partie du monde se l&egrave;ve sur nos douceurs, et c&rsquo;est &agrave; chaque fois l&rsquo;occasion de relever ce d&eacute;fi cher &agrave; notre maison : partager l&rsquo;amour du bon pain, aussi bien aupr&egrave;s de nos 1500 collaborateurs &agrave; travers le monde qu&rsquo;au sein de notre client&egrave;le.</p>\r\n\r\n<p>De cette fa&ccedil;on, notre histoire continue &agrave; s&rsquo;&eacute;crire, jour apr&egrave;s jour. Pour toujours rester en phase avec les attentes de l&rsquo;&eacute;poque, nous faisons &eacute;voluer nos produits pour leur donner plus de go&ucirc;t, adopter des m&eacute;thodes de fabrication plus pointues, s&eacute;lectionner des mati&egrave;res premi&egrave;res de haute qualit&eacute;. Le pain est un aliment d&eacute;mocratique par excellence, dont personne ne devrait &ecirc;tre priv&eacute;. Depuis la fin de l&rsquo;ann&eacute;e 2013, les intol&eacute;rants au gluten peuvent profiter de ce plaisir, gr&acirc;ce &agrave; une gamme &laquo; libre de gluten &raquo; propos&eacute;e au sein de notre boulangerie parisienne de la rue de l&rsquo;Echelle.</p>\r\n\r\n<p>Pour demain, la Maison Kayser souhaite conserver sa signature d&rsquo;artisan boulanger et porter haut et fort les valeurs qui y sont associ&eacute;es. Aussi bien dans les lieux de transit - gares, a&eacute;roports, ... - que dans les centres commerciaux ou art&egrave;res passantes, la recette demeure la m&ecirc;me. Pour la partager dans les p&eacute;trins et les fournils, une &eacute;cole de boulangerie et p&acirc;tisserie verra le jour dans les mois &agrave; venir, en France puis sur les autres continents o&ugrave; notre enseigne est pr&eacute;sente.</p>'),
(2, 'Notre philosophie', '<p>Le bon pain ne saurait &ecirc;tre r&eacute;alis&eacute; sans une certaine philosophie, des valeurs et des fondements sur lesquels s&#39;appuient nos boulangers chaque jour. D&egrave;s le d&eacute;but, Eric Kayser a b&acirc;ti son entreprise sur des bases solides, imprimant un mouvement singulier et sans cesse renouvel&eacute; depuis lors.</p>\r\n\r\n<h4>L&#39;INNOVATION EN FER DE LANCE</h4>\r\n\r\n<p>De prime abord, on pourrait penser que l&#39;innovation n&#39;est pas un &eacute;l&eacute;ment fondamental du m&eacute;tier d&#39;artisan boulanger, port&eacute; couramment par les traditions et des m&eacute;thodes ancestrales. Ce n&#39;est pourtant pas la conviction d&#39;Eric Kayser, qui a multipli&eacute; les travaux pour apporter au pain les atouts du progrès en magnifiant sa saveur et sa conservation.</p>\r\n\r\n<p>Cette d&eacute;marche se poursuit bien au del&agrave; de l&rsquo;aspect technologique, dans la cr&eacute;ation de nouveaux am&eacute;nagements de magasins, toujours plus lumineux et vivants, devenant de v&eacute;ritables lieux de vie : c&rsquo;est une dimension bien nouvelle au sein de la boulangerie fran&ccedil;aise, directement inspir&eacute;e des concepts observ&eacute;s &agrave; l&rsquo;&eacute;tranger, et notamment en Asie o&ugrave; l&rsquo;on con&ccedil;oit mal de ne pas pouvoir s&rsquo;asseoir pour d&eacute;guster douceurs et gourmandises.<br />\r\nL&rsquo;ouverture sur le monde, particuli&egrave;rement marqu&eacute;e au sein de la maison Kayser, permet d&rsquo;enrichir jour apr&egrave;s jour notre vision de l&rsquo;artisanat boulanger, afin de le faire &eacute;voluer et de le rendre toujours plus vivant et en phase avec les attentes de notre &eacute;poque. Ce n&rsquo;est pas un hasard si nous avons inaugur&eacute; des &laquo; boulangeries-restaurant &raquo; (85, boulevard Malesherbes &ndash; Paris 8&egrave; et Bercy Village, Paris 12&egrave;) et un Bar &agrave; Tartines (18 rue du Bac, Paris 7&egrave;), qui sont autant des innovations que les marqueurs de tendances profondes.</p>\r\n\r\n<p>Les produits ne sont pas mis &agrave; l&rsquo;&eacute;cart de cette d&eacute;marche globale, avec de nombreuses cr&eacute;ations aux saveurs et formes atypiques. Du croissant au th&eacute; vert matcha, largement pl&eacute;biscit&eacute; au Japon, aux pains libres de gluten d&eacute;velopp&eacute;s pour la boulangerie parisienne de la rue de l&rsquo;Echelle, on retrouve &agrave; chaque fois le savoir-faire et l&rsquo;exigence d&rsquo;Eric Kayser et ses &eacute;quipes.</p>\r\n\r\n<p>Des &eacute;quipes dont l&rsquo;organisation est justement aussi innovante que les produits qu&rsquo;elles fa&ccedil;onnent chaque jour. Dans chaque boutique, nos chefs boulangers sont disposent de la latitude n&eacute;cessaire pour g&eacute;rer leur laboratoire de la m&ecirc;me fa&ccedil;on qu&rsquo;ils le feraient au sein de leur propre &eacute;tablissement : en donnant &agrave; chacun la possibilit&eacute; de mener des initiatives et de s&rsquo;&eacute;panouir, la Maison Kayser grandit autant que ceux qui la composent.</p>\r\n\r\n<h4>PARTAGER LA CONNAISSANCE AVEC LE GRAND PUBLIC</h4>\r\n\r\n<p>La transmission s&rsquo;op&egrave;re &eacute;galement aupr&egrave;s du grand public, avec des ouvrages de recettes accessibles &agrave; tous. Tartinez Gourmand, 100% pain, Autour des pains d&rsquo;Eric Kayser, ... Le dernier, et sans doute le plus remarquable, est le &laquo; Larousse du Pain &raquo; : 80 recettes, plus de 800 photographies... voil&agrave; qui devrait donner envie &agrave; chacun de &laquo; mettre la main &agrave; la p&acirc;te &raquo;, pour d&eacute;couvrir cette merveilleuse mati&egrave;re, empreinte de vie et d&rsquo;un savoir-faire mill&eacute;naire.</p>\r\n\r\n<h4>LE CHOIX ET LE RESPECT DES MATI&Egrave;RES PREMI&Egrave;RES</h4>\r\n\r\n<p>La connaissance doit s&rsquo;appuyer sur une s&eacute;lection rigoureuse des mati&egrave;res premi&egrave;res : le pain ne ment pas, et il dit tout de ce et ceux qui le font. Eric Kayser a plac&eacute; au centre de ses pr&eacute;occupations la mise en oeuvre de c&eacute;r&eacute;ales de haute qualit&eacute;. Ainsi, les farines de Bl&eacute;, Sarrasin, Epeautre, ... sont issues de l&rsquo;agriculture raisonn&eacute;e et proviennent des meilleurs terroirs. Les amateurs de pains biologiques peuvent &eacute;galement en trouver au sein de notre boulangerie du 14 rue Monge, Paris 5&egrave;.<br />\r\nCette exigence se prolonge dans le choix des produits laitiers, fruits et autres mati&egrave;res premi&egrave;res mises en oeuvre au sein de nos fournils et laboratoires. Nos boulangers et p&acirc;tissiers observent le plus grand soin lors de leur mise en oeuvre : nos pains font l&rsquo;objet d&rsquo;un p&eacute;trissage lent et doux, pr&eacute;servant toute la typicit&eacute; et les parfums des c&eacute;r&eacute;ales, tandis que nous privil&eacute;gions des cuissons et fa&ccedil;onnages respectueux des fruits, du chocolat, ... pour nos gourmandises.</p>'),
(3, 'Nos méthodes', '<p>Si la Maison Kayser s&rsquo;est affirm&eacute;e au fil du temps comme une r&eacute;f&eacute;rence en terme de qualit&eacute; de produit, aussi bien pour ses pains que ses viennoiseries ou p&acirc;tisseries, c&rsquo;est le fruit d&rsquo;un savoir- faire toujours renouvel&eacute; et compl&eacute;t&eacute;.</p>\r\n\r\n<h4>LE LEVAIN NATUREL</h4>\r\n\r\n<p>Ce qui a distingu&eacute; Eric Kayser d&egrave;s le d&eacute;but, et encore aujourd&rsquo;hui, c&rsquo;est sa volont&eacute; d&rsquo;utiliser le levain naturel pour faire pousser ses pains et viennoiseries.<br />\r\nCet agent de fermentation est &eacute;troitement li&eacute; &agrave; l&rsquo;histoire du pain : en effet, ce m&eacute;lange d&rsquo;eau, de farine et de sucres (de miel, pour la Maison Kayser), rafraichi r&eacute;guli&egrave;rement et laiss&eacute; reposer, a &eacute;t&eacute; le premier a &ecirc;tre utilis&eacute;, son caract&egrave;re naturel n&rsquo;y &eacute;tant pas &eacute;tranger.<br />\r\nNos boulangers modernes ont eu tendance &agrave; l&rsquo;oublier au XX&egrave; si&egrave;cle, privil&eacute;giant l&rsquo;emploi de la levure. En effet, cette derni&egrave;re est moins difficile &agrave; ma&icirc;triser, et permet d&rsquo;obtenir rapidement des pains lev&eacute;s et pr&eacute;sentant un certain volume. Cependant, leur conservation est moins bonne et leurs parfums souvent plus pauvres que ceux pouss&eacute;s &agrave; partir de levain naturel.</p>\r\n\r\n<p>C&rsquo;est la raison principale qui a pouss&eacute; Eric Kayser a remettre &agrave; l&rsquo;honneur ce proc&eacute;d&eacute;, qui est aujourd&rsquo;hui utilis&eacute; pour l&rsquo;ensemble de nos pains, croissants, brioches...<br />\r\nA une &eacute;poque o&ugrave; nous sommes toujours plus concern&eacute;s par notre sant&eacute;, cette technique de fermentation pr&eacute;sente l&rsquo;avantage d&rsquo;aboutir &agrave; des produits plus digestes et mieux tol&eacute;r&eacute;s par l&rsquo;organisme. Il ne reste ainsi plus que le plaisir !</p>\r\n\r\n<h4>PLUS QUE DES PRODUITS, UN SAVOIR-VIVRE ET UN SERVICE DE QUALIT&Eacute;</h4>\r\n\r\n<p>Savoir cr&eacute;er des produits savoureux et de qualit&eacute; ne suffit pas : il faut &eacute;galement pouvoir les vendre, et satisfaire sur tous les plans une client&egrave;le exigeante. Au sein de nos espaces de vente, les femmes et les hommes de la Maison Kayser oeuvrent pour partager au quotidien la singularit&eacute; de notre univers artisanal en ma&icirc;trisant les sp&eacute;cificit&eacute;s de chacun de nos pains, viennoiseries, p&acirc;tisseries, sandwiches... afin de vous conseiller au mieux et donner &agrave; l&rsquo;ensemble de ces douceurs et gourmandises la place qu&rsquo;elles m&eacute;ritent sur vos tables et dans vos instants de vie.</p>\r\n\r\n<h4>CONJUGUER AVEC &Eacute;L&Eacute;GANCE PLUSIEURS M&Eacute;TIERS</h4>\r\n\r\n<p>En tenant &agrave; pr&eacute;server le caract&egrave;re artisanal, polyvalent et dynamique de son entreprise, Eric Kayser l&rsquo;a positionn&eacute;e sur une multitude de m&eacute;tiers. A la fois boulangerie, p&acirc;tisserie, traiteur, restaurant pour certaines boutiques... nous mettons un point d&rsquo;honneur &agrave; donner la m&ecirc;me importance &agrave; l&rsquo;ensemble de ces activit&eacute;s, et &agrave; les conjuguer harmonieusement. Le fil conducteur reste le pain, car c&rsquo;est bien l&agrave; notre savoir-faire de base. Il s&rsquo;invite lors de toutes les d&eacute;gustations, aussi bien au travers d&rsquo;accord inventifs avec des mets raffin&eacute;s que comme contenant pour des soupes et salades, ou encore comme base pour des sandwiches aux inspirations vari&eacute;es.</p>\r\n\r\n<h4>DES GOURMANDISES POUR CHAQUE MOMENT DE LA JOURN&Eacute;E</h4>\r\n\r\n<p>Si l&rsquo;implantation de la Maison Kayser aux Etats-Unis reste r&eacute;cente, nous n&rsquo;avons pas attendu ce moment pour d&eacute;velopper de savoureux cookies, &agrave; la fois croustillants aux extr&eacute;mit&eacute;s et moelleux &agrave; coeur. Le secret ? Un p&eacute;trissage ma&icirc;tris&eacute;, des ingr&eacute;dients soigneusement pes&eacute;s et incorpor&eacute;s, une cuisson douce.</p>\r\n\r\n<p><a href="#">VIDEO</a></p>\r\n\r\n<p>Les financiers sont de petites douceurs incontournables de l&rsquo;heure du th&eacute;, et nous poss&eacute;dons tous de savoureux souvenirs autour de ces gourmandises r&eacute;gressives. A la mani&egrave;re de la fameuse Madeleine de Proust, leur cro&ucirc;te l&eacute;g&egrave;rement craquante et leur parfum d&rsquo;amandes nous renvoie en enfance &agrave; chaque d&eacute;gustation. La Maison Kayser les a d&eacute;clin&eacute;s en version miniature, au chocolat, nature ou &agrave; la pistache, pour varier les plaisirs et les saveurs.</p>\r\n\r\n<p><a href="#">VIDEO</a></p>\r\n\r\n<p>Un macaron, c&rsquo;est une succession de textures et de sensations, qui ont rendu cette petite mignardise particuli&egrave;rement c&eacute;l&egrave;bre et appr&eacute;ci&eacute;e : un peu de craquant, du fondant, de l&rsquo;onctueux... puis viennent les parfums et les couleurs, qui renouvellent &agrave; chaque fois le plaisir, autant des yeux que des papilles. Framboise, pistache, chocolat, ... nos propositions varient selon les saisons, mais le proc&eacute;d&eacute; reste le m&ecirc;me : des coques fabriqu&eacute;es &agrave; partir d&rsquo;une poudre d&rsquo;amande s&eacute;lectionn&eacute;e, meringu&eacute;es avec pr&eacute;caution par nos p&acirc;tissiers, puis g&eacute;n&eacute;reusement garnies de cr&egrave;mes, ganaches ou confits de fruit. Une gourmandise &agrave; offrir autant qu&rsquo;&agrave; s&rsquo;offrir.</p>\r\n\r\n<p><a href="#">VIDEO</a></p>\r\n\r\n<p>Nos viennoiseries sont &eacute;galement r&eacute;alis&eacute;es &agrave; partir de levain naturel, ce qui leur conf&egrave;re un croustillant et une conservation exceptionnels. Lentement p&eacute;trie, la p&acirc;te (ou d&eacute;trempe) se voit ensuite incorporer le beurre, tout en observant les temps de repos n&eacute;cessaires &agrave; l&rsquo;obtention d&rsquo;un produit tr&egrave;s feuillet&eacute; : un bon croissant, en plus de mati&egrave;res premi&egrave;res de qualit&eacute;, c&rsquo;est surtout du temps ! Des dizaines heures de patience pour quelques secondes de plaisir...</p>\r\n\r\n<p><a href="#">VIDEO</a></p>\r\n\r\n<p>Une p&acirc;tisserie &agrave; consommer en un... &eacute;clair. Ce grand classique de notre r&eacute;pertoire sucr&eacute; s&rsquo;est vu replac&eacute; sur le devant de la sc&egrave;ne, avec l&rsquo;engouement du public pour les sp&eacute;cialit&eacute;s &agrave; base de p&acirc;te &agrave; choux. La Maison Kayser n&rsquo;a pas attendu la tendance pour soigner ses &eacute;clairs et les d&eacute;cliner en de multiples saveurs : chocolat et caf&eacute; bien s&ucirc;r, mais &eacute;galement pistache, entre autres variations de saison.</p>\r\n\r\n<p><a href="#">VIDEO</a></p>\r\n\r\n<p>Chacun a sa recette de tarte, qu&rsquo;elle soit sabl&eacute;e, feuillet&eacute;e, bris&eacute;e... La Maison Kayser les d&eacute;cline selon les saisons et les inspirations des p&acirc;tissiers, m&ecirc;me si certaines d&rsquo;entre elles sont devenues des classiques au fil du temps : la tarte Monge, du nom de notre boutique historique, associe l&rsquo;acidul&eacute; d&rsquo;une compot&eacute;e de fruits rouges &agrave; la douceur du fromage blanc, tandis que la tarte pistache-abricot transporte directement le gourmand dans la douceur d&rsquo;une apr&egrave;s-midi d&rsquo;&eacute;t&eacute;... et ce m&ecirc;me en hiver !</p>\r\n\r\n<p><a href="#">VIDEO</a></p>\r\n\r\n<p>Difficile d&rsquo;imaginer un repas sans pain. La Maison Kayser a &eacute;labor&eacute; des recettes autour de cet aliment afin d&rsquo;en faire un v&eacute;ritable compagnon gastronomique. Ainsi est n&eacute;e la &laquo; cuisine du boulanger &raquo;, que l&rsquo;on retrouve dans chaque boulangerie de notre entreprise, mais &eacute;galement dans des d&eacute;clinaisons plus pouss&eacute;es au sein de nos boulangeries-restaurant. Une miche-soupi&egrave;re ou contenant &agrave; salade, des tartines gourmandes, le fameux &laquo; pan bagnat &raquo; m&eacute;ridional, le Reuben&rsquo;s sandwich... le champ des possibles est quasi-infini, tant le pain est un merveilleux support de saveurs.</p>\r\n\r\n<p><a href="#">VIDEO</a></p>'),
(4, 'Nos équipes', '<p>Eric Kayser a su s&rsquo;entourer d&egrave;s le d&eacute;but de femmes et d&rsquo;hommes passionn&eacute;s, partageant la m&ecirc;me vision que lui du bon pain et du travail artisanal. Si aujourd&rsquo;hui la Maison Kayser est en mesure d&rsquo;offrir des produits de qualit&eacute; &agrave; travers le monde, c&rsquo;est gr&acirc;ce &agrave; l&rsquo;implication de nos collaborateurs, aussi bien au sein de nos fournils qu&rsquo;en boutique.</p>\r\n\r\n<h4>L&rsquo;HUMAIN ET LA TRANSMISSION</h4>\r\n\r\n<p>La fibre humaine d&eacute;velopp&eacute;e par Eric Kayser s&rsquo;est affirm&eacute;e au contact des Compagnons du Devoir, chez qui le partage est une valeur forte et assure la coh&eacute;sion de l&rsquo;organisation.<br />\r\nIl en est de m&ecirc;me au sein de notre maison, et c&rsquo;est ainsi que 1500 femmes et hommes ont aujourd&rsquo;hui &agrave; coeur de continuer &agrave; faire &eacute;voluer l&rsquo;entreprise, avec ses valeurs d&rsquo;artisanat, de cr&eacute;ativit&eacute; et d&rsquo;humanit&eacute;. Chacun de nos boulangers et p&acirc;tissiers est int&eacute;gr&eacute; au travers d&rsquo;un processus d&rsquo;un an, au cours duquel il re&ccedil;oit les principes et fa&ccedil;ons de faire qui marquent la &laquo; signature Kayser &raquo;. Ma&icirc;trise du levain naturel (avec utilisation du Fermentolevain), temps de fermentation, p&eacute;trissage doux et respectueux des farines, fa&ccedil;onnages soign&eacute;s... autant d&rsquo;&eacute;l&eacute;ments indispensables pour aboutir &agrave; des produits &agrave; la hauteur des attentes de notre client&egrave;le.</p>\r\n\r\n<p>Il en va de m&ecirc;me pour nos personnels de vente qui sont re&ccedil;oivent les cl&eacute;s pour servir au mieux nos clients, que ce soit en terme de qualit&eacute; de service ou d&rsquo;information sur les produits. Le pain n&rsquo;est pas une marchandise comme les autres, et la Maison Kayser transmet &agrave; ses &eacute;quipes la culture et le go&ucirc;t pour ce produit d&rsquo;exception.</p>\r\n\r\n<h4>DES CULTURES ET DES COULEURS : UNE M&Ecirc;ME PASSION</h4>\r\n\r\n<p>La diversit&eacute; de nos lieux d&rsquo;implantation est une merveilleuse opportunit&eacute; pour remettre en question nos fa&ccedil;ons de concevoir les boulangeries et les produits que nous y proposons. Chaque collaborateur contribue ainsi &agrave; notre d&eacute;veloppement, gr&acirc;ce &agrave; la flexibilit&eacute; de notre organisation : m&ecirc;me si notre entreprise a grandi, elle a gard&eacute; son caract&egrave;re artisanal, avec des structures proches des individus et de leurs aspirations. C&rsquo;est ainsi que dans tous les pays o&ugrave; nous sommes pr&eacute;sents, de la France &agrave; la Chine, en passant par le Br&eacute;sil, les Emirats Arabes Unis, le S&eacute;n&eacute;gal, le Portugal..., chacun se met au service d&rsquo;une m&ecirc;me passion : celle du bon pain.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `kay_product`
--

CREATE TABLE IF NOT EXISTS `kay_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subCategory_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B87780E93DA5256D` (`image_id`),
  KEY `IDX_B87780E912469DE2` (`category_id`),
  KEY `IDX_B87780E9DB5A7180` (`subCategory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=46 ;

--
-- Dumping data for table `kay_product`
--

INSERT INTO `kay_product` (`id`, `category_id`, `name`, `description`, `link`, `subCategory_id`, `image_id`) VALUES
(8, 2, 'Tarte Monge', 'Tarte Monge', NULL, 5, 9),
(9, 1, 'Baguette Monge', 'Farine de froment et levain liquide.\nBaguette croquante à la mie crème, goût délicat de lait et de noisettes.', NULL, 3, 15),
(10, 1, 'BAGUETTE MALESHERBES OU TOLBIAC', 'Farine de froment et levain liquide.\nBaguette non façonnée, mie grasse et dotée de belles alvéoles.', NULL, 3, 16),
(11, 1, 'GRAND RUSTIQUE', 'Farine de levain et de froment et levain liquide.  \r\n \r\nCroûte épaisse et craquante, mie brune et très alvéolée; longue saveur en bouche et très bonne conservation.', NULL, 3, 17),
(12, 1, 'TOURTE DE MEULE', 'Farine écrasée à la meule de pierre, levain et sel de Guérande.  \n \nGrosse miche dotée d''une odeur de  miel et de Genêts qui réserve une forte saveur acidulée.', NULL, 3, 18),
(13, 1, 'PAIN AUX CÉRÉALES', 'Farine de froment, levain liquide, mélange de graines: sésame, pavot, millet, lin blond, lin brun ou tournesol.', NULL, 3, 19),
(15, 1, 'LE CARRÉ', 'Farine de froment, levain liquide, sel de Guérande.\nCroustillant et alvéolé, ce pain est idéal pour des grandes tartines.', NULL, 3, 21),
(16, 1, 'PAIN AUX NOIX', 'Farine de froment, lait, levain liquide, noix.\r\n\r\nOnctuosité, croquant et douceur sont au rendez-vous!', NULL, 3, 22),
(17, 1, 'PAIN AU CURCUMA', 'Farine de froment, lait, levain liquide, curcuma, noix & noisettes. L’originalité de l’épice est adoucie par une pâte viennoise douce et relevée par le croquant des noix et des noisettes.', NULL, 3, 23),
(18, 1, 'PAIN AUX OLIVES', 'Farine de froment, levain liquide, olives vertes et noires, huile d''olive.', NULL, 3, 24),
(19, 1, 'PAIN AUX FIGUES', 'Farine de froment, levain liquide, figues séchées.\r\n\r\nSucré naturellement, pain délicat qui accompagne petit-déjeuner, terrines ou fromages.', NULL, 3, 25),
(20, 1, 'PAIN AU FROMAGE', 'Farine de froment, levain liquide, emmental.\r\nA dévorer fraîchement sorti du four!', NULL, 3, 26),
(21, 1, 'PAIN COMPLET', 'Farine complète, farine de froment et levain liquide. Mie brune et dense très riche en goût.', NULL, 3, 27),
(22, 1, 'PAIN DE SEIGLE', 'Farine de seigle, farine de froment et levain liquide.  \n        Croûte épaisse et croquante, mie brune. Saveur longue en bouche. Odeurs de sève de pin.\n\nArômes de réglisse et d’anis. Très bonne conservation.', NULL, 3, 28),
(23, 1, 'CIABATTA', 'Farine de froment, levain liquide, huile d''olive.\nPain italien très tendre.', NULL, 3, 29),
(24, 1, 'PAIN DE MIE', 'Farine de froment, lait et levain liquide.\n\nCroûte dorée, mie tendre.', NULL, 3, 30),
(25, 1, 'PAIN GONESSE SEMI-COMPLET', 'Farine de riz,farine de sarrasin, levain de riz, graines de lin, gomme de guar.', NULL, 2, 31),
(26, 1, 'PAIN POIS-CHICHE', 'Farine de pois-chiche,farine de riz, farine de sarrasin, amidon de pomme de terre, amidon de tapioca, œuf, gomme de guar.', NULL, 2, 32),
(27, 1, 'PAIN AUX MENDIANTS', 'Farine de riz, farine de sarrasin, levain de riz, mélange de graines (lin, courge, tournesol et sésame), raisin de Corinthe, pomme, abricot, gomme de guar.', NULL, 2, 33),
(28, 1, 'pain de sarrasin', 'Farine de sarrasin, levain de riz, gomme de guar.', NULL, 2, 34),
(29, 4, 'PAIN AU CHOCOLAT', 'PAIN AU CHOCOLAT', NULL, 1, 35),
(30, 4, 'PAIN AUX RAISINS', 'PAIN AUX RAISINS', NULL, 1, 36),
(31, 4, 'CHAUSSON AUX POMMES', 'CHAUSSON AUX POMMES', NULL, 1, 37),
(32, 4, 'CROISSANT AUX AMANDES', 'CROISSANT AUX AMANDES', NULL, 1, 38),
(33, 4, 'PAIN AU CHOCOLAT & AMANDES', 'PAIN AU CHOCOLAT & AMANDES', NULL, 1, 39),
(34, 4, 'BRIOCHE MOUSSELINE', 'Brioche ronde et joufflue aux œufs et au bon beurre', NULL, 1, 40),
(35, 4, 'BRIOCHE AUX PRALINES ROSES', 'Brioche riche en pralines roses', NULL, 1, 41),
(36, 4, 'BRIOCHE AU CHOCOLAT BLANC', 'Brioche aux pépites de chocolat blanc', NULL, 1, 43),
(37, 4, 'KOUGLOFF', 'Farine de froment, œufs, beurre, lait, sucre, sel, levain liquide, rhum, kirsch, raisins blonds et amandes', NULL, 1, 44),
(38, 4, 'CROISSANT', 'CROISSANT', NULL, 1, 45);

-- --------------------------------------------------------

--
-- Table structure for table `kay_product_category`
--

CREATE TABLE IF NOT EXISTS `kay_product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `kay_product_category`
--

INSERT INTO `kay_product_category` (`id`, `name`) VALUES
(1, 'pains'),
(2, 'patisseries'),
(3, 'sales'),
(4, 'viennoiseries');

-- --------------------------------------------------------

--
-- Table structure for table `kay_product_image`
--

CREATE TABLE IF NOT EXISTS `kay_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fileExtension` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=53 ;

--
-- Dumping data for table `kay_product_image`
--

INSERT INTO `kay_product_image` (`id`, `name`, `url`, `fileExtension`) VALUES
(1, '6ea94dbfd8c5e41af8060d930a9efa8e.jpeg', 'img/products/6ea94dbfd8c5e41af8060d930a9efa8e.jpeg', 'jpeg'),
(2, 'a3ebddb7f0835336d3a8bfcafa0cacac.jpeg', 'img/products/a3ebddb7f0835336d3a8bfcafa0cacac.jpeg', 'jpeg'),
(3, '41d0a307c70a0827775328ddf9d68041.jpeg', 'img/products/41d0a307c70a0827775328ddf9d68041.jpeg', 'jpeg'),
(4, '0a417b1f21c35846f14ea753c0c295b1.jpeg', 'img/products/0a417b1f21c35846f14ea753c0c295b1.jpeg', 'jpeg'),
(5, '6b48ff340855398c4f564d1bf9aad8f3.jpeg', 'img/products/6b48ff340855398c4f564d1bf9aad8f3.jpeg', 'jpeg'),
(6, 'ece3f193e78e6303e5ba40d7036646a5.jpeg', 'img/products/ece3f193e78e6303e5ba40d7036646a5.jpeg', 'jpeg'),
(7, '413ed82575a42fcc83c3262ce24841c2.jpeg', 'img/products/413ed82575a42fcc83c3262ce24841c2.jpeg', 'jpeg'),
(9, '12a4c53a6f933a667325f6471f4f550d.jpeg', 'img/products/12a4c53a6f933a667325f6471f4f550d.jpeg', 'jpeg'),
(15, 'e0053f9307843133dc3fbf70a77ab932.jpeg', 'img/products/e0053f9307843133dc3fbf70a77ab932.jpeg', 'jpeg'),
(16, 'ec4b49c484516fda068b4cde7eb45f5c.jpeg', 'img/products/ec4b49c484516fda068b4cde7eb45f5c.jpeg', 'jpeg'),
(17, '4827c4c4b9bdd9111cfdb5e5ffb601a2.jpeg', 'img/products/4827c4c4b9bdd9111cfdb5e5ffb601a2.jpeg', 'jpeg'),
(18, '9508373009b56ba126dfc589baa93a42.jpeg', 'img/products/9508373009b56ba126dfc589baa93a42.jpeg', 'jpeg'),
(19, '9e8756d5ee3f6c8ef983b02ce5823d5b.jpeg', 'img/products/9e8756d5ee3f6c8ef983b02ce5823d5b.jpeg', 'jpeg'),
(20, '69fb6de833c4cf14f789040ab201ca63.jpeg', 'img/products/69fb6de833c4cf14f789040ab201ca63.jpeg', 'jpeg'),
(21, '167bf6d66471a28df009ae9339fe49e6.jpeg', 'img/products/167bf6d66471a28df009ae9339fe49e6.jpeg', 'jpeg'),
(22, '9cb3e19f0e11c63a1c6f5c0edc401de2.jpeg', 'img/products/9cb3e19f0e11c63a1c6f5c0edc401de2.jpeg', 'jpeg'),
(23, '685aae1a441fa6f09706a4c8c4147afb.jpeg', 'img/products/685aae1a441fa6f09706a4c8c4147afb.jpeg', 'jpeg'),
(24, 'ace853afd0a7a725cc8cd8ed4c250729.jpeg', 'img/products/ace853afd0a7a725cc8cd8ed4c250729.jpeg', 'jpeg'),
(25, '3c5b6c72640cb1ce3a745779d4e2c5ed.jpeg', 'img/products/3c5b6c72640cb1ce3a745779d4e2c5ed.jpeg', 'jpeg'),
(26, '0038cc599b80d92ba702a3d335f87c4d.jpeg', 'img/products/0038cc599b80d92ba702a3d335f87c4d.jpeg', 'jpeg'),
(27, 'aa2ee79245c2ebf5f6793ce58df30f48.jpeg', 'img/products/aa2ee79245c2ebf5f6793ce58df30f48.jpeg', 'jpeg'),
(28, '8c192dc90b2c1c1a6c7e97926094af45.jpeg', 'img/products/8c192dc90b2c1c1a6c7e97926094af45.jpeg', 'jpeg'),
(29, '6b4683df66490e5ef69bf185e45c1f1d.jpeg', 'img/products/6b4683df66490e5ef69bf185e45c1f1d.jpeg', 'jpeg'),
(30, 'ce5078debf0315b65dad4cc190fd731c.jpeg', 'img/products/ce5078debf0315b65dad4cc190fd731c.jpeg', 'jpeg'),
(31, '6eb8322def3ba141cc4508f6aff41302.jpeg', 'img/products/6eb8322def3ba141cc4508f6aff41302.jpeg', 'jpeg'),
(32, 'e81be395314c679600f2d10d3300dea8.jpeg', 'img/products/e81be395314c679600f2d10d3300dea8.jpeg', 'jpeg'),
(33, '20a0c2cc4baed77609457d6fcf678a96.jpeg', 'img/products/20a0c2cc4baed77609457d6fcf678a96.jpeg', 'jpeg'),
(34, 'feaecabf1bc56297c0cc8a4f5a47ae56.jpeg', 'img/products/feaecabf1bc56297c0cc8a4f5a47ae56.jpeg', 'jpeg'),
(35, 'df01e63a97663521e4a5df1078663547.jpeg', 'img/products/df01e63a97663521e4a5df1078663547.jpeg', 'jpeg'),
(36, '25b5f0bbe39332f0791ac4414f039b31.jpeg', 'img/products/25b5f0bbe39332f0791ac4414f039b31.jpeg', 'jpeg'),
(37, '143576966c7e6a191acca2f2734f9b5e.jpeg', 'img/products/143576966c7e6a191acca2f2734f9b5e.jpeg', 'jpeg'),
(38, '4426787a5cb4694fae47456839f6e9d7.jpeg', 'img/products/4426787a5cb4694fae47456839f6e9d7.jpeg', 'jpeg'),
(39, 'e81b30f54b3b0506ebca894631d4339b.jpeg', 'img/products/e81b30f54b3b0506ebca894631d4339b.jpeg', 'jpeg'),
(40, 'fdbc7b0942ee4d32efb4b128d89c83ea.jpeg', 'img/products/fdbc7b0942ee4d32efb4b128d89c83ea.jpeg', 'jpeg'),
(41, 'cc209a140d26763ddeb7f96e793d8b19.jpeg', 'img/products/cc209a140d26763ddeb7f96e793d8b19.jpeg', 'jpeg'),
(43, '305b0f3112d066288322767805384026.jpeg', 'img/products/305b0f3112d066288322767805384026.jpeg', 'jpeg'),
(44, '816c0deeb3b52a2d7d82185e205a450f.jpeg', 'img/products/816c0deeb3b52a2d7d82185e205a450f.jpeg', 'jpeg'),
(45, '1fb47aca8c2ce86805b66ce4f69ffea4.jpeg', 'img/products/1fb47aca8c2ce86805b66ce4f69ffea4.jpeg', 'jpeg'),
(46, 'f8973c1ce92877c70540e966932fe2f6.jpeg', 'img/products/f8973c1ce92877c70540e966932fe2f6.jpeg', 'jpeg'),
(47, 'b065c7990f65eba7423c9ea6f1a353a2.jpeg', 'img/products/b065c7990f65eba7423c9ea6f1a353a2.jpeg', 'jpeg'),
(48, 'dcd74fd2577339cdba0a56a0ea7df540.jpeg', 'img/products/dcd74fd2577339cdba0a56a0ea7df540.jpeg', 'jpeg'),
(49, 'd1d2ade3a942ea6a5af1d6f291ad7ecd.jpeg', 'img/products/d1d2ade3a942ea6a5af1d6f291ad7ecd.jpeg', 'jpeg'),
(50, '1b2817a316ea3c21296a6943a104e8f6.jpeg', 'img/products/1b2817a316ea3c21296a6943a104e8f6.jpeg', 'jpeg'),
(51, '5085f5ab90f335425d5b722aa5180ef7.jpeg', 'img/products/5085f5ab90f335425d5b722aa5180ef7.jpeg', 'jpeg'),
(52, 'c885c46ae707b37713ae8489ed751ce5.jpeg', 'img/products/c885c46ae707b37713ae8489ed751ce5.jpeg', 'jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `kay_product_save`
--

CREATE TABLE IF NOT EXISTS `kay_product_save` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subCategory_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B87780E93DA5256D` (`image_id`),
  KEY `IDX_B87780E912469DE2` (`category_id`),
  KEY `IDX_B87780E9DB5A7180` (`subCategory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=46 ;

--
-- Dumping data for table `kay_product_save`
--

INSERT INTO `kay_product_save` (`id`, `category_id`, `name`, `description`, `link`, `subCategory_id`, `image_id`) VALUES
(1, 1, 'Baguette Tolbiac', 'Description', '', 2, 1),
(2, 1, 'Boule de Meule', 'Description', NULL, 2, 2),
(3, 1, 'Pavé aux cérales', 'Description', NULL, 3, 3),
(4, 1, 'Pavé au sarrasin', 'Description', NULL, 3, 4),
(5, 1, 'Grand pain au curcuma et noisettes', 'Description', NULL, 2, 5),
(6, 1, 'Croissant', 'Description', NULL, 3, 6),
(7, 2, 'Pain au chocolat', 'Description', NULL, 1, 7),
(8, 2, 'Tarte Monge', 'Desc', NULL, 5, 9),
(9, 1, 'Baguette Monge', 'baguette monge', NULL, 1, 15),
(10, 1, 'BAGUETTE MALESHERBES OU TOLBIAC', 'BAGUETTE MALESHERBES OU TOLBIAC', NULL, 1, 16),
(11, 1, 'GRAND RUSTIQUE', 'GRAND RUSTIQUE', NULL, 1, 17),
(12, 1, 'TOURTE DE MEULE', 'TOURTE DE MEULE', NULL, 1, 18),
(13, 1, 'PAIN AUX CÉRÉALES', 'PAIN AUX CÉRÉALES', NULL, 3, 19),
(14, 1, 'PAIN AUX CÉRÉALES', 'PAIN AUX CÉRÉALES', NULL, 3, 20),
(15, 1, 'LE CARRÉ', 'LE CARRÉ', NULL, 3, 21),
(16, 1, 'PAIN AUX NOIX', 'PAIN AUX NOIX', NULL, 1, 22),
(17, 1, 'PAIN AU CURCUMA', 'PAIN AU CURCUMA', NULL, 3, 23),
(18, 1, 'PAIN AUX OLIVES', 'PAIN AUX OLIVES', NULL, 3, 24),
(19, 1, 'PAIN AUX FIGUES', 'PAIN AUX FIGUES', NULL, 3, 25),
(20, 1, 'PAIN AU FROMAGE', 'PAIN AU FROMAGE', NULL, 3, 26),
(21, 1, 'PAIN COMPLET', 'PAIN COMPLET', NULL, 3, 27),
(22, 1, 'PAIN DE SEIGLE', 'PAIN DE SEIGLE', NULL, 3, 28),
(23, 1, 'CIABATTA', 'CIABATTA', NULL, 3, 29),
(24, 1, 'PAIN DE MIE', 'PAIN DE MIE', NULL, 3, 30),
(25, 1, 'PAIN GONESSE SEMI-COMPLET', 'PAIN GONESSE SEMI-COMPLET', NULL, 3, 31),
(26, 1, 'PAIN POIS-CHICHE', 'PAIN POIS-CHICHE', NULL, 3, 32),
(27, 1, 'PAIN AUX MENDIANTS', 'PAIN AUX MENDIANTS', NULL, 3, 33),
(28, 1, 'PAIN BÉNIT AUX CÉRÉALES', 'PAIN BÉNIT AUX CÉRÉALES', NULL, 3, 34),
(29, 1, 'PAIN AU CHOCOLAT', 'PAIN AU CHOCOLAT', NULL, 3, 35),
(30, 1, 'PAIN AUX RAISINS', 'PAIN AUX RAISINS', NULL, 3, 36),
(31, 1, 'CHAUSSON AUX POMMES', 'CHAUSSON AUX POMMES', NULL, 2, 37),
(32, 1, 'CROISSANT AUX AMANDES', 'CROISSANT AUX AMANDES', NULL, 2, 38),
(33, 1, 'PAIN AU CHOCOLAT & AMANDES', 'PAIN AU CHOCOLAT & AMANDES', NULL, 2, 39),
(34, 1, 'BRIOCHE MOUSSELINE', 'BRIOCHE MOUSSELINE', NULL, 2, 40),
(35, 1, 'BRIOCHE AUX PRALINES ROSES', 'BRIOCHE AUX PRALINES ROSES', NULL, 2, 41),
(36, 1, 'BRIOCHE AU CHOCOLAT BLANC', 'Description', NULL, 2, 43),
(37, 1, 'KOUGLOFF', 'Description', NULL, 2, 44),
(38, 1, 'CROISSANT', 'Description', NULL, 4, 45),
(39, 1, 'PAIN AU CHOCOLAT', 'Description', NULL, 4, 46),
(40, 1, 'PAIN AUX RAISINS', 'Description', NULL, 4, 47),
(41, 1, 'CHAUSSON AUX POMMES', 'Description', NULL, 4, 48),
(42, 1, 'CROISSANT AUX AMANDES', 'Description', NULL, 4, 49),
(43, 1, 'PAIN AU CHOCOLAT & AMANDES', 'Description', NULL, 4, 50),
(44, 1, 'BRIOCHE MOUSSELINE', 'Description', NULL, 4, 51),
(45, 1, 'BRIOCHE AUX PRALINES ROSES', 'Description', NULL, 4, 52);

-- --------------------------------------------------------

--
-- Table structure for table `kay_product_subcategory`
--

CREATE TABLE IF NOT EXISTS `kay_product_subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `kay_product_subcategory`
--

INSERT INTO `kay_product_subcategory` (`id`, `name`) VALUES
(1, 'all'),
(2, 'gluten'),
(3, 'levain'),
(4, 'classiques'),
(5, 'signatures');

-- --------------------------------------------------------

--
-- Table structure for table `kay_videos`
--

CREATE TABLE IF NOT EXISTS `kay_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7F7FD4623DA5256D` (`image_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kay_videos`
--

INSERT INTO `kay_videos` (`id`, `image_id`, `title`, `description`, `link`) VALUES
(1, 1, 'Ma vidéo', 'pppp', 'youtube');

-- --------------------------------------------------------

--
-- Table structure for table `kay_video_image`
--

CREATE TABLE IF NOT EXISTS `kay_video_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fileExtension` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kay_video_image`
--

INSERT INTO `kay_video_image` (`id`, `name`, `url`, `fileExtension`) VALUES
(1, '9a4cb341a1775ad9e780975cf74c9967.jpeg', 'img/videos/9a4cb341a1775ad9e780975cf74c9967.jpeg', 'jpeg');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kay_block`
--
ALTER TABLE `kay_block`
  ADD CONSTRAINT `FK_E56FE4173DA5256D` FOREIGN KEY (`image_id`) REFERENCES `kay_blockimage` (`id`);

--
-- Constraints for table `kay_product`
--
ALTER TABLE `kay_product`
  ADD CONSTRAINT `FK_B87780E912469DE2` FOREIGN KEY (`category_id`) REFERENCES `kay_product_category` (`id`),
  ADD CONSTRAINT `FK_B87780E93DA5256D` FOREIGN KEY (`image_id`) REFERENCES `kay_product_image` (`id`),
  ADD CONSTRAINT `FK_B87780E9DB5A7180` FOREIGN KEY (`subCategory_id`) REFERENCES `kay_product_subcategory` (`id`);

--
-- Constraints for table `kay_videos`
--
ALTER TABLE `kay_videos`
  ADD CONSTRAINT `FK_7F7FD4623DA5256D` FOREIGN KEY (`image_id`) REFERENCES `kay_video_image` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
