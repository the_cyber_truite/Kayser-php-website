<?php

namespace Kayser\PlatformBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * JpProduct
 *
 * @ORM\Table(name="jp_product")
 * @ORM\Entity(repositoryClass="Kayser\PlatformBundle\Entity\JpProductRepository")
 */
class JpProduct
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    protected $link;

    /**
    * @ORM\ManyToOne(targetEntity="Kayser\PlatformBundle\Entity\Category")
    * @ORM\JoinColumn(nullable=false)
    */
    protected $category;

    /**
    * @ORM\ManyToOne(targetEntity="Kayser\PlatformBundle\Entity\SubCategory")
    * @ORM\JoinColumn(nullable=false)
    */
    protected $subCategory;

    /**
    *
    * @ORM\OneToOne(targetEntity="Kayser\PlatformBundle\Entity\ProductImage", cascade={"persist", "remove"})
    * @ORM\JoinColumn(nullable=false)
    */
    protected $image;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Product
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set category
     *
     * @param \Kayser\PlatformBundle\Entity\Category $category
     * @return Product
     */
    public function setCategory(\Kayser\PlatformBundle\Entity\Category $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Kayser\PlatformBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set subCategory
     *
     * @param \Kayser\PlatformBundle\Entity\SubCategory $subCategory
     * @return Product
     */
    public function setSubCategory(\Kayser\PlatformBundle\Entity\SubCategory $subCategory)
    {
        $this->subCategory = $subCategory;

        return $this;
    }

    /**
     * Get subCategory
     *
     * @return \Kayser\PlatformBundle\Entity\SubCategory 
     */
    public function getSubCategory()
    {
        return $this->subCategory;
    }

    /**
     * Set image
     *
     * @param \Kayser\PlatformBundle\Entity\ProductImage $image
     * @return Product
     */
    public function setImage(\Kayser\PlatformBundle\Entity\ProductImage $image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Kayser\PlatformBundle\Entity\ProductImage 
     */
    public function getImage()
    {
        return $this->image;
    }
}
