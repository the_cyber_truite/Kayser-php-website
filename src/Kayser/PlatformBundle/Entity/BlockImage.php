<?php

namespace Kayser\PlatformBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * BlockImage
 *
 * @ORM\Table(name="kay_blockimage")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class BlockImage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    protected $url;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    protected $description;

    protected $file;

    protected $tempFilename;

    /**
     * @var string
     *
     * @ORM\Column(name="fileExtension", type="string", length=255, nullable=true)
     */
    protected $fileExtension;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BlockImage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return BlockImage
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return BlockImage
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set fileExtension
     *
     * @param string $fileExtension
     * @return BlockImage
     */
    public function setFileExtension($fileExtension)
    {
        $this->fileExtension = $fileExtension;

        return $this;
    }

    /**
     * Get fileExtension
     *
     * @return string 
     */
    public function getFileExtension()
    {
        return $this->fileExtension;
    }

    // On modifie le setter de file pour prendre en compte l'upload d'un fichier lorsqu'il en existe déjà un autre

    /**
     * Set file
     *
     * @param string $file
     * @return BlockImage
     */
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;

        // On vérifie si il y avait déjà un fichier pour cette entité
        if (null !== $this->url)
        {
            // On sauvegarde l'extension du fichier pour le supprimer plus tard
            $this->tempFilename = $this->url;

            // On réinitialise url et alt
            $this->url = null;
            $this->alt = null;
        }
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Get tempFilename
     *
     * @return string 
     */
    public function getTempFilename()
    {
        return $this->tempFilename;
    }

    /**
     * Set tempFilename
     *
     * @param string $tempFilename
     * @return BlockImage
     */
    public function setTempFilename($tempFilename)
    {
        $this->tempFilename = $tempFilename;

        return $this;
    }

    /**
    * @ORM\PrePersist()
    * @ORM\PreUpdate()
    */
    public function preUpload()
    {
        // On rend le champ du formulaire facultatif
        if (null === $this->file)
        {
            return;
        }

        // $this->name = $this->id.'.'.$this->file->guessExtension();
        $this->name = $this->uniqueName().'.'.$this->file->guessExtension();
        $this->url = $this->getWebPath();
        $this->fileExtension = $this->file->guessExtension(); 
    }

    /**
    * @ORM\PostPersist()
    * @ORM\PostUpdate()
    */
    public function upload()
    {
        // On rend le champ du formulaire facultatif
        if (null === $this->file)
        {
            return;
        }

        // Si on avait un ancien fichier, on le supprime
        if (null !== $this->tempFilename)
        {
            $oldFile = $this->getUploadRootDir().'/'.$this->getName();
            if (file_exists($oldFile))
            {
                unlink($oldFile);
            }
        }

        // On déplace le fichier
        $this->file->move($this->getUploadRootDir(), $this->getName());
    }

    /**
    * @ORM\PreRemove()
    */
    public function preRemoveUpload()
    {
        // On sauvegarde temporairement le nom du fichier car il dépend de l'id
        $this->tempFilename = $this->getUploadRootDir().'/'.$this->getName();
    }

    /**
    * @ORM\PostRemove()
    */
    public function removeUpload()
    {
        // En post remove on n'a pas accès à l'id donc on utilise le nom sauvegardé plus haut
        if (file_exists($this->tempFilename))
        {
            unlink($this->tempFilename);
        }
    }


    public function getUploadDir()
    {
        return 'img/blocks';
    }

    public function getUploadRootDir()
    {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    public function getWebPath()
    {
        return $this->getUploadDir().'/'.$this->getName();
    }

    public function uniqueName()
    {
        return md5(uniqid(time()));
    }
}
