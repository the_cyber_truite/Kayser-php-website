<?php

namespace Kayser\PlatformBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Locations
 *
 * @ORM\Table(name="kay_locations")
 * @ORM\Entity(repositoryClass="Kayser\PlatformBundle\Entity\Repository\LocationsRepository")
 */
class Locations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * @var integer
     *
     * @ORM\Column(name="zip_code", type="integer")
     */
    private $zipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=50)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="opening", type="string", length=124)
     */
    private $opening;

    /**
     * @var string
     *
     * @ORM\Column(name="day_off", type="string", length=50)
     */
    private $dayOff;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=52)
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="coordsy", type="string", length=20)
     */
    private $coordsy;

    /**
     * @var string
     *
     * @ORM\Column(name="coordsx", type="string", length=20)
     */
    private $coordsx;

    /**
     * @var string
     *
     * @ORM\Column(name="sans_gluten", type="string", length=1)
     */
    private $sansGluten;

    /**
     * @var string
     *
     * @ORM\Column(name="restaurant", type="string", length=1)
     */
    private $restaurant;

    /**
     * @var string
     *
     * @ORM\Column(name="cafe", type="string", length=1)
     */
    private $cafe;

    /**
     * @var string
     *
     * @ORM\Column(name="upcomming", type="string", length=1)
     */
    private $upcomming;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=50)
     */
    private $location;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Locations
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return Locations
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string 
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Locations
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set zipCode
     *
     * @param integer $zipCode
     * @return Locations
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return integer 
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Locations
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Locations
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set opening
     *
     * @param string $opening
     * @return Locations
     */
    public function setOpening($opening)
    {
        $this->opening = $opening;

        return $this;
    }

    /**
     * Get opening
     *
     * @return string 
     */
    public function getOpening()
    {
        return $this->opening;
    }

    /**
     * Set dayOff
     *
     * @param string $dayOff
     * @return Locations
     */
    public function setDayOff($dayOff)
    {
        $this->dayOff = $dayOff;

        return $this;
    }

    /**
     * Get dayOff
     *
     * @return string 
     */
    public function getDayOff()
    {
        return $this->dayOff;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     * @return Locations
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string 
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Locations
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set coordsy
     *
     * @param string $coordsy
     * @return Locations
     */
    public function setCoordsy($coordsy)
    {
        $this->coordsy = $coordsy;

        return $this;
    }

    /**
     * Get coordsy
     *
     * @return string 
     */
    public function getCoordsy()
    {
        return $this->coordsy;
    }

    /**
     * Set coordsx
     *
     * @param string $coordsx
     * @return Locations
     */
    public function setCoordsx($coordsx)
    {
        $this->coordsx = $coordsx;

        return $this;
    }

    /**
     * Get coordsx
     *
     * @return string 
     */
    public function getCoordsx()
    {
        return $this->coordsx;
    }

    /**
     * Set sansGluten
     *
     * @param string $sansGluten
     * @return Locations
     */
    public function setSansGluten($sansGluten)
    {
        $this->sansGluten = $sansGluten;

        return $this;
    }

    /**
     * Get sansGluten
     *
     * @return string 
     */
    public function getSansGluten()
    {
        return $this->sansGluten;
    }

    /**
     * Set restaurant
     *
     * @param string $restaurant
     * @return Locations
     */
    public function setRestaurant($restaurant)
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    /**
     * Get restaurant
     *
     * @return string 
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }

    /**
     * Set cafe
     *
     * @param string $cafe
     * @return Locations
     */
    public function setCafe($cafe)
    {
        $this->cafe = $cafe;

        return $this;
    }

    /**
     * Get cafe
     *
     * @return string 
     */
    public function getCafe()
    {
        return $this->cafe;
    }

    /**
     * Set upcomming
     *
     * @param string $upcomming
     * @return Locations
     */
    public function setUpcomming($upcomming)
    {
        $this->upcomming = $upcomming;

        return $this;
    }

    /**
     * Get upcomming
     *
     * @return string 
     */
    public function getUpcomming()
    {
        return $this->upcomming;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return Locations
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }
}
