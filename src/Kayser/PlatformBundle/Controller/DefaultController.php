<?php

namespace Kayser\PlatformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

use Kayser\PlatformBundle\Entity\Category;
use Kayser\PlatformBundle\Entity\SubCategory;
use Kayser\PlatformBundle\Resources\Classes\TwitterTimeline;

use Kayser\PlatformBundle\Entity\Contact;
use Kayser\PlatformBundle\Form\Type\ContactType;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction() 
    {
        $em = $this->getDoctrine()->getManager();
        $language = $this->get('request')->getLocale();
        $rootUrl = $this->get('request')->getBasePath();

        if ($language == 'fr')
            $repo = $em->getRepository('KayserPlatformBundle:Block');
        else if ($language == 'en')
            $repo = $em->getRepository('KayserPlatformBundle:EngBlock');
        else if ($language == 'es')
            $repo = $em->getRepository('KayserPlatformBundle:EsBlock');
        else if ($language == 'jp')
            $repo = $em->getRepository('KayserPlatformBundle:JpBlock');

        $listeBlocs = $repo->findAll();
        $blocFeatureHome = $repo->findOneById(1);
        $blocHomeOne = $repo->findOneById(3);
        $blocHomeTwo = $repo->findOneById(2);
        $blocHomeThree = $repo->findOneById(4);  

        $tweets = new TwitterTimeline('timeline', 'maison_ek');
        return $this->render('KayserPlatformBundle:Default:index.html.twig', array(
            'blocs' => $listeBlocs,
            'blocFeatureHome'   => $blocFeatureHome,
            'blocHomeOne'       => $blocHomeOne,
            'blocHomeTwo'       => $blocHomeTwo,
            'blocHomeThree'     => $blocHomeThree,
            'rootUrl'           => $rootUrl,
        )); 
    }

    public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();
 
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
 
        return $this->render('KayserPlatformBundle:Default:login.html.twig', array(
            'last_username' => $session->get(SecurityContext::LAST_USERNAME),
            'error'         => $error,
        ));
    }

    public function savoirfaireAction()
    {
        $em = $this->getDoctrine()->getManager();     
        $language = $this->get('request')->getLocale();
        $rootUrl = $this->get('request')->getBasePath();

        if ($language == 'fr')
            $repo = $em->getRepository('KayserPlatformBundle:Block');
        else if ($language == 'en')
            $repo = $em->getRepository('KayserPlatformBundle:EngBlock');
        else if ($language == 'es')
            $repo = $em->getRepository('KayserPlatformBundle:EsBlock');
        else if ($language == 'jp')
            $repo = $em->getRepository('KayserPlatformBundle:JpBlock');

        $listeBlocs = $repo->findAll();
        $sfFeature = $repo->findOneById(5);
        $sfUn = $repo->findOneById(6);
        $sfDeux = $repo->findOneById(7);
        $sfTrois = $repo->findOneById(8);

        return $this->render('KayserPlatformBundle:Default:savoir-faire.html.twig', array(
            'blocs' => $listeBlocs,
            'sfFeature' => $sfFeature,
            'sfUn' => $sfUn,
            'sfDeux' => $sfDeux,
            'sfTrois' => $sfTrois,
            'rootUrl' => $rootUrl
        ));
    }

    public function histoireAction()
    {
        $em = $this->getDoctrine()->getManager();     
        
        $rootUrl = $this->get('request')->getBasePath();
        $language = $this->get('request')->getLocale();
        $repo = $em->getRepository('KayserPlatformBundle:Block');           

        if ($language == 'fr')
        {
            $repo = $em->getRepository('KayserPlatformBundle:Block');           
            $HistRepo = $em->getrepository('KayserPlatformBundle:Histoire');
        } 
        else if ($language == 'en')
        {
            $repo = $em->getRepository('KayserPlatformBundle:EngBlock');           
            $HistRepo = $em->getrepository('KayserPlatformBundle:EngHistoire');
        }
        else if ($language == 'es')
        {
            $repo = $em->getRepository('KayserPlatformBundle:EsBlock');           
            $HistRepo = $em->getrepository('KayserPlatformBundle:EsHistoire');
        }
        else if ($language == 'jp')
        {
            $repo = $em->getRepository('KayserPlatformBundle:JpBlock');           
            $HistRepo = $em->getrepository('KayserPlatformBundle:JpHistoire');
        }

        for ($i = 0, $count = 0; $count != 16; $count++)
        {
            $tableHistoire[$i] = $HistRepo->findOneById($count);
            $i++;
        }
            
        $listeBlocs = $repo->findAll();
        $sfFeature = $repo->findOneById(5);
        $sfUn = $repo->findOneById(6);
        $sfDeux = $repo->findOneById(7);
        $sfTrois = $repo->findOneById(8);

        return $this->render('KayserPlatformBundle:Default:histoire.html.twig', array(
            'blocs' => $listeBlocs,
            'sfFeature' => $sfFeature,
            'sfUn' => $sfUn,
            'sfDeux' => $sfDeux,
            'sfTrois' => $sfTrois,
            'rootUrl' => $rootUrl,
            'tableHistoire' => $tableHistoire
        ));
    }

    public function philosophieAction()
    {
        $em = $this->getDoctrine()->getManager();
        $rootUrl = $this->get('request')->getBasePath();
        $language = $this->get('request')->getLocale();

        if ($language == 'fr')
        {
            $repo = $em->getRepository('KayserPlatformBundle:Block');
            $PhiloRepo = $em->getRepository('KayserPlatformBundle:Philosophie');
        }
        else if ($language == 'en')
        {
            $repo = $em->getRepository('KayserPlatformBundle:EngBlock');
            $PhiloRepo = $em->getRepository('KayserPlatformBundle:EngPhilosophie');
        }
        else if ($language == 'es')
        {
            $repo = $em->getRepository('KayserPlatformBundle:EsBlock');
            $PhiloRepo = $em->getRepository('KayserPlatformBundle:EsPhilosophie');
        }
        else if ($language == 'jp')
        {
            $repo = $em->getRepository('KayserPlatformBundle:JpBlock');
            $PhiloRepo = $em->getRepository('KayserPlatformBundle:JpPhilosophie');
        }

        for ($i = 0, $count = 0; $count != 11; $count++)
        {
            $tablePhilo[$i] = $PhiloRepo->findOneById($count);
            $i++;
        }

        $listeBlocs = $repo->findAll();
        $sfFeature = $repo->findOneById(5);
        $sfUn = $repo->findOneById(6);
        $sfDeux = $repo->findOneById(7);
        $sfTrois = $repo->findOneById(8);

        //var_dump($tablePhilo); die();
        return $this->render('KayserPlatformBundle:Default:philosophie.html.twig', array(
            'blocs' => $listeBlocs,
            'sfFeature' => $sfFeature,
            'sfUn' => $sfUn,
            'sfDeux' => $sfDeux,
            'sfTrois' => $sfTrois,
            'rootUrl' => $rootUrl,
            'tablePhilo' => $tablePhilo
        ));
    }

    public function methodesAction()
    {
        $em = $this->getDoctrine()->getManager();

        $rootUrl = $this->get('request')->getBasePath();
        $language = $this->get('request')->getLocale();

        if ($language == 'fr')
        {
            $MethRepo = $em->getRepository('KayserPlatformBundle:Methodes');
            $repo = $em->getRepository('KayserPlatformBundle:Block');
        }
        else if ($language == 'en')
        {
            $MethRepo = $em->getRepository('KayserPlatformBundle:EngMethodes');
            $repo = $em->getRepository('KayserPlatformBundle:EngBlock');
        }
        else if ($language == 'es')
        {
            $MethRepo = $em->getRepository('KayserPlatformBundle:EsMethodes');
            $repo = $em->getRepository('KayserPlatformBundle:EsBlock');
        }
        else if ($language == 'jp')
        {
            $MethRepo = $em->getRepository('KayserPlatformBundle:JpMethodes');
            $repo = $em->getRepository('KayserPlatformBundle:JpBlock');
        }

        for ($i = 0, $count = 0; $count != 15; $count++)
        {
            $tableMethodes[$i] = $MethRepo->findOneById($count);
            $i++;
        }

        $listeBlocs = $repo->findAll();
        $sfFeature = $repo->findOneById(5);
        $sfUn = $repo->findOneById(6);
        $sfDeux = $repo->findOneById(7);
        $sfTrois = $repo->findOneById(8);

        //var_dump($tableMethodes); die();
        return $this->render('KayserPlatformBundle:Default:methodes.html.twig', array(
            'blocs' => $listeBlocs,
            'sfFeature' => $sfFeature,
            'sfUn' => $sfUn,
            'sfDeux' => $sfDeux,
            'sfTrois' => $sfTrois,
            'rootUrl' => $rootUrl,
            'tableMethodes' => $tableMethodes
        ));
    }

    public function equipesAction()
    {
        $em = $this->getDoctrine()->getManager();

        $rootUrl = $this->get('request')->getBasePath();
        $language = $this->get('request')->getLocale();

        if ($language == 'fr')
        { 
            $EquipesRepo = $em->getRepository('KayserPlatformBundle:Equipes');
            $repo = $em->getRepository('KayserPlatformBundle:Block');
        }
        else if ($language == 'en')
        {
            $EquipesRepo = $em->getRepository('KayserPlatformBundle:EngEquipes');
            $repo = $em->getRepository('KayserPlatformBundle:EngBlock');
        }
        else if ($language == 'es')
        {
            $EquipesRepo = $em->getRepository('KayserPlatformBundle:EsEquipes');
            $repo = $em->getRepository('KayserPlatformBundle:EsBlock');
        }
        else if ($language == 'jp')
        {
            $EquipesRepo = $em->getRepository('KayserPlatformBundle:JpEquipes');
            $repo = $em->getRepository('KayserPlatformBundle:JpBlock');
        }

        for ($i = 0, $count = 0; $count != 7; $count++)
        {
            $tableEquipes[$i] = $EquipesRepo->findOneById($count);
            $i++;
        }

        $listeBlocs = $repo->findAll();
        $sfFeature = $repo->findOneById(5);
        $sfUn = $repo->findOneById(6);
        $sfDeux = $repo->findOneById(7);
        $sfTrois = $repo->findOneById(8);

        //var_dump($tableEquipes); die();
        return $this->render('KayserPlatformBundle:Default:equipes.html.twig', array(
            'blocs' => $listeBlocs,
            'sfFeature' => $sfFeature,
            'sfUn' => $sfUn,
            'sfDeux' => $sfDeux,
            'sfTrois' => $sfTrois,
            'rootUrl' => $rootUrl,
            'tableEquipes' => $tableEquipes
        ));
    }

    public function produitsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('KayserPlatformBundle:Block');
        $language = $this->get('request')->getLocale();

        if ($language == 'fr')
            $repo = $em->getRepository('KayserPlatformBundle:Block');
        else if ($language == 'en')
            $repo = $em->getRepository('KayserPlatformBundle:EngBlock');
        else if ($language == 'es')
            $repo = $em->getRepository('KayserPlatformBundle:EsBlock');
        else if ($language == 'jp')
            $repo = $em->getRepository('KayserPlatformBundle:JpBlock');

        
        $listeBlocs = $repo->findAll();
        $sfFeature = $repo->findOneById(9);
        $sfUn = $repo->findOneById(10);
        $sfDeux = $repo->findOneById(11);
        $sfTrois = $repo->findOneById(12);

        $rootUrl = $this->get('request')->getBasePath();

        return $this->render('KayserPlatformBundle:Default:produits.html.twig', array(
            'blocs' => $listeBlocs,
            'sfFeature' => $sfFeature,
            'sfUn' => $sfUn,
            'sfDeux' => $sfDeux,
            'sfTrois' => $sfTrois,
            'rootUrl' => $rootUrl
        ));
    }

    // public function painsAction()
    // {
    //     $em = $this->getDoctrine()->getManager();
    //     $repo = $em->getRepository('KayserPlatformBundle:Product');

    //     $products = $repo->findAll();

    //     return $this->render('KayserPlatformBundle:Default:pains.html.twig', array(
    //         'products' => $products
    //     ));
    // }

    public function painslevainAction()
    {
        return $this->render('KayserPlatformBundle:Default:painslevain.html.twig');
    }

    public function painsglutenAction()
    {
        return $this->render('KayserPlatformBundle:Default:painsgluten.html.twig');
    }

    public function videosAction()
    {
        return $this->render('KayserPlatformBundle:Default:videos.html.twig');
    }

    public function trouverAction()
    {
        return $this->render('KayserPlatformBundle:Default:trouver.html.twig');
    }

    public function produitsListAction($category, $subcategory)
    {
        $em = $this->getDoctrine()->getManager();

        $catRepo = $em->getRepository('KayserPlatformBundle:Category');
        $categoryId = $catRepo->findOneByName($category);
        $subCatRepo = $em->getRepository('KayserPlatformBundle:SubCategory');
        $subcategoryId = $subCatRepo->findOneByName($subcategory);
        $language = $this->get('request')->getLocale();

        if ($language == 'fr')
            $repo = $em->getRepository('KayserPlatformBundle:Product');
        else if ($language == 'en')
            $repo = $em->getRepository('KayserPlatformBundle:EngProduct');
        else if ($language == 'es')
            $repo = $em->getRepository('KayserPlatformBundle:EsProduct');
        else if ($language == 'jp')
            $repo = $em->getRepository('KayserPlatformBundle:JpProduct');

        if ($subcategory == 'all')
        {
            $products = $repo->findAllMyBreads($categoryId);
        } else {
            $products = $repo->findMyBreads($categoryId, $subcategoryId);
        }

        return $this->render('KayserPlatformBundle:Default:pains.html.twig', array(
            'products' => $products,
            'category' => $category,
            'subcategory' => $subcategory
        ));
    }

    //var_dump($products); die();
    public function carteAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $language = $this->get('request')->getLocale();

        if ($language == 'fr')
        {
            $repoSales = $em->getRepository('KayserPlatformBundle:Sales');
            $repoPatisserie = $em->getRepository('KayserPlatformBundle:Patisserie');
        }
        else if ($language == 'en')
        {
            $repoSales = $em->getRepository('KayserPlatformBundle:EngSales');
            $repoPatisserie = $em->getRepository('KayserPlatformBundle:EngPatisserie');
        }
        else if ($language == 'es')
        {
            $repoSales = $em->getRepository('KayserPlatformBundle:EsSales');
            $repoPatisserie = $em->getRepository('KayserPlatformBundle:EsPatisserie');
        }
        else if ($language == 'jp')
        {
            $repoSales = $em->getRepository('KayserPlatformBundle:JpSales');
            $repoPatisserie = $em->getRepository('KayserPlatformBundle:JpPatisserie');
        }

        $productSale = $repoSales->findAll();
        $productPatisserie = $repoPatisserie->findAll();
        $catSales = 0;
        $catPatisserie = 0;

        foreach($productSale as $element)
        {
            if ($catSales < $element->getCategory())
            {
                $catSales = $element->getCategory();
            }
        }
        
        foreach($productPatisserie as $element)
        {
            if ($catPatisserie < $element->getCategory())
            {
                $catPatisserie = $element->getCategory();
            }
        }

        return $this->render('KayserPlatformBundle:Default:'.$slug.'.html.twig', array(
            'productSale' => $productSale,
            'catSales' => $catSales,
            'productPatisserie' => $productPatisserie,
            'catPatisserie' => $catPatisserie
            ));

    }

    public function contactAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $language = $this->get('request')->getLocale();
        $slug = $request->get('slug', null);

        if ($language == 'fr')
        {
            $repoContact = $em->getRepository('KayserPlatformBundle:ContactText');
            $repoHotel = $em->getRepository('KayserPlatformBundle:HotelRestaurant');
            $repoEntreprise = $em->getRepository('KayserPlatformBundle:Entreprise');
            $homme = 'Monsieur'; $femme = 'Madame';
        }
        else if ($language =='en')
        {
            $repoContact = $em->getRepository('KayserPlatformBundle:EngContactText');
            $repoHotel = $em->getRepository('KayserPlatformBundle:EngHotelRestaurant');
            $repoEntreprise = $em->getRepository('KayserPlatformBundle:EngEntreprise');
            $homme = 'Mr'; $femme = 'Mrs';   
        }
        else if ($language == 'es')
        {
            $repoContact = $em->getRepository('KayserPlatformBundle:EsContactText');
            $repoHotel = $em->getRepository('KayserPlatformBundle:EsHotelRestaurant');
            $repoEntreprise = $em->getRepository('KayserPlatformBundle:EsEntreprise');
            $homme = 'Hombre'; $femme = 'Mujer';   
        }
        else if ($language == 'jp')
        {
            $repoContact = $em->getRepository('KayserPlatformBundle:JpContactText');
            $repoHotel = $em->getRepository('KayserPlatformBundle:JpHotelRestaurant');
            $repoEntreprise = $em->getRepository('KayserPlatformBundle:JpEntreprise');
            $homme = '男'; $femme = '女性';   
        }

        for ($i = 0, $count = 3; $count != 6; $count++)
        {
            $tableEntreprise[$i] = $repoEntreprise->findOneById($count);
            $i++;
        }
        for ($i = 0, $count = 0; $count != 5; $count++)
        {
            $tableHotel[$i] = $repoHotel->findOneById($count);
            $i++;            
        }
        for ($i = 0, $count = 0; $count != 4; $count++)
        {
            $tableContact[$i] = $repoContact->findOneById($count);
            $i++;
        }

        if ($slug == 'hotels-restaurants') 
        {
            return $this->render('KayserPlatformBundle:Default:hotels-restaurants.html.twig', array('slug' => $slug,
                'tableHotel' => $tableHotel));
        }
        else if ($slug == 'entreprises') 
        {
            return $this->render('KayserPlatformBundle:Default:entreprises.html.twig' , array('slug' => $slug,
                'tableEntreprise' => $tableEntreprise));
        }
        else
        {
            if ($language == 'fr')
                $defaultData = array('message' => 'Tapez votre message ici...');
            else if ($language == 'en')
                $defaultData = array('message' => 'Type your message here...'); 
            else if ($language == 'es')
                $defaultData = array('message' => 'Escribe tu mensaje aquí...');                
            else if ($language == 'jp')
                $defaultData = array('message' => 'ここにメッセージを入力...');             
            $form = $this->createFormBuilder($defaultData)
                ->add('objet', 'text')
                ->add('nom', 'text')
                ->add('prenom', 'text')
                ->add('email', 'email')
                ->add('message', 'textarea')
                ->add('gender', 'choice', array(
                    'choices'   => array('monsieur' => $homme, 'madame' => $femme),
                    'expanded' => true))
                ->getForm();

            $form->handleRequest($request);

            if ($request->getMethod() === 'POST')
            {
                if ($form->isValid()) 
                {
                    $data = $form->getData();
                    $message = \Swift_Message::newInstance()
                        ->setSubject($data['objet'])
                        ->setFrom($data['email'])
                        ->setTo('contact@maison-kayser.com')
                        ->setBody($data['message']);

                    $this->get('mailer')->send($message);
                    if ($language == 'fr')
                        $this->get('session')->getFlashBag()->add('info', 'Votre message a été envoyé. Merci.');
                    else if ($language == 'en')
                        $this->get('session')->getFlashBag()->add('info', 'Your message has been sent. Thanks.');
                    else if ($language == 'es')
                        $this->get('session')->getFlashBag()->add('info', 'Su mensaje fue enviado. Gracias.');
                    else if ($language == 'jp')
                        $this->get('session')->getFlashBag()->add('info', 'あなたのメッセージが送信されました。ありがとう。.');
                    return $this->redirect($this->generateUrl('kay_contact'));
                }
            }
            return $this->render('KayserPlatformBundle:Default:contact.html.twig', array(
                'form' => $form->createView(),
                'tableContact' => $tableContact,
                'language' => $language
                ));
        }
    }

      public function upload()
      {
        if (null === $this->file) 
        {
          return;
        }

        $name = $this->file->getClientOriginalName();
        $this->file->move($this->getUploadRootDir(), $name);
        $this->url = $name;
        $this->alt = $name;
      }

      public function getUploadDir()
      {
        return 'cv';
      }

      protected function getUploadRootDir()
      {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
      }

    public function candidatureAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $language = $this->get('request')->getLocale();

        if ($language == 'fr')
        {
            $repoCandidature = $em->getRepository('KayserPlatformBundle:Candidature');
            $homme = 'Monsieur'; $femme = 'Madame';
            // $boulangerie = 'Boulangerie'; $patisserie = 'Patisserie'; $vente = 'Vente';
            // $cuisine = 'Cuisine'; $autres = 'Autres';
        }
        else if ($language =='en')
        {
            $repoCandidature = $em->getRepository('KayserPlatformBundle:EngCandidature');
            $homme = 'Mr'; $femme = 'Mrs';
            // $boulangerie = 'Bakery'; $patisserie = 'Pastry'; $vente = 'Sale';
            // $cuisine = 'Cookery'; $autres = 'Others';            
        }
        else if ($language == 'es')
        {
            $repoCandidature = $em->getRepository('KayserPlatformBundle:EsCandidature');
            $homme = 'Hombre'; $femme = 'Mujer';
            // $boulangerie = 'Panadería'; $patisserie = 'Pasteles'; $vente = 'Venta';
            // $cuisine = 'Cocina'; $autres = 'Otro';
        }
        else if ($language == 'jp')
        {
            $repoCandidature = $em->getRepository('KayserPlatformBundle:JpCandidature');
            $homme = '男'; $femme = '女性';
            // $boulangerie = 'ベーカリー'; $patisserie = 'ペストリー'; $vente = '販売';
            // $cuisine = '料理'; $autres = 'その他';
        }

        for ($i = 0, $count = 0; $count != 6; $count++)
        {
            $tableCandidature[$i] = $repoCandidature->findOneById($count);
            $i++;
        }

        $defaultData = array(); 
        $form = $this->createFormBuilder($defaultData)
            ->add('nom', 'text')
            ->add('prenom', 'text')
            ->add('telephone', 'number')
            ->add('email', 'email')
            ->add('adresse', 'text')
            ->add('cp', 'number')
            ->add('ville', 'text')
            ->add('cv', 'file')
            ->add('lettre', 'file')
            // ->add('objet', 'choice', array(
            //     'choices'   => array($boulangerie => $boulangerie, $patisserie => $patisserie, $vente => $vente, $cuisine => $cuisine, $autres => $autres),
            //     'expanded' => false
            // ))
            ->add('objet', 'text')
            ->add('message', 'textarea')
            ->add('gender', 'choice', array(
                'choices'   => array('monsieur' => $homme, 'madame' => $femme),
                'expanded' => true
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($request->getMethod() === 'POST')
        {
            if ($form->isValid()) 
            {
                try 
                {
                    $data = $form->getData();
                    $cvPath = $this->iUpload($data['cv']);
                    $letterPath = $this->iUpload($data['lettre']);
                    
                    $message = \Swift_Message::newInstance()
                        ->setSubject($data['objet'])
                        ->setFrom($data['email'])
                        ->setTo('recrutementkayser@gmail.com')
                        ->setBody($data['message'])
                        ->attach(\Swift_Attachment::fromPath($cvPath))
                        ->attach(\Swift_Attachment::fromPath($letterPath));
                    $this->get('mailer')->send($message);

                    if ($language == 'fr')
                        $this->get('session')->getFlashBag()->add('info', 'Votre message a bien été envoyé. Merci.');
                    else if ($language == 'en')
                        $this->get('session')->getFlashBag()->add('info', 'Your message has been sent. Thanks.');
                    else if ($language == 'en')
                        $this->get('session')->getFlashBag()->add('info', 'Su mensaje fue enviado. Gracias.');
                    else if ($language == 'jp')
                        $this->get('session')->getFlashBag()->add('info', 'メッセージは送信されました。ありがとう。');
                } 
                catch (Exception $e) 
                {
                    error_log($e->getMessage());     
                }

                return $this->redirect($this->generateUrl('kay_candidature'));
            }
        }

        return $this->render('KayserPlatformBundle:Default:candidature.html.twig', array(
            'form' => $form->createView(),
            'tableCandidature' => $tableCandidature,
            'language' => $language
        ));
    }

    // utilisé dans CandidatureAction
    private function iUpload($file)
    {
        $extension = $file->guessExtension();
        $nameOfFile = $this->iRename($file);

        if (!$extension) 
        {
            $extension = 'bin';
        }

        $file->move(__DIR__.'/../../../../upload/', $nameOfFile.'.'.$extension);

        return ('http://www.maison-kayser.com/upload/'.$nameOfFile.'.'.$extension);
    }

    // utilisé dans iUpload 
    private function iRename($file)
    {
        $name = $file->getClientOriginalName();
        $nameOfFile = " ";

        for ($i= 0, $j = 0; $i != strlen($name); $i++, $j++) 
        { 
            if ($name[$i] == '.')
                break;
            $nameOfFile[$j] = $name[$i];
        }

        $nameOfFile = $this->iClear($nameOfFile);

        return ($nameOfFile);
    }

    //utilisé dans iRename
    private function iClear($str) 
    {
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", '-', $clean);

        return $clean;
    }

    public function creditsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $language = $this->get('request')->getLocale();

        if ($language == 'fr')
            $repo = $em->getRepository('KayserPlatformBundle:Informations');
        else if ($language == 'en')
            $repo = $em->getRepository('KayserPlatformBundle:EngInformations');  
        else if ($language == 'es')
            $repo = $em->getRepository('KayserPlatformBundle:EsInformations');
        else if ($language == 'jp')
            $repo = $em->getRepository('KayserPlatformBundle:JpInformations');
                                    
        for ($i = 0, $count = 0; $count != 6; $count++)
        {
            $tableInfo[$i] = $repo->findOneById($count);
            $i++;
        }        
        return $this->render('KayserPlatformBundle:Default:informations-legales.html.twig', array(
            'tableInfo' => $tableInfo
            ));
    }

    public function faqAction()
    {
        $em = $this->getDoctrine()->getManager();
        $language = $this->get('request')->getLocale();

        if ($language == 'fr') 
            $repo = $em->getRepository('KayserPlatformBundle:FAQ');
        else if ($language == 'en')
           $repo = $em->getRepository('KayserPlatformBundle:EngFaq');
        else if ($language =='es')
            $repo = $em->getRepository('KayserPlatformBundle:EsFaq');
        else if ($language =='jp')
            $repo = $em->getRepository('KayserPlatformBundle:JpFaq');

        for ($count = 0, $i = 0; $count != 12; $count++)
        {
            $tableFaq[$i] = $repo->findOneById($count);
            $i++;
        }
        
        return $this->render('KayserPlatformBundle:Default:faq.html.twig', array(
                'tableFaq' => $tableFaq
            ));
    }


    public function annoncesAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $language = $this->get('request')->getLocale();

        if ($language == 'fr')
        {
            $repo = $em->getRepository('KayserPlatformBundle:Annonces');
            $repoAnnonce = $em->getRepository('KayserPlatformBundle:AnnoncesText');
        }
        else if ($language == 'en')
        {
            $repo = $em->getRepository('KayserPlatformBundle:EngAnnonces');
            $repoAnnonce = $em->getRepository('KayserPlatformBundle:EngAnnoncesText');
        }
        else if ($language == 'es')
        {
            $repo = $em->getRepository('KayserPlatformBundle:EsAnnonces');
            $repoAnnonce = $em->getRepository('KayserPlatformBundle:EsAnnoncesText');
        }
        else if ($language == 'jp')
        {
            $repo = $em->getRepository('KayserPlatformBundle:JpAnnonces');
            $repoAnnonce = $em->getRepository('KayserPlatformBundle:JpAnnoncesText');
        }

        $params =  array('annonces' => $repo->findAll(array('creationdate' => 'desc')));
        if(!empty($slug))
        {
            $annonce = $repo->findOneBySlug($slug);
            if ($annonce) 
            {
                $params['show'] = $annonce;
            }
        }
        $params['titleOne'] = $repoAnnonce->findOneById(1);
        $params['titleTwo'] = $repoAnnonce->findOneById(2);
        $params['titleTree'] = $repoAnnonce->findOneById(3);

        return $this->render('KayserPlatformBundle:Default:annonces.html.twig', $params);
    }

    public function storesAction(Request $request)
    {
        $features = $request->query->get('features', null);
        $where = array();
        if (!is_null($features)) {
            $ft = explode(',', $features);
            if(in_array('gluten', $ft)) $where['sansGluten'] = 'Y' ;
            if(in_array('restaurant', $ft)) $where['restaurant'] = 'Y' ;
            if(in_array('cafe', $ft)) $where['cafe'] = 'Y' ;
        }

        $location = $request->query->get('location', null);
        if($location){
            $where['location'] = $location;
        }

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('KayserPlatformBundle:Locations');

        $locations = array();
        foreach ($repo->findAll($where) as $loc) {
            $name= strtolower($loc->getName()) ;
            $name = (substr($name, 0, 2) == 'ek') ? ( 'EK '. ucfirst(trim(substr($name, 2)))) : ucfirst($name);
            $properties = array(
                  'uuid'   => $loc->getId() ,
                  'Shp_centre' => '',
                  'Fcilty_nam' => $name ,
                  'Street_add'   => $loc->getStreet() ,
                  'Postcode' => $loc->getZipCode() ,
                  'Locality' => $loc->getCity() ,
                  'Hrs_of_bus'  => $loc->getOpening() ,
                  'State'    => $loc->getState() ,
                  'Display_wd' => '1.5',
                  'Fcilty_typ_2'    => $loc->getEmail() ,
                  'day_off'  => $loc->getDayOff() ,
                  'Shp_num_an' => '' ,
                  'Fcilty_ctc' => $loc->getPhoneNumber() ,
                  'Country'  => $loc->getCountry() ,
                  'sans_gluten'  => $loc->getSansGluten() == 'Y' ? 'YES' : 'NO' ,
                  'restaurant'   => $loc->getRestaurant() == 'Y' ? 'YES' : 'NO' ,
                  'cafe' => $loc->getCafe() == 'Y' ? 'YES' : 'NO' ,
                );
            $geometry = array(
                'type' => 'Point',
                'coordinates' => array($loc->getCoordsx(), $loc->getCoordsy()),
            );
            $locations[] = array('geometry' => $geometry, 'properties' => $properties);
        }
        $data = array(
            'type'  => "FeatureCollection",
            'features' => $locations
        );

        $response = new Response();
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/json');
        $response->send();
        die();
    }
}