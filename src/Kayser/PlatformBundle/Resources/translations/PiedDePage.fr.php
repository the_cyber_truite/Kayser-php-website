<?php

return array(
    'rejoindre' => "Nous rejoindre",
    'contacter' => "Nous contacter",
    'presse'    => "Espace Presse",
    'client'   => "Clients Professionnels",
    'information' => "Informations légales",

    'candidature' => "Candidature spontanée",
    'annonce' => "Annonces",
    'information' => "Informations générales",
    'faq' => "FAQ",
    'texte_presse' => 'Si vous avez besoin d\'un dossier de presse, de photos, ou de toute autre information, nous vous invitons à contacter <a href="http://www.marysemasse.com/"> Maryse Masse </a>
Tél : 01 40 54 72 50',
    'hotel' => "Hôtels & Restaurants",
    'entreprise' => "Entreprises",
    'adresse' => 'Siège social : 19 rue Valette, 75005 Paris, France <a href="mailto:contact@maison-kayser.com">contact@maison-kayser.com</a><br/> RCS Créteil B415308568.<br /> Tél : 01 42 34 50 20. <br />Consultez nos Crédits',
    'adresse_suivante' => ' ici',
);

