<?php

return array(
    'rejoindre' => "Join Our Team",
    'contacter' => "Contact Us",
    'presse'    => "Press",
    'client'   => "Business Customers",
    'information' => "Legal Notices",

    'candidature' => "Apply Here",
    'annonce' => "Vacancies",
    'information' => "General Information",
    'faq' => "FAQ",
    'texte_presse' => ' Please contact <a href="http://www.marysemasse.com/">Maryse Masse</a> for press kits, photos or any additional information. <br/> Tel.: +33 (0)1 40 54 72 50',
    'hotel' => "Hotels and Restaurants",
    'entreprise' => "Enterprises",
    'adresse' => 'Headsquaters : 19 rue Valette, 75005 Paris, France <a href="mailto:contact@maison-kayser.com">contact@maison-kayser.com</a><br/> RCS Créteil B415308568. Tel.: +33 (0)1 40 54 72 50 . Check our Credits: ',
    'adresse_suivante' => 'here',
);