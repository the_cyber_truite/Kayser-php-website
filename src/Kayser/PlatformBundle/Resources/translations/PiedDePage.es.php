<?php

return array(
    'rejoindre' => "Trabaje con nosotros",
    'contacter' => "Contacto",
    'presse'    => "Espacio para la prensa",
    'client'   => "Clientes Profesionales",
    'information' => "Información legal",

    'candidature' => "Candidatura espontánea",
    'annonce' => "Espacio para anuncios",
    'information' => "Información general",
    'faq' => "Preguntas frecuentes",
    'texte_presse' => ' Si necesita un dossier de prensa, fotos o cualquier otra información,<br/> le invitamos a ponerse en contacto con <a href="http://www.marysemasse.com/">Maryse Masse</a>.<br/> Tel.: +33 (0)1 40 54 72 50',
    'hotel' => "Hoteles y Restaurantes",
    'entreprise' => "Negocios",
    'adresse' => 'Oficina registrada : 19 rue Valette, 75005 Paris, France <a href="mailto:contact@maison-kayser.com">contact@maison-kayser.com</a><br/> RCS Créteil B415308568. Tel.: +33 (0)1 40 54 72 50 . Check our Credits: ',
    'adresse_suivante' => 'aquí',
);