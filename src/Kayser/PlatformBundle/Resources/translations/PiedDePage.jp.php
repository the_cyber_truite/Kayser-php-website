<?php

return array(
    'rejoindre' => "アクセス",
    'contacter' => "店へのお問合せ",
    'presse'    => "プレス空間",
    'client'   => "業者のお客様",
    'information' => "法的情報",

    'candidature' => "採用情報",
    'annonce' => "広告空間",
    'information' => "般情報",
    'faq' => "FAQ",
    'texte_presse' => '報道書類や写真、あるいは他の情報が必要な場合には、マリーズ・マッスにご連絡下さい。<br/>Tel: 01 40 54 72 50',
    'hotel' => "ホテルやレストラン",
    'entreprise' => "事業",
    'adresse' => 'Headsquaters : 19 rue Valette, 75005 Paris, France <a href="mailto:contact@maison-kayser.com">contact@maison-kayser.com</a><br/> RCS Créteil B415308568. Tel.: +33 (0)1 40 54 72 50 . Check our Credits: ',
    'adresse_suivante' => 'ここに',
);