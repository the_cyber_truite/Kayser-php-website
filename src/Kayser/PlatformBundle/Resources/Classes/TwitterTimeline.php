<?php
namespace Kayser\PlatformBundle\Resources\Classes;

/* --------------------------------------------------------------------------------------------------- 
 * Custom Twitter API
 *
 * @author	Robin Bonnes <http://robinbonnes.nl/>
 * @version	1.5
 *
 * Copyright (C) 2013 Robin Bonnes. All rights reserved.
 * 
 * DESCRIPTION:
 * 
 * Due to deprecating Twitter API v1.0, developers need to create oAuth authentication to retrieve tweets.
 * This script does exactly the same, without the OAuth hazzle, so its much easier to use.
 * Its only less advanced as Twitter API v1.1. 
 * It will retrieve tweets (and retweets) with their avatar, username and post date in JSON format.
 * 
 * HOW TO USE:
 * 
 * Get tweets by username:
 * 
 * twitter_api.php?type=timeline&username=yourusername&count=5&retweets=true
 * 
 * - username	=	Twitter username to retrieve tweets from.
 * - count 		=	Number of tweets to retrieve. Default: 5.
 * - retweets	=	Boolean to enable/disable displaying retweets. Default: true.
 * 
 * Get tweets by search keyword:
 * 
 * twitter_api.php?type=search&q=yourkeyword&count=5
 * 
 * - q 		=	Search keyword to retrieve tweets from.
 * - count 	=	Number of tweets to retrieve. Default: 5.
 *
 * OUTPUT:
 * 
 * [{"username":"test","type":"tweet","avatar":"http://.../.png","date":"21 January 13","tweet":"Hello"},
 *  {"username":"test2","type":"retweet","avatar":"http://.../.png","date":"23 January 13","tweet":"Hello"}]
 *
 * CHANGELOG:
 *
 * v1.0	- Release
 * v1.1 - Search function added
 * v1.2 - Several bugfixes
 * v1.3 - Added hashtag search support, little bit optimized and several bugs fixed
 * v1.4 - Special characters fix
 * v1.5 - New layout compatible
 *
 * Note: PHP extension CURL is required.
 * --------------------------------------------------------------------------------------------------- */

/*
 * Allow Cross Domain
 */
// header('Access-Control-Allow-Origin: *');
 
/*
 * Method
 */

Class TwitterTimeline
{
	public function __construct($type, $name, $count = 5, $retweets = true, $keyword = null)
	{
		if(isset($type) && ($type == 'timeline' || $type == 'search'))
		{
			$this->type = $type;
		}else
			return;

		/*
		 * Twitter Username
		 */
		if(isset($name) && !empty($name))
		{
			$this->name = $name;
		}
		else
		{
			return;
		}

		/*
		 * Number of tweets to retrieve. (max is 200)
		 */
		if(isset($count) && !empty($count))
		{
			if(is_numeric($count))
			{
				$this->count = (int) $count;
			}
			else
			{
				$this->count = 5;
			}
		}
		else
		{
			$this->count = 5;
		}

		/*
		 * Boolean to retrieve retweets or not.
		 */
		if(is_string($retweets) && ($retweets == "0" || $retweets == "false"))
		{
			$this->retweets = false;
		}
		else
		{
			$this->retweets = $retweets;;
		}

		if(isset($keyword) && !empty($keyword))
		{
			$this->keyword = urlencode($keyword);
		}



	}
	
	function get()
	{
		/*
		 * Get the tweets using CURL.
		 */
		if($this->type == 'timeline')
		{
			$url = 'https://twitter.com/i/profiles/show/' . $this->name . '/timeline?count=5' ;
		}
		else 
		{
			$url = 'https://twitter.com/i/search/timeline?q=' . $this->keyword . '&count=' . $this->count;
		}

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_REFERER, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		$result = curl_exec($curl);
		curl_close($curl);

		/*
		 * Decode JSON Encoded string to DOM
		 */
		 
		if(empty($result))
		{
			die("Can't fetch data from Twitter server.");
		}
		
		$decoded	=	json_decode($result, true);
		$decoded	=	$decoded['items_html'];
		$decoded	=	utf8_decode($decoded);
		$decoded	=	trim($decoded);
		if(empty($decoded) || !$decoded)
		{
			if($this->type == 'timeline')
			{
				die("Username doesn't exist or doesn't have tweets yet.");
			}
			else
			{
				die("No results found for keyword: " . $this->keyword . ".");
			}
		}
		
		$domdoc		=	new \DOMDocument();
		libxml_use_internal_errors(TRUE);
		$domdoc		->	loadHTML($decoded);
	    //echo($decoded); die();

		$arr = array();
		$finder		=	new \DomXPath($domdoc);				// Find tweets in DOMDocument
		$tweets		=	$finder->query("//*[contains(concat(' ', normalize-space(@data-component-term), ' '), ' tweet ')]"); // Find query
		if($tweets->item(0) == null)
		{
			$tweets		=	$finder->query("//*[contains(concat(' ', normalize-space(@data-component-term), ' '), ' tweet ')]"); // Find query
		}

		if(!sizeof($tweets)) return $arr;

		for($i = 0; $i < $this->count; $i++)
		{
			$a = array();
			$skip = false;									// Boolean to check if item has to be skipped
			$tweet = $tweets->item($i);						// Get tweetdata
			
			// Extract tweet
			$newdomdoc 	=	new \DomDocument;
			$newdomdoc	->	loadHTML("<html></html>");
			$newdomdoc	->	documentElement->appendChild($newdomdoc->importNode($tweet,true));
			$finder		=	new \DomXpath($newdomdoc);

			// Check if retweets should be in result
			if($this->type == 'timeline' && $this->retweets == false)
			{
				$find	=	$finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), 'js-retweet-text')]");
				
				// If its an retweet, skip it
				if(isset($find->item(0)->nodeValue))
				{
					$skip = true;
				}
			}
			
			if(!$skip)
			{

				$first = false;
							
				// Extract username
				$find	=	$finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), 'fullname')]");
				$a['username'] =  htmlspecialchars($find->item(0)->nodeValue, ENT_QUOTES);
				
				// Determine Type
				$find	=	$finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), 'js-retweet-text')]");
				if(isset($find->item(0)->nodeValue)) {
					$a['type'] = "retweet";
				} else {
					$a['type'] = "tweet";
				}

				// Extract avatar
				$find	=	$finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), 'avatar')]");
				$a['avatar'] = htmlspecialchars($find->item(0)->getAttribute('src'), ENT_QUOTES);
				
				// Extract date
				$find	=	$finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), 'js-short-timestamp')]");
				$date = htmlspecialchars($find->item(0)->nodeValue, ENT_QUOTES);
				$a['date'] = str_replace("\r", "", $date);

				// Extract tweet
				$find	=	$finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), 'js-tweet-text')]");
				$fixed_tweet = fix_tweet($newdomdoc->saveXML($find->item(0)), $this->name);
				$a['tweet'] = str_replace("\r", "", $fixed_tweet);

				$arr[] = $a;
			
			}
		}

		return $arr;								// Output
	}
		
	
	
	
}
	/*
	 * Helper Functions
	 */
	 
	/* Brings hastag and URL support */
	function fix_tweet($temptweet, $name)
	{
		$stripped_elements = array();
		$simplexml = simplexml_load_string('<root>'. str_replace('"',"'", $temptweet) .'</root>', 'SimpleXMLElement', LIBXML_NOERROR | LIBXML_NOXMLDECL);

		if($simplexml)
		{
			// Elements in tweet
			foreach($simplexml->xpath('descendant::*[@*]') as $tag)
			{
				// Attributes in elements
				foreach($tag->attributes() as $name => $value)
				{
					if($name != "href")
					{
						// Strip attribute
						$tag->attributes()->$name = '';
						$stripped_elements[$name] = '/ '. $name .'=""/';
					}
					else
					{
						// Fix link
						$first_char = substr($tag->attributes()->$name, 0, 1);
						if($first_char == "/")
						{
							$temp_val = str_replace("#", "%23", $value);
							$tag->attributes()->$name = "https://twitter.com" . $temp_val;
						}
					}
				}
			}
			
			return str_replace('"',"'", strip_tags(preg_replace($stripped_elements, array(''), $simplexml->asXML()), '<a>'));
		}
		
		return "";
	}
