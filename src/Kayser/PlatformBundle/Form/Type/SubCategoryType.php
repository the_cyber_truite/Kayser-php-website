<?php
// src/Kayser/PlatformBundle/Form/Type/BlockType.php

namespace Kayser\PlatformBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SubCategoryType extends AbstractType
{
    public function  buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',  'text')
            // ->add('category', 'entity', array(
            //     'class' => 'KayserPlatformBundle:Category',
            //     'property' => 'name',
            //     'multiple' => false
            // ))
            ->add('save',   'submit')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kayser\PlatformBundle\Entity\SubCategory'
        ));
    }

    public function getName()
    {
        return 'add_subcat';
    }
}