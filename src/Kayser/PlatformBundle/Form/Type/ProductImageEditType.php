<?php
// src/Kayser/PlatformBundle/Form/Type/ProductImageEditType.php

namespace Kayser\PlatformBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductImageEditType extends ProductType
{
    public function  buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->remove('name')
            ->remove('description')
            ->remove('category')
            ->remove('subCategory')
            ->remove('link')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kayser\PlatformBundle\Entity\Product'
        ));
    }

    public function getName()
    {
        return 'product_image_edit';
    }
}