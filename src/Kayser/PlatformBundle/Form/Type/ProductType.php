<?php
// src/Kayser/PlatformBundle/Form/Type/ProductType.php

namespace Kayser\PlatformBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductType extends AbstractType
{
    public function  buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',  'text')
            ->add('description',  'textarea')
            ->add('category', 'entity', array(
                'class' => 'KayserPlatformBundle:Category',
                'property' => 'name',
                'multiple' => false
            ))
            ->add('subCategory', 'entity', array(
                'class' => 'KayserPlatformBundle:SubCategory',
                'property' => 'name',
                'multiple' => false
            ))
            ->add('link',  'text', array(
                'required' => false
            ))
            ->add('image',  new ProductImageType())
            ->add('save',   'submit')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kayser\PlatformBundle\Entity\Product'
        ));
    }

    public function getName()
    {
        return 'add_product';
    }
}