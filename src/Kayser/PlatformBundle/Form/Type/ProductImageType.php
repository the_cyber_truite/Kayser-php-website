<?php
// src/Kayser/PlatformBundle/Form/Type/ProductImageType.php

namespace Kayser\PlatformBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductImageType extends AbstractType
{
    public function  buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', 'file')
            ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kayser\PlatformBundle\Entity\ProductImage',
            'cascade_validation' => true,
        ));
    }

    public function getName()
    {
        return 'product_image';
    }
}