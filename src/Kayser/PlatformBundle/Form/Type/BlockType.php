<?php
// src/Kayser/PlatformBundle/Form/Type/BlockType.php

namespace Kayser\PlatformBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BlockType extends AbstractType
{
    public function  buildForm(FormBuilderInterface $builder, array $options)
    {
        // parent::buildForm($builder, $options);

        $builder
            ->add('title',  'text')
            ->add('body',   'textarea', array(
                'attr' => array('class' => 'ckeditor', 'row' => 10),
            ))
            ->add('link',   'text')
            ->add('image',  new BlockImageType())
            ->add('save',   'submit')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kayser\PlatformBundle\Entity\Block'
        ));
    }

    public function getName()
    {
        return 'block_add';
    }
}