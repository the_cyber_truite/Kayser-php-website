<?php
// src/Kayser/PlatformBundle/Form/Type/VideoType.php

namespace Kayser\PlatformBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VideoType extends AbstractType
{
    public function  buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title',  'text')
            ->add('description',  'textarea')
            ->add('link',  'text')
            ->add('image',  new VideoImageType())
            ->add('save',   'submit')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kayser\PlatformBundle\Entity\Video'
        ));
    }

    public function getName()
    {
        return 'add_video';
    }
}