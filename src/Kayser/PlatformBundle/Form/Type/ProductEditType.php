<?php
// src/Kayser/PlatformBundle/Form/Type/ProductEditType.php

namespace Kayser\PlatformBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductEditType extends ProductType
{
    public function  buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->remove('image')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kayser\PlatformBundle\Entity\Product'
        ));
    }

    public function getName()
    {
        return 'product_edit';
    }
}