<?php
// src/Kayser/PlatformBundle/Form/Type/BlockImageEditType.php

namespace Kayser\PlatformBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BlockImageEditType extends BlockType
{
    public function  buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->remove('title')
            ->remove('body')
            ->remove('link')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kayser\PlatformBundle\Entity\Block'
        ));
    }

    public function getName()
    {
        return 'block_image_edit';
    }
}