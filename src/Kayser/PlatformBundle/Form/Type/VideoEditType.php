<?php
// src/Kayser/PlatformBundle/Form/Type/VideoEditType.php

namespace Kayser\PlatformBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VideoEditType extends VideoType
{
    public function  buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->remove('image')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Kayser\PlatformBundle\Entity\Video'
        ));
    }

    public function getName()
    {
        return 'video_edit';
    }
}