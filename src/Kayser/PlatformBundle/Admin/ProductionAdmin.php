<?php

namespace Kayser\PlatformBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ProductionAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', array('label' => 'Nom'))
            ->add('description')
            ->add('category', 'entity', array(
            'class'    => 'KayserPlatformBundle:Category',
            'property' => 'name',
            'multiple' => false,
            'label' => 'Categorie'
            ))
            ->add('subCategory', 'entity', array(
            'class'    => 'KayserPlatformBundle:SubCategory',
            'property' => 'name',
            'multiple' => false,
            'label' => 'Sous Categorie'
            ))
            ->add('image', 'entity', array(
            'class'    => 'KayserPlatformBundle:ProductImage',
            'property' => 'id',
            'multiple' => false,
            'label' => 'Image Id'
            ))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('id')
            ->add('category.name')
            ->add('subCategory.name')
            ->add('image.id')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('id')
            ->add('description')
            ->add('category.name')
            ->add('subCategory.name')
            ->add('image.id')
            ->add('_action', 'actions', array(
            'actions' => array(
            'edit' => array(),
            'delete' => array(),)
            ))
        ;
    }
}