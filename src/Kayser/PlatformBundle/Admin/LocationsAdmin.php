<?php

namespace Kayser\PlatformBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class LocationsAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->add('name', 'text')
        ->add('street', 'text')
        ->add('city', 'text')
        ->add('zip_code', 'integer')
        ->add('state', 'text')
        ->add('country', 'text')
        ->add('opening', 'text')
        ->add('day_off', 'text')
        ->add('phone_number', 'text', array('required' => false))
        ->add('email', 'text', array('required' => false))
        ->add('coordsx', 'text')
        ->add('coordsy', 'text')
        ->add('sans_gluten', 'text', array('required' => false))
        ->add('restaurant', 'text', array('required' => false))
        ->add('cafe', 'text', array('required' => false))
        ->add('upcomming', 'text', array('required' => false))
        ->add('location', 'text', array('required' => false))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('street')
            ->add('city')
            ->add('country')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
        ->add('name')
        ->add('street')
        ->add('city')
        ->add('zip_code')
        ->add('state')
        ->add('country')
        ->add('opening')
        ->add('day_off')
        ->add('phone_number')
        ->add('coordsx')
        ->add('coordsy')
        ->add('sans_gluten')
        ->add('restaurant')
        ->add('cafe')
        ->add('upcomming')
            ->add('_action', 'actions', array(
            'actions' => array(
            'edit' => array(),
            'delete' => array(),)
            ))
        ;
    }
}